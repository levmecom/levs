<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-20 18:37
 *
 * 项目：levs  -  $  - header_htm.php
 *
 * 作者：likeqian
 */

//!defined('INLEV') && exit('Access Denied LEV');
?>

<div class="card card-header">
    <ol class="ol-cm">
        <li>
            <a class="button-small button button-fill inblk scale9 vera" href="<?=Lev::toReRoute(['admin-users', 'id'=>'levs'])?>">用户密码</a>
        </li>
    </ol>
</div>
