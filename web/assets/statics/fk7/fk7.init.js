// Initialize your app
var myApp = new Framework7({
    //material:true,
    //materialPageLoadDelay:3,
    //allowDuplicateUrls:true,
    //pushStateNoAnimation:true,
    //reloadPages:true,
    //swipePanel: 'left',//左滑动屏幕打开侧边栏
    //fastClicks:false,
    activeState: jQuery('.noActiveState').length <= 0,
    modalTitle: '',
    modalButtonOk: '\u786e\u5b9a',//确定
    modalButtonCancel: '\u53d6\u6d88',///取消
    smartSelectBackText: '\u8fd4\u56de',//返回
    smartSelectPopupCloseText: '\u5173\u95ed',//关闭
    smartSelectPickerCloseText: '\u5b8c\u6210',//完成
    animateNavBackIcon: true,
    pushState: true,
    ajaxLinks: 'a.is_ajax_a',
    cache: typeof myAppCache !=='undefined' ? myAppCache : true,
    onAjaxStart: function (xhr) {
        //myApp.closeModal('', false);
        Levme.closePP(false);
        showIconLoader();
    },
    onAjaxComplete: function (xhr) {
        hideIconLoader(true);//console.log(xhr, JSON.parse(xhr.responseText).message);
        if (xhr.responseText) {
            if (xhr.responseText.indexOf('<title>提示 -') >= 0 && xhr.responseText.indexOf('showMsgBox') >= 0) {
                levtoast(xhr.responseText);
            }else if (xhr.responseText.indexOf('{') === 0) {
                var json = JSON.parse(xhr.responseText);
                if (json) {
                    if (json.confirmurl) {
                        myApp.confirm(json.message, '', function () {
                            Levme.ajaxv.getv(json.confirmurl, function (data, status) {

                            }, {confirmdoit:1});
                        });
                        return;
                    }
                    json.message && levtoast(json.message);
                    json.status === -5 && openLoginScreen();
                    json.tourl && (window.location = json.tourl);
                    json.notices && Levme.showNotices(json.notices);
                }
            }
        }
        if (typeof (ajax_complete_back) === "function") {
            ajax_complete_back(xhr)
        }
    }
});

function openscreen(id, hide, obj) {
    //showIconLoader();
    myApp.closeModal('.picker-modal', false);
    myApp.closeModal('.login-screen', false);
    myApp.closeModal('.popup', false);
    myApp.closeModal(id, false);
    myApp.loginScreen(id);//'.qju_login'
    if (!hide) {
        jQuery(id).on('opened', function(){hideIconLoader();});
    }
    var replyscreen = jQuery(obj).attr('reply');
    if (replyscreen) {
        jQuery(id +' .replytoscreen').attr('class', replyscreen +' replytoscreen');
    }

    jQuery('.cleartzbtn').click();
}

var LoginWinConfirm__ = 0;
function openLoginScreen(_id, animated, onConfirm) {
    var id = _id ? _id : '.my-login-screen';
    showIconLoader(true);
    myApp.closeModal('.actions-modal', false);
    //myApp.closeModal('.popup', false);
    Levme.closePP(false);
    myApp.popup(id, false, animated);
    if (!jQuery(id).hasClass('login_screen') && id === '.my-login-screen') {
        hideIconLoader();
        myApp.closeModal('.modal.modal-in', false);
        onConfirm ? Levme.loginv.win() :
        myApp.confirm(Levme.la('抱歉，您需要登陆才能继续操作'), '', function () {
            Levme.loginv.win();
        });
    }
    jQuery(id).on('opened', function(){hideIconLoader();});
}

function arrayProduct(n) {
    if (!n) return 0;
    if (n < 2) {
        return n;
    }else {
        return (n * arrayProduct(n - 1));
    }
}
function arrayA(n, m) {
    if (n < m) return 0;
    if (n === m) return 1;
    return Math.round(arrayProduct(n) / arrayProduct( n - m ));
}
function arrayC(n, m, isrepeat) {
    if (n < m) return 0;
    if (n === m) return 1;
    isrepeat && (n += m - 1);//为真时包含重复组合
    return Math.round(arrayA(n, m) / arrayProduct(m));
}

function levArt(src) {
    var iframe = '<iframe style="min-width:700px;min-height:400px;height:100%;border:0;" src="'+src+'"></iframe>';
    levtoast(iframe, -1);
    return false;
}

var _levtoast2Timeout = null;
function levtoast2(msgtxt, time, cls, ckNoClose) {
    //if (msgtxt && msgtxt.indexOf('</title>') >0) return myApp.confirm('<div style="overflow: auto !important;">'+msgtxt+'</div>');
    clearTimeout(_levtoast2Timeout);
    var _cls = cls ? cls : '';
    var _time = time ? time : 2000;
    var _htm = '<div class="levtoast2 '+_cls+'"><div class="msgTxtBox">'+ msgtxt +'</div></div>';
    jQuery('.levtoast2').remove();
    msgtxt && jQuery('body').append(_htm);
    if (_time > 0) {
        _levtoast2Timeout = window.setTimeout(function(){ jQuery('.levtoast2').remove(); }, _time);
    }
    ckNoClose ? jQuery(document).off('click', '.levtoast2')
        : jQuery(document).on('click', '.levtoast2', function () { jQuery('.levtoast2').remove(); });
}
function levtoast(msgtxt, time, _cls) {
    return levtoast2(msgtxt, time, _cls);
}

function myLoginScreen(cls, animated) {
    openLoginScreen(cls, animated)
}

function getParseUrlQuery(url) {
    var pms = {};
    var _pms = url.split('?');
    if (_pms.length >1) {
        var _pms = _pms[1].split('&');
        for (var k in _pms) {
            var _pmss = _pms[k].split('=');
            pms[_pmss[0]] = _pmss[1];
        }
    }
    return pms;
}

function levConfirm(text, title, canFunc, celFunc) {
    myApp.closeModal('.modal.modal-in', false);
    myApp.confirm(text, title, canFunc, celFunc);
}

function ckWxUserAgent(userAgent) {
    return (userAgent ? userAgent : navigator.userAgent).indexOf('MicroMessenger') >0;
}

function deviceType(name) {
    return myApp.device[name];
}

function showToolbar(cls) {
    myApp.showToolbar(cls);
}
function hideToolbar(cls) {
    myApp.hideToolbar(cls);
}

function showIconLoader(clickHide, hideFunc) {
    myApp.showIndicator();
    var objm = '.preloader-indicator-modal, .preloader-indicator-overlay';
    jQuery(objm).css('z-index', '99999999999999999');
    if (clickHide) {
        Levme.onClick(objm, function () {
            hideIconLoader(hideFunc)
        });
    }else {
        jQuery(document).off('click', objm);
    }
}
function hideIconLoader(hideFunc) {
    myApp.hideIndicator();
    typeof hideFunc === "function" && hideFunc();
}

//设置滚动条到最底部
function setScrollBottom(id) {
    var ele = document.getElementById(id);
    if(ele && ele.scrollHeight > ele.clientHeight) {
        ele.scrollTop = ele.scrollHeight;
    }
}

//lev 路由 levToRoute('/ucenter/my', {'uid':'1', 'dd':0})
function levToRoute(_rote, pm) {
    var url = '', pmstr = '';
    if (_rote && (_rote.indexOf('http') === 0 || _rote.indexOf('.php') > 0)) {
        url = _rote;
    }else if (typeof homeFile !== "undefined") {
        var rote = _rote && _rote.indexOf('/') === 0 ? _rote.slice(1) : _rote;
        if (homeFile.indexOf('.php') >=0) {
            url = homeUrl + '/' + homeFile + '?r='+ rote;
        }else {
            url = homeUrl + '/' + homeFile + rote
        }
    }else {
        url = homeUrl + (_rote.indexOf('/') === 0 ? '' : '/') + _rote;
    }
    if (pm) {
        for (var k in pm) {
            url = changeUrlArg(url, k, pm[k]);
        }
    }
    return url;
}
function changeUrlArgs(url, pm) {
    if (pm) {
        for (var k in pm) {
            url = changeUrlArg(url, k, pm[k]);
        }
    }
    return url;
}
function changeUrlArg(url, arg, val){
    if (!url) return url;

    var _replace = false;
    if (url.indexOf('?') <0) {
        url += '?' + arg + '=' + val;
    } else {
        var _pm = url.split('?');
        var _pm2 = _pm[1].split('&');
        for (var k in _pm2) {
            if (Levme.hasOwnProperty(_pm2, k) && (_pm2[k] || parseInt(_pm2[k]) === 0)) {
                _pm2[k] = _pm2[k].toString();
                if (_pm2[k].indexOf(encodeURI(arg) + '=') === 0 || _pm2[k].indexOf(arg + '=') === 0) {
                    _pm2[k] = arg + '=' + val;
                    _replace = true;
                    break;
                }
            }
        }
        url = _pm[0] +'?'+ _pm2.join('&');
        url+= !_replace ? '&'+ arg +'='+ val : '';
    }
    return url;
}

function dom7showmsg(msgtxt, time) {
    myApp.closeModal();
    var time = time ? time : 1500;
    myApp.modal({
        title: false,
        text: '<div class="msg_box">'+ msgtxt +'</div>',
        buttons: [{
            text:'\u786e\u5b9a',//确定
            bold:true
        }]
    });
    if (time > 0) {
        var cls_win = window.setTimeout(function(){
            clearTimeout(cls_win);
            myApp.closeModal();
        }, time);
    }
}

function levmarquee(elementID, _h, _n, _speed, _delay, ckH){
    var t = null;
    var box = '#' + elementID;
    var h = _h ? _h : jQuery(box + ' li').height();
    var n = _n ? _n : 1;
    var speed = _speed ? _speed : 500;
    var delay = _delay ? _delay : 2000;
    if (jQuery(box + ' li').length <2) return false;
    if (ckH && jQuery(box).height() >= jQuery(box).children('ul:first').height()) return false;
    jQuery(box).hover(function(){
        clearInterval(t);
    }, function(){
        t = setInterval(function(){_start(box, h, n, speed)}, delay);
    }).trigger('mouseout');

    function _start(box, h, n, speed){
        jQuery(box).children('ul:first').animate({marginTop: '-='+h}, speed, function(){
            jQuery(this).css({marginTop:'0'}).find('li').slice(0,n).appendTo(this);
        })
    }
}

function loginscreen() {
    //levtoast('抱歉，您需要先登陆才能执行该操作！');
    levtoast('\u62b1\u6b49\uff0c\u60a8\u9700\u8981\u5148\u767b\u9646\u624d\u80fd\u6267\u884c\u8be5\u64cd\u4f5c\uff01');
    myLoginScreen();
}

function actionLocalStorage(key, val, del) {
    if (typeof localStorage === "undefined") {
        return false;
    }
    if (typeof(in__android__app) != 'undefined' && JSON.stringify(localStorage) === null) {
        return false
    }

    if (del) {
        return localStorage.removeItem(key);
    }

    if (val !== undefined) {
        return localStorage.setItem(key, val);
    }else {
        return localStorage.getItem(key);
    }
}

function reload_mainurl() {
    var urlx = window.location.href.split('#!');
    Levme.mainView.router.back({
        url: urlx[0],
        //animatePages:false,
        force:true,
        ignoreCache:true,
        reload:true,
        //reloadPrevious:true,
        pushState:false,
    });
    window.history.pushState('forward', null, urlx[0]);
}

function arr_remove(val, arr) {
    var xindex = arr.indexOf(val);
    if (xindex !=-1) {
        arr.splice(xindex, 1);
    }
    return arr;
}

function levmaxmin(noarr) {//返回数组中最大、最小数；
    var min = 0, max = 0;
    var len = noarr.length;
    for (var i=0; i<len; i++) {
        var no = parseInt(noarr[i]);
        min = i==0 || no <min ? no : min;
        max = i==0 || no >max ? no : max;
    }
    var res = [max,min];
    return res;
}

function errortips(obj, msg) {
    var time = 5000;
    var dmsg = msg ? msg : '';
    if (!dmsg && obj) {
        switch (obj.status) {
            case 200 : dmsg = '系统繁忙'; break;
            case 500 : dmsg = '服务器错误500'; break;
            default: if ( window.UID == 1 ) dmsg = '网络已断开'+ obj.status; break;
        }
    }
    if (!obj) {
        //msg !== false && levtoast(dmsg, 5000);
    } else if (obj.responseJSON && obj.responseJSON.message) {
        //levtoast(obj.responseJSON.message, 15000);
        time = 15000;
        dmsg = obj.responseJSON.message;
    }else if (obj.error && obj.error.message) {
        //levtoast(obj.error.message, 15000);
        time = 15000;
        dmsg = obj.error.message;
    }else if (obj.message) {
        //levtoast(obj.message, 15000);
        time = 15000;
        dmsg = obj.message;
    }else {
        dmsg = jQuery.trim(obj.responseText ? obj.responseText : dmsg);
        if (msg === false) return;
    }
    var showMsgBox = jQuery('<div>'+ dmsg+'</div>').find('.showMsgBox').html();
    levtoast(showMsgBox || dmsg, time);
}

function showFormErrors(data, box) {
    box = box ? box : '';
    jQuery('errors').hide();
    for (var key in data) {
        var obj = jQuery(box +'[name="'+ key +'"], #'+ key);
        var objp = obj.parent();
        if (jQuery(box + '[name="' + key + '[]"]').parents('.checkbox-list').length >0) {
            objp = jQuery(box + '[name="' + key + '[]"]').parents('.checkbox-list');
            obj = objp;
        }
        if (objp.find('errors').html()) {
            objp.find('errors').html(data[key]).show();
        }else {
            objp.append('<errors>'+ data[key] +'</errors>');
        }
        obj.addClass('errorsTip');
        obj.focus();
    }
    jQuery(document).on('change', 'input,textarea', function () {
        jQuery(this).parent().find('errors').hide();
        jQuery(this).removeClass('errorsTip');
    });
}

function levtoMao(val) {
    var myY = jQuery("#"+val).offset().top;
    jQuery("html,body").stop().animate({ scrollTop:myY},800);
}


function levtoMaoLeft(id, pid, ts, extra, top) { //console.log(jQuery(id).offset());
    var _ts = ts ? ts : 300;
    var _extra = parseFloat(extra);
    _extra = isNaN(_extra) ? 0 : _extra;
    var ztop = 0, ctop = 0, myY = 0;
    if (top) {
        ztop = jQuery(pid).find(id).offset().top - jQuery(pid).offset().top;
        ctop = jQuery(pid).scrollTop();
        myY = ztop + ctop + (ztop >0 ? _extra : 0);//console.log(myY, ztop);
        jQuery(pid).stop().animate({scrollTop : myY}, _ts);
    }else {
        ztop = jQuery(pid).find(id).offset().left - jQuery(pid).offset().left;
        ctop = jQuery(pid).scrollLeft();
        myY = ztop + ctop + (ztop > 0 ? _extra : 0);
        jQuery(pid).stop().animate({scrollLeft : myY}, _ts);
    }
}
function levtoMaoTop(id, pid, pts, ts) {
    ts = ts ? ts : 30;
    var ztop = (pts ? jQuery(id).parents(pts).offset().top : jQuery(id).offset().top) - jQuery(pid).offset().top;
    var ctop = jQuery(pid).scrollTop();
    var myY = ztop + ctop;//console.log(ztop, ctop, myY, jQuery(pid));
    jQuery(pid).stop().animate({scrollTop : myY}, ts);
}
function levtoMaoCenter(id, pid, ts, extra, top, navBoxWidth) { //console.log(jQuery(id).offset());
    if (jQuery(pid).find(id).length <1) return;
    var _ts = ts ? ts : 300;
    var _extra = parseFloat(extra);
    _extra = isNaN(_extra) ? 0 : _extra;
    var ztop = 0, ctop = 0, myY = 0;
    if (top) {
        ztop = jQuery(pid).find(id).offset().top - jQuery(pid).offset().top;
        ctop = jQuery(pid).scrollTop();
        myY = ztop + ctop + (ztop >0 ? _extra : 0);//console.log(myY, ztop);
        jQuery(pid).stop().animate({scrollTop : myY}, _ts);
    }else {
        navBoxWidth = navBoxWidth ? navBoxWidth : jQuery(pid).parent().width();
        ztop = jQuery(pid).find(id).offset().left - jQuery(pid).offset().left;
        ctop = jQuery(pid).scrollLeft();
        myY = ztop + ctop + (ztop > 0 ? _extra : 0) - navBoxWidth/2 + jQuery(id).width();
        jQuery(pid).stop().animate({scrollLeft : myY}, _ts);
    }
}

function checkedToggle(toggleId, opid) {
    if (jQuery(toggleId).prop('checked')) {
        jQuery(opid).prop('checked', true);
    }else {
        jQuery(opid).prop('checked', false);
    }
}


function levcutstr(str, len, hasDot) {
    var newLength = 0;
    var newStr = "";
    var chineseRegex = /[^\x00-\xff]/g;
    var singleChar = "";
    var strLength = str.replace(chineseRegex, "**").length;
    for (var i = 0; i < strLength; i++) {
        singleChar = str.charAt(i).toString();
        if (singleChar.match(chineseRegex) != null) {
            newLength += 2;
        }
        else {
            newLength++;
        }
        if (newLength > len) {
            break;
        }
        newStr += singleChar;
    }

    if (hasDot && strLength > len) {
        newStr += "...";
    }
    return newStr;
}

function dlevrandom(max, num){
    var randarr = [];//从0－max随机取出不重复的数字；num取出个数
    var a = [];
    for(var i = 0; i <=max; i++){
        a.push(i);
    }
    for (var j=0; j < num; j++) {
        var b = [];
        var _idx = -1;
        for (i=0; i <=max; i++) {
            if (!isNaN(a[i])) {
                _idx+= 1;
                b[_idx] = a[i];
            }
        }
        var _len = b.length;
        var _randnum = levrandom(1, _len) -1;
        randarr[j] = b[_randnum];
        a.splice(_randnum, 1);
        //a.sort();console.log('数组：', a);
    }
    return randarr;
}
function levrandom(min, max) {
    if (max == null) {
        max = min;
        min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
}

function animateCSS(element, animationName, callback) {
    var node = document.querySelector(element);
    //node.classList.add('animated', animationName);
    jQuery(element).addClass(animationName).addClass('animated');

    function handleAnimationEnd() {
        //node.classList.remove('animated', animationName);
        jQuery(element).removeClass(animationName);
        node.removeEventListener('animationend', handleAnimationEnd);

        if (typeof callback === 'function') callback();
    }

    node.addEventListener('animationend', handleAnimationEnd);
}

jQuery('.open-panel').on('click', function(){
    var panel_left_open = false;
    jQuery('.panel-left').on('open', function () {
        panel_left_open = true;
    });
    if (!panel_left_open) {
        myApp.closePanel();
    }
});

jQuery(document).on('dblclick', 'textarea', function(){
    var obj = this;
    if (jQuery(obj).hasClass('except')) return;

    if (jQuery(obj).hasClass('isBigArea')) {
        if (jQuery(obj).hasClass('dbLc')) {
            jQuery(obj).removeClass('isBigArea dbLc');
            jQuery(obj).parent().removeClass('isBigArea');
        }else {
            jQuery(obj).addClass('dbLc');
            Levme.showNotices('【提示】5秒内两次双击将还原输入框', 0, 5000);
            window.setTimeout(function () { jQuery(obj).removeClass('dbLc'); }, 5000);
        }
    }else {
        jQuery(obj).addClass('isBigArea');
        jQuery(obj).parent().addClass('isBigArea');
    }
});

var Levme = {
    tempDatas:{},
    la(key){
        if (typeof vLangs !== "undefined") {
            return typeof vLangs[key] === "undefined" ? key : vLangs[key];
        }
        return key;
    },
    playVideoBtnFunc(btnObj) {
        var src = jQuery(btnObj).attr('src');
        var height = jQuery('.mobile-1').length >0 ? 320 : 520;
        Levme.alert('<div class="videoBox" style="background: rgba(0,0,0,0.8);max-width:100%;height:'+height+'px"> ' +
            '<video id="pVideo" style="width:100%;height:100%" src="'+src+'" controls="" autoplay="autoplay">' +
            '您的浏览器不支持此视频的播放，请升级浏览器版本或更换其它浏览器进行播放</video> </div>', '', function () {
        }, 0, '关闭');
    },
    outerHTML(cls) {
        return jQuery(cls).prop('outerHTML');
    },
    hasOwnProperty (arr, key) {
        return arr.hasOwnProperty(key);
    },
    checkVersion (version, jsversion, ck, msg) {
        var _version = version && version.indexOf('{{'+'#v#') === 0 ? version : '{{'+'#v#'+version+'#v#'+'}}';
        if (ck || (version && _version !== jsversion)) {
            console.log(jsversion, version);
            Levme.showNotices(msg || '亲，检测到有新版，请清理浏览器缓存');
        }
    },
    hideProgressbar (cls) {
        myApp.hideProgressbar(cls);
    },
    showProgressbaring (container, hide, maxProgress, color) {
        hide = hide === undefined ? true : hide;
        maxProgress = maxProgress || 100;
        //color = color || 'red';
        if (jQuery(container).length <1) return;

        myApp.showProgressbar(container, 0, color);

        var progress = 0;
        function simulateLoading() {
            setTimeout(function () {
                var progressBefore = progress;
                progress += Math.random() * 20;
                myApp.setProgressbar(container, progress);
                if (progressBefore < maxProgress) {
                    simulateLoading(); //keep "loading"
                } else {
                    hide && Levme.hideProgressbar(container);
                } //hide
            }, Math.random() * 400 + 400);
        }
        simulateLoading();
    },
    sendEmailBtn (btnObj) {
        var touid = parseInt(jQuery(btnObj).attr('touid'));
        var title = jQuery(btnObj).attr('title') || '';
        var tips = jQuery(btnObj).attr('tips') || '';
        var username = jQuery(btnObj).attr('username') || '';
        var contents = jQuery(btnObj).attr('contents') || '';
        Levme.sendEmail(touid, title, contents, tips, username);
        return false;
    },
    sendEmailBug(btnObj) {
        Levme.sendEmail(-4, 'BUG提交', '', '每位提交BUG者都可以获得奖奖励');
        return false;
    },
    sendEmail (touid, title, contents, tips, username) {
        tips = tips || '';
        username = username || '';

        var url = levToRoute('my-email/send', {id:'email',touid:touid});
        var imgsrc = '';
        var msgTitle = '<span class="font14">发送邮件'+ (username ? '【'+ username +'】' : '')+'</span>';
        var text = '<div class="sendEmailForm"><div>' +
                '<input type="text" class="comm-input" name="title" placeholder="请输入邮件标题" value="'+title+'">' +
            '</div>'+
            '<div>' +
                '<textarea class="comm-input" name="contents" style="height:70px" placeholder="请输入邮件内容">'+contents+'</textarea>' +
            '</div>' +
            '<label class="imgsrcForm"><btn class="button button-small color-gray button-fill">' +
            '<span class="imguped"><svg class="icon"><use xlink:href="#fa-haibao"></use></svg></span>' +
            '&nbsp;<b>上传图片</b></btn>' +
            '<input type="file" class="hiddenx imgsrc" name="imgsrc" accept="image/*"></label>' +
            '<tips>'+ tips +'</tips></div>';
        myApp.confirm(text, msgTitle, function () {
            var title = jQuery('.modal-inner .sendEmailForm input[name="title"]').val();
            var contents = jQuery('.modal-inner .sendEmailForm textarea[name="contents"]').val();
            if (!title) {
                levtoast('请输入标题');
                return;
            }
            if (!contents && !imgsrc) {
                levtoast('请输入内容');
                return;
            }
            Levme.ajaxv.postv(url, function (data, status) {

            }, {title:title, contents:contents, imgsrc:imgsrc});
        });
        Levme.uploadImgForm('.imgsrc', levToRoute('upload/image', {input:'imgsrc'}), 0, function (data) {
            imgsrc = data.src;
            jQuery('.imgsrcForm b').html('已上传，可直接发送');
        });
    },
    showMyScores(myScores, x) {
        if (myScores) {
            var myScoreBox = '.myScoreBox';
            for (var k in myScores) {
                var score = Levme.formatNumber(myScores[k].score, x);
                var scoreCls = myScoreBox +' myscore.key_'+ myScores[k].id;
                var omyScore = jQuery(scoreCls).attr('score');
                jQuery(scoreCls)
                    .html(score)
                    .attr('score', myScores[k].score)
                    .attr('title', myScores[k].title);
                Levme.addScoreDhua(myScores[k].id, omyScore, myScores[k].score, myScores[k].title)
            }
        }
    },
    addScoreDhua(id, omyScore, myScore, title) {
        title = title || '';
        var myScoreBox = '.myScoreBox';
        var scoreCls = myScoreBox +' myscore.key_'+ id;
        var tipscore = '';
        var xscore = (parseFloat(myScore) - parseFloat(omyScore)) || 0;
        if (!xscore) {
            return;
        }
        if (xscore > 0) {
            tipscore = '<b class="color-orange">+'+ xscore + title +'</b>';
            jQuery(scoreCls).append('<div class="addscore_gif font12 active">'+ tipscore + '</div>');
        }else {
            tipscore = '<b class="color-red">'+ xscore + title +'</b>';
            jQuery(scoreCls).append('<div class="addscore_gif font12 active">'+ tipscore + '</div>');
        }
        Levme.setTimeout(function () {
            jQuery(scoreCls +' .addscore_gif').remove();
        }, 2400);
    },
    formatNumber(num, x) {
        var str = '';
        x = x || 1;
        if (num <1000*x) str = num;
        else if (num <10000) str = parseFloat((num/1000).toFixed(1))+ '千';
        else if (num <100000000) str = parseFloat((num/10000).toFixed(1))+ '万';
        else str = parseFloat((num/100000000).toFixed(2))+ '亿';
        return str;
    },
    animated:function (obj, animate) {
        var animated = 'animated '+ (animate ? animate : 'heartBeat');
        jQuery(obj).addClass(animated);
        window.setTimeout(function () { jQuery(obj).removeClass(animated); }, 1000);
    },
    setIframeHeight:function (id, ht) {jQuery(id).css('height', ht);},
    unload:function () {
        jQuery(window).unload(function () {
            levtoast('ddd');
        });
    },
    todYmd:function (timestamp, format) {
        var date = timestamp ? new Date(timestamp) : new Date();
        var year = date.getFullYear().toString();
        var month = Levme.addx0(date.getMonth()+1) +'';
        var day = Levme.addx0(date.getDate()) +'';
        var hour = Levme.addx0(date.getHours()) +'';
        var minite = Levme.addx0(date.getMinutes()) +'';
        var second = Levme.addx0(date.getSeconds()) +'';
        var datestr = '';
        if (format) {
            datestr = format.toString().replace(/Y/g, year).replace(/m/g, month).replace(/d/g, day).replace(/H/g, hour).replace(/i/g, minite).replace(/s/g, second);
        }else {
            datestr = year + month + day;
        }
        return datestr;
    },
    addx0:function (num) {
        var number = parseFloat(num);
        if (isNaN(number)) return num;
        number = number < 10 ? '0'+ number : number;
        return number;
    },
    sign: {
        init: function () {
            Levme.onClick('.signBtn', function () {
                Levme.sign.signBtn(jQuery(this).data('typeid'));
            });
            if (Levme.todYmd() != actionLocalStorage('signYmd')) {
                jQuery('.signNavbarBtn cir.hiddenx').removeClass('hiddenx');
            }
        },
        signBtn: function (typeid) {
            Levme.ajaxv.getv(levToRoute('ajax/sign', {id: 'levsign', typeid: typeid}), function (data, status) {
                if (status > 0) {
                    jQuery('.typeid_' + typeid).removeClass('signBtn').addClass('isSigned');
                    actionLocalStorage('signYmd', Levme.todYmd());
                }
                jQuery('.tabBtnBox .tabBtn_' + typeid + ' cir').remove();
            });
        },
    },
    setups:{
        btn:function () {
            Levme.onClick('.setupsBox a', function () {
                var opid = parseInt(jQuery(this).parents('.setupsBox').attr('opid'));
                if (isNaN(opid) || opid < 1) {
                    Levme.showNotices('抱歉，新增数据，你必须先保存第一步内容，才能进入下一步', 'error');
                    return false;
                }
            });

            var _setup_btn_idx = jQuery('.setupsBox a.active').index('.setupsBox a');
            jQuery('.setupsBox a:lt('+(_setup_btn_idx <0 ? 1 : _setup_btn_idx)+')').addClass('color-black active');
        }
    },
    showNotices:function (str, type, timeout, wait, closeOnClick) {
        wait = wait === undefined ? 200 : wait;
        closeOnClick = closeOnClick === undefined ? true : closeOnClick;
        timeout = timeout ? timeout : 25000;
        type = type ? type : 'info';
        str = str !== undefined ? str : actionLocalStorage('cookieNotices');
        str && str !== "" &&
        window.setTimeout(function () {
            jQuery('.close-notification').removeAttr('href');
            actionLocalStorage('cookieNotices', 0, 1);
            var style = closeOnClick ? '{display: none !important;}' : '{height: 22px;position: absolute;z-index: 9999;margin: 0;overflow: hidden;border-radius: 50%;padding: 5px 5px 5px 0;transform: scale(.6);right: 0;top: 0;box-shadow: 0 0 5px 5px gray;}';
            myApp.addNotification({
                title: '<style>.notifications .item-title-row '+ style +'</style>',
                message: '<div class="notification notification-session"><div class="item-text alert-'+type+'">' + str + '</div></div>',
                media: '',
                closeOnClick: closeOnClick,
                hold: timeout,
            });
        }, wait);
    },
    showNoticesAdd:function (str, type, timeout) {
        jQuery('.notification.notification-session .item-text').length >0
            ? jQuery('.notification.notification-session .item-text').append(str)
            : Levme.showNotices(str, type, timeout);
    },
    getNoticsAddhtm:function () {
        return jQuery('.notification.notification-session .item-text').html();
    },
    loginv:{
        btns: function () {
            Levme.onClick('.dLoginBtn', function () {
                Levme.loginv.dLoginScreen();
                return false;
            });
            Levme.onClick('.openLoginBtn', function () {
                Levme.loginv.win(jQuery(this).attr('href'));
                return false;
            });
            Levme.onClick('.openRegisterBtn', function () {
                Levme.loginv.reg(jQuery(this).attr('href'));
                return false;
            });
            Levme.onClick('.exitLoginOutBtn', function () {
                Levme.loginv.exitLogin(this);
                return false;
            });
        },
        win:function (src) {
            src = src ? src : Levme.loginv.loginUrl;
            if (src) {
                if (src.indexOf('logging') > 0) {
                    return Levme.popupIframe.popupShow(0, src);
                }
                Levme.mainView.router.loadPage(src);
                return;
            }
            myApp.closeModal('.popup', false);
            myApp.popup('.my-login-screen', false);
            Levme.loginv.loadScreen();
        },
        reg:function (src) {
            src = src ? src : Levme.loginv.registerUrl;
            if (src) {
                return Levme.popupIframe.popupShow(0, src);
            }
            myApp.closeModal('.popup', false);
            myApp.popup('.my-register-screen', false);
            Levme.loginv.callRegWin = 1;
            Levme.loginv.loadScreen();
        },
        dLoginScreen:function () {
            myApp.closeModal('.popup', false);
            myApp.popup('.my-login-screen', false);
            Levme.loginv.loadScreen();
        },
        exitLogin:function () {
            myApp.confirm('您确定要退出登陆吗？', '', function () {
                Levme.ajaxv.getv(levToRoute('login/exit', {id:APPVIDEN}), function(data, status) {
                    if (status > 0) {
                        if (data.loginReferer && data.loginReferer !== "") {
                            window.top.location = data.loginReferer;
                        }else if (window.top.location.href.indexOf('accesstoken=') >0) {
                            window.top.location = window.top.location.href.replace(/accesstoken=/g, 'akenx=');
                        }else {
                            window.top.location = window.location.href.split('#!/')[0];
                        }
                    }
                });
            });
        },
        loaded:0,
        loginUrl:0,
        registerUrl:0,
        callRegWin:0,
        loadScreen:function () {
            if (Levme.loginv.loaded) return;
            Levme.loginv.loaded = 1;
            //if (jQuery('.my-login-screen').length >0) return;
            showIconLoader(true);
            Levme.ajaxv.getv(levToRoute('login/load-screen', {id: RouteIden}), function (data, status) {
                if (status > 0 && data) {
                    data.loginUrl && (Levme.loginv.loginUrl = data.loginUrl);
                    data.registerUrl && (Levme.loginv.registerUrl = data.registerUrl);
                    data.htms && jQuery('.my-login-screen').length <1 && jQuery('body').append(data.htms);
                    Levme.loginv.callRegWin ? Levme.loginv.reg() : Levme.loginv.win();
                }
            });
        }
    },
    confirm:function (text, title, canFunc, celFunc) {
        text = text ? text : '<b>'+ title+ '</b>';
        var btn = '<p class="flex-box ju-sa">' +
            '<a class="button button-fill Btn1">确定</a><a class="button button-fill color-gray Btn2">取消</a></p>';
        levtoast(text + btn, 3600000, 'LevmeConfirm');
        Levme.onClick('.levtoast2.LevmeConfirm .Btn1', function () { typeof canFunc === "function" && canFunc() });
        Levme.onClick('.levtoast2.LevmeConfirm .Btn2', function () { typeof celFunc === "function" && celFunc() });
    },
    openedScreens:{},
    screenHtml: function(id, htm) {
        if (htm) {
            return jQuery('.lev-screen.swiper-box.' + id + ' .swiper-slide').eq(1).html(htm);
        }else {
            return jQuery('.lev-screen.swiper-box.' + id + ' .swiper-slide').eq(1).html();
        }
    },
    screenClose:function(id, animated, forceClose) {
        Levme.onClick('.closeLevScreen', function () {
            var id = jQuery(this).parents('.lev-screen.swiper-box').data('id');
            closeScreen(id, animated);
        });

        id && closeScreen(id, animated, forceClose);

        function closeScreen(id, animated, forceClose) {
            if (typeof Levme.openedScreens[id] !== "undefined") {
                var len = Levme.openedScreens[id].length - 1;
                if (typeof Levme.openedScreens[id][len] === "function") {
                    if (forceClose === 1) {
                        Levme.openedScreens[id][len](0);
                    } else {
                        Levme.openedScreens[id][len](forceClose ? 0 : animated);
                    }
                    Levme.openedScreens[id].splice(len, 1);
                } else {
                    jQuery('.lev-screen.swiper-box.' + id).remove();
                }
            } else {
                jQuery('.lev-screen.swiper-box.' + id).remove();
            }
        }
    },
    ajaxv:{
        isGetv:0,
        postv:function (url, sucFunc, param) {
            return Levme.ajaxv.dov(url, sucFunc, 'post', param);
        },
        getv:function (url, sucFunc, param) {
            return Levme.ajaxv.dov(url, sucFunc, 'get', param);
        },
        dosubmit:function (formid, action, sucFunc) {
            if (Levme.ajaxv.isGetv) {
                showIconLoader(true);
                return;
            }
            Levme.ajaxv.isGetv = 1;
            jQuery(formid).ajaxSubmit({
                url: action ? action : '',
                data: {dosubmit:1, _csrf:_csrf, inajax:1, _:Math.random()},
                type:'post',
                dataType: 'json',
                success: function(data){
                    Levme.ajaxv.isGetv = 0;
                    hideIconLoader();
                    var status = 0;
                    if (data) {
                        data.message && levtoast(data.message);

                        status = parseInt(data.status);
                        if (status === -5) {
                            openLoginScreen();
                            return false;
                        }
                    }
                    typeof sucFunc === "function" && sucFunc(data, status);
                    showFormErrors(data.errors);
                },
                error: function(data) {
                    Levme.ajaxv.isGetv = 0;
                    typeof sucFunc === "function" && sucFunc(data);
                    hideIconLoader();
                    errortips(data);
                }
            });
            return false;
        },
        dov:function (url, sucFunc, type, param) {
            if (Levme.ajaxv.isGetv) {
                showIconLoader(true);
                return;
            }
            param === undefined && (param = {});
            param['_csrf'] = _csrf;
            param['_'] = Math.random();
            Levme.ajaxv.isGetv = 1;
            jQuery.ajax({
                url: url,
                data: param,
                dataType: 'json',
                type: type ? type : 'get',
                success: function (data) {
                    Levme.ajaxv.isGetv = 0;
                    hideIconLoader();
                    var status = 0;
                    if (data) {
                        Levme.viptoast(data);

                        status = parseInt(data.status);
                        if (status === -5) {
                            openLoginScreen();
                            return false;
                        }
                    }
                    typeof sucFunc === "function" && sucFunc(data, status);
                },
                error: function (data) {
                    Levme.ajaxv.isGetv = 0;
                    typeof sucFunc === "function" && sucFunc(data);
                    hideIconLoader();
                    errortips(data);
                }
            });
        }
    },
    viptoast(data, time) {
        if (!data) {
            return;
        }
        if (data.tourl) {
            if (data.message) {
                myApp.closeModal('.actions-modal.modal-in', false);
                var btns = [{
                    text: data.message,
                    label: true,
                    close: true,
                    disabled: false,
                }];
                if (typeof data.tourl !== "string") {
                    var _data = data;
                    for (var kname in data.tourl) {
                        btns.push({
                            text: kname,
                            onClick: function () {
                                //window.location = data.tourl[kname];
                                _data.tourl = data.tourl[kname];
                                Levme.tourl(_data);
                            }
                        });
                    }
                } else {
                    btns.push({
                        text: '确定',
                        onClick: function () {
                            //window.location = data.tourl;
                            Levme.tourl(data);
                        }
                    });
                }
                myApp.actions(btns);
            }else {
                Levme.tourl(data);
            }
        }else if (data.reload) {
            Levme.setTimeout(function () {
                window.location.reload();
            }, 400);
        }else if (data.message) {
            levtoast(data.message, time);
        }
        data.showNotices &&
        Levme.showNotices(data.showNotices.notices, data.showNotices.type, data.showNotices.timeout, 200, data.showNotices.closeOnClick);
    },
    tourl (data) {
        if (!data) return;
        if (data.top) {
            parent.Levme.closePP(false);
            if (data.openPP) {
                data.Inajax
                    ? parent.Levme.popupAjax.showv(data.tourl, undefined, data.fullScreen, 1)
                    : parent.Levme.popupIframe.popupShow(0, data.tourl, data.title, data.fullScreen);
            }else {
                window.top.location = data.tourl;
            }
        }else {
            if (data.openPP) {
                data.Inajax
                    ? Levme.popupAjax.showv(data.tourl, undefined, data.fullScreen, 1)
                    : Levme.popupIframe.popupShow(0, data.tourl, data.title, data.fullScreen);
            } else {
                window.location = data.tourl;
            }
        }
    },
    screen:function(id, params) {
        params = params ? params : {};
        id = id ? id : 'notId';

        var timeId = 'timeId' + new Date().getTime();
        var animated = params.animated === undefined ? true : params.animated;
        Levme.screenClose(id, animated, 1);

        var swiperScreen;
        var cls = '.lev-screen.swiper-box.'+ id + '.' +timeId;
        var html = params.html ? params.html : '';

        jQuery('body').append('<div class="lev-screen login-screen force-in virtual-list swiper-box '+id+' '+timeId+'" data-id="'+ id +'" data-cls="'+cls+'">' +
            '<div class="swiper-wrapper virtual-list">' +
            '<div class="swiper-slide"></div><div class="swiper-slide '+timeId+'">'+ html +'</div><div class="swiper-slide"></div></div></div>');

        var closeScreen = function (animated) {
            typeof params.onClose === "function" && params.onClose();
            animated ? swiperScreen.slidePrev() : jQuery(cls).remove();
            //Levme.openedScreens[id] = undefined;
        };

        swiperScreen = myApp.swiper(cls, {
            initialSlide:0,
            speed:400,
            //loop:true,
            direction: params.direction ? 'vertical' : undefined,
            onTransitionStart:function (s) {
            },
            onSlideChangeEnd:function (s) {
                myApp.loginScreen(cls+ '.login-screen', false);
                s.activeIndex !== 1 && closeScreen();
            },
        });

        swiperScreen.slideNext();

        //window.setTimeout(function () { jQuery(cls+ '.login-screen').addClass('force-in'); }, 400);

        typeof params.open === "function" && params.open();
        typeof params.opened === "function" && window.setTimeout(function () { params.opened(); }, 400);
        if (typeof Levme.openedScreens[id] !== "undefined") {
            Levme.openedScreens[id][Levme.openedScreens[id].length] = closeScreen;
        }else {
            Levme.openedScreens[id] = [];
            Levme.openedScreens[id][0] = closeScreen;
        }
        return swiperScreen;
    },
    picker:function(id, pickerHTML, openedFunc) {
        myApp.closeModal('.levpicker-modal.'+ id, false);
        var pickerBox = '<div class="picker-modal levpicker-modal '+ id +'">'+ pickerHTML +'</div>';
        myApp.pickerModal(pickerBox);
        jQuery('body').append('<div class="picker-modal-overlay modal-overlay-visible '+ id +'Overlay"></div>');
        jQuery(document).off('picker:closed', '.picker-modal.'+ id).on('picker:closed', '.picker-modal.'+ id, function () {
            jQuery('.picker-modal-overlay.'+id+'Overlay').remove();
        });
        jQuery(document).off('picker:opened', '.picker-modal.'+ id).on('picker:opened', '.picker-modal.'+ id, function () {
            //jQuery('body').append('<div class="picker-modal-overlay modal-overlay-visible '+ id +'Overlay"></div>');
            typeof openedFunc === "function" && openedFunc();
        });
    },
    getMyCity (getMyProv) {
        if (getMyProv) {
            return Levme.optMyCityStrage(undefined, true);
        }
        return Levme.optMyCityStrage();
    },
    optMyCityStrage (city, prov) {
        var provKey = 'myProv', cityKey = 'myCity';
        if (city === undefined) {
            return prov ? actionLocalStorage(provKey) : actionLocalStorage(cityKey);
        }
        actionLocalStorage(cityKey, city);
        prov !== undefined && actionLocalStorage(provKey, prov);
    },
    setMyCity:function (pm) {
        !actionLocalStorage('myCity') && jQuery('iframe.setMyCity').length <1 && jQuery('body').append(Levme.myCity(0,pm,0,0,1));
    },
    myCity:function (inputCls, pm, def, unLoad, initCity) {
        !pm && (pm = {});
        !pm.id && (pm.id = 'citys');

        var inputValue = jQuery(inputCls).val();
        var city = inputValue ? inputValue : actionLocalStorage('myCity');
        var iframestr = '';
        var mapurl = levToRoute('map', pm);
        var cityInput = '<input type="hidden" name="address" value="' + (city ? city : '') + '">';
        if (city) {
            if (inputCls) {
                window.setTimeout(function () { !inputValue && jQuery(inputCls).val(city); }, 1000);
            }
        }else {
            city = def ? def : '标注城市';
            iframestr = unLoad ? '' : '<iframe class="setMyCity" style="display:none !important;" src="'+mapurl+'"></iframe>';
            if (initCity) return iframestr;
        }
        return '<a class="openPP" data-full="1" href="'+mapurl+'">' +
            '<svg class="icon"><use xlink:href="#fa-weizhi"></use></svg><sas>'+ city +'</sas>' + cityInput + '</a>'+ iframestr;
    },
    APP:{
        init:function () {
            var app = Levme.APP.ckAPP();
            if (!app) return;
            Levme.APP.callInAndroidFunc('initConfig');
            typeof app.hideFab === "function" && app.hideFab();

            if (jQuery('.apkDownBox cir').length > 0) {
                var version = jQuery('.apkDownBox cir').attr('v');
                if (version && typeof app.version === "function" && app.version() != version) {
                    jQuery('.apkDownBox cir').show().removeClass('hiddenx');
                }
            }
        },

        wxLogin () {
            Levme.APP.ckAPP() ? in__android__app.wxLogin() : levtoast('抱歉，请在APP中使用');
        },
        ckAPP:function () {
            if (typeof(in__android__app) != "undefined") {
                return in__android__app;
            }
            return false;
        },
        getSPData (key) {
            var inAndroid = Levme.APP.ckAPP();
            if (!inAndroid || typeof inAndroid.getSPData !== "function") {
                return false;
            }
            return inAndroid.getSPData(key.toString());
        },
        saveData (key, val) {
            var inAndroid = Levme.APP.ckAPP();
            if (!inAndroid || typeof inAndroid.saveData !== "function") {
                return false;
            }
            inAndroid.saveData(key.toString(), val.toString());
            return true;
        },
        appNoticeLive:function (uid) {
            if (typeof(in__android__app) != "undefined") {
                var xxuid = in__android__app.setLoginUID(uid);
                if (xxuid == uid) {
                    in__android__app.getNotification(true);
                }
            }
        },
        phone:0,
        myPhoneLogin:function (phone) {
            if (typeof in__android__app !== "undefined") {
                showIconLoader(true);
                if (phone === true) {
                    var phoneNum = Levme.APP.callInAndroidFunc('getPhoneInfos', 1);
                    if (!phoneNum) {
                        hideIconLoader();
                        Levme.alertMini('获取本机电话号码失败，请确认权限开通且未开启隐私保护', '', function () {
                            Levme.APP.callInAndroidFunc('gotoPermission')
                        }, function () {
                            Levme.APP.callInAndroidFunc('uuidLogin');
                        }, '开通权限', '设备登陆');
                    }else {
                        in__android__app.loginMyPhoneDialog();
                    }
                } else {
                    Levme.APP.phone = phone;
                    phone = Levme.encodeHTML(phone);
                    var uuid = Levme.APP.callInAndroidFunc('getDeviceId');
                    var code = Levme.encodeHTML(actionLocalStorage('androidAppSign'));
                    var myphoneurl = levToRoute('auth/myphone',{id:'levmb', uuid:uuid, phone:phone, code:code, dosubmit:1});
                    Levme.ajaxv.postv(myphoneurl, function (data, status) {
                        status >0 &&
                        window.setTimeout(function () {
                            data.tourl ? (window.top.location = data.tourl) : window.top.location.reload();
                        }, 400);
                    });
                    actionLocalStorage('androidAppSign', '', true);
                }
            }else {
                levtoast('非APP无法使用');
            }
        },
        callInAndroidFunc(funcName, pm1, pm2, pm3, pm4) {
            var inAndroid = Levme.APP.ckAPP();
            if (!inAndroid || typeof inAndroid[funcName] !== "function") {
                return false;
            }
            if (pm4 !== undefined) {
                return inAndroid[funcName](pm1, pm2, pm3, pm4);
            }else if (pm3 !== undefined) {
                return inAndroid[funcName](pm1, pm2, pm3);
            }else if (pm2 !== undefined) {
                return inAndroid[funcName](pm1, pm2);
            }else if (pm1 !== undefined) {
                return inAndroid[funcName](pm1);
            }
            return inAndroid[funcName]();
        },
        clearCache () {
            var InAndroid = Levme.APP.ckAPP();
            if (InAndroid) {
                InAndroid.clearCache();
                ajaxClear();
            }else {
                myApp.confirm('清除本地缓存，包括个人设置、浏览记录等，您确定吗？', '', function () {
                    ajaxClear();
                })
            }
            function ajaxClear() {
                var url = levToRoute('my-setting/clear-cache', {id:'levs'});
                Levme.ajaxv.getv(url, function (data, status) {});
            }
        },
    },
    ziframescreenReload:function () {},
    openziframescreenx:function (obj, _src, creload, titlex, hidetitle) {
        if (typeof creload === "function") {
            Levme.ziframescreenReload = creload;
            creload = 'func';
        }
        openziframescreenx(obj, _src, creload, titlex, hidetitle);
    },
    LoginReferer:false,
    checkLogin:function(not) {
        if (actionLocalStorage('UID') <1 && window.UID <1) {
            !not && openLoginScreen();
            return false;
        }
        return true;
    },
    onOnce(onName, cls, func) {
        onName = onName || 'change';
        jQuery(document).off(onName, cls).on(onName, cls, func);
    },
    onClick:function (cls, func) {
        jQuery(document).off('click', cls).on('click', cls, func);
    },
    dblNum:{},
    onDbClick:function (cls, func, num) {//双击
        var dblNumKey = typeof cls === "string" ? cls : (jQuery(cls).attr('id') || jQuery(cls).attr('class'));
        Levme.dblNum[dblNumKey] = 0;
        Levme.onClick(cls, function () {
            num = parseInt(num) || 1;
            Levme.dblNum[dblNumKey] += 1;
            if (Levme.dblNum[dblNumKey] > num) {
                Levme.dblNum[dblNumKey] = 0;
                func(typeof cls === "string" ? this : cls);
            }
            Levme.setTimeout(function () {
                Levme.dblNum[dblNumKey] = 0;
            }, 500);
        });
    },
    photoShow:function (src, idx) {
        idx = parseInt(idx) || 0;
        src = typeof src === "string" ? [src] : src;
        myApp.photoBrowser({
            photos:src,
            initialSlide: idx,
            theme: 'dark',
            type: 'standalone'
        }).open();
    },
    //eg:Levme.onClick('.msgImgBox .button, .msgImgBox img', function () { Levme.photoShows(this, '.msgImgBox .button, .msgImgBox img'); });
    myPhotoBrowser:{},
    photoShows:function (obj, img) {
        jQuery('.photo-browser.photo-browser-in').remove();
        //typeof Levme.myPhotoBrowser.close === "function" && Levme.myPhotoBrowser.close();
        var photos = [];
        var idx = jQuery(obj).index(img);
        jQuery(img).each(function (n) {
            var src = jQuery(this).attr('src') ? jQuery(this).attr('src') : jQuery(this).attr('title');
            src && photos.push(src.replace('/thumb_320_320_', '/').replace('/thumb_150_0_', '/').replace('/thumb_320_170_', '/'));
        });
        Levme.myPhotoBrowser = myApp.photoBrowser({
            zoom: 400,
            initialSlide: idx,
            photos: photos,
            backLinkText: '<p class="date">上滑关闭</p>',
            ofText: 'of',
            theme: 'dark',
            type: 'standalone',//popup
            // navbarTemplate: '<div class="navbar"><div class="navbar-inner"> <div class="left sliding"> ' +
            //     '<a class="link close-popup photo-browser-close-link ' +
            //     '{{#unless backLinkText}}icon-only{{/unless}} {{js "this.type === \'page\' ? \'back\' : \'\'"}}"> ' +
            //     '<i class="icon icon-back {{iconsColorClass}}"></i>{{#if backLinkText}}<span>{{backLinkText}}</span>{{/if}} </a> ' +
            //     '</div> <div class="center sliding"> <span class="photo-browser-current"></span> ' +
            //     '<span class="photo-browser-of">{{ofText}}</span> <span class="photo-browser-total"></span> </div> ' +
            //     '<div class="right"></div> </div> </div>',
            toolbarTemplate: '<div class="toolbar tabbar"><div class="toolbar-inner">'
                + '<a href="#" class="link photo-browser-prev"><i class="icon icon-prev color-white"></i></a>'
                + '<a class="link dorotate"><svg class="icon"><use xlink:href="#fa-refresh"></use></svg></a>'
                + '<a href="#" class="link photo-browser-next"><i class="icon icon-next color-white"></i></a>'
                + '</div></div>',
        });
        Levme.myPhotoBrowser.open();

        Levme.onClick('.dorotate', function (e, n) {
            var obj = jQuery('.swiper-slide-active .swiper-zoom-container img');//.swiper-zoom-container
            var deg = parseInt(obj.attr('deg'));
            deg = isNaN(deg) ? 90 : deg + 90;
            var tsf = obj.css('transform');
            //console.log(tsf);
            var scl = '';
            if (tsf && tsf.indexOf('3,') > 0) {
                scl = 'scale(3) ';
            }
            obj.css('transform', scl + 'rotate(' + deg + 'deg)').attr('deg', deg);
        });
    },
    formLoader:function (cls, func) {
        cls = cls ? cls : 'form [type=submit]';
        jQuery(document).on('click', cls, function () {
            showIconLoader();
            loopCheckError();
            function loopCheckError() {
                window.setTimeout(function () {
                    if (jQuery('form .has-error').hasClass('has-error')) {
                        hideIconLoader()
                    }else {
                        loopCheckError();
                    }
                }, 500);
            }
            typeof func === "function" && func();
        });
    },
    likes: {
        init:function (opCls) {
            Levme.onClick(opCls ? opCls : '.likesA', function () {
                return Levme.likes.ajaxOp(this);
            });
            Levme.likes.myLikes();
        },
        myLikes:function () {
            var myLikesId = {};
            jQuery('.likesA').each(function () {
                var id = jQuery(this).data('ckid') ? jQuery(this).data('ckid') : jQuery(this).data('link');
                var sg = jQuery(this).data('iden') + id;
                if (actionLocalStorage(sg)) {
                    jQuery(this).addClass('color-red');
                    myLikesId[id] = id;
                }
            });
            return myLikesId;
        },
        ajaxOp:function(obj) {
            if (jQuery(obj).hasClass('noAjax')) {
                return false;
            }
            jQuery(obj).attr('disabled', true);
            var iden = jQuery(obj).data('iden');
            var link = jQuery(obj).data('link');
            var ckid = jQuery(obj).data('ckid');
            jQuery.ajax({
                url: levToRoute('likes/ajax/op'),
                data: {
                    ajax: 1,
                    iden: iden,
                    name: jQuery(obj).data('name'),
                    link: link,
                    ckid: ckid,
                    s: jQuery(obj).attr('s'),
                    _: Math.random()
                },
                dataType: 'json',
                type: 'get',
                success: function (data) {
                    window.setTimeout(function () {
                        jQuery(obj).removeAttr('disabled');
                    }, 2000);
                    if (data.message) {
                        levtoast(data.message);
                    }
                    if (parseInt(data.status) > 0) {
                        var sg = iden + (ckid ? ckid : link);
                        if (data.status === 1) {
                            jQuery(obj).addClass('color-red');
                            actionLocalStorage(sg, 1);
                        } else {
                            jQuery(obj).addClass('color-gray').removeClass('color-red');
                            actionLocalStorage(sg, 1, 1);
                        }
                    } else if (parseInt(data.status) === -5) {
                        openLoginScreen();
                    } else {
                    }
                },
                error: function (data) {
                    window.setTimeout(function () {
                        jQuery(obj).removeAttr('disabled');
                    }, 2000);
                    errortips(data);
                }
            });
            return false;
        },
    },
    deleteUploadImg:function (id, func) {
        showIconLoader();
        jQuery.ajax({
            url: levToRoute('uploads/upload/delete'),
            data:{id:id, _:Math.random()},
            dataType:'json',
            type:'get',
            success:function(data){
                hideIconLoader();
                if (data && data.message) {
                    levtoast(data.message, 5000);
                }
                var status = parseInt(data.status);
                if (status === -5) {
                    openLoginScreen();
                }else if (status > 0) {

                }
                if (typeof func === "function") {
                    func(data);
                }
            },
            error:function(data){
                hideIconLoader();
                errortips(data);
            }
        });
    },
    setImgIdsDelete (id) {Levme.setImgIds(id, true);},
    setImgIdsClear () {Levme.setImgIds(0, 0, true);},
    setImgIds(id, del, clear) {
        var imgIdsInput = jQuery('._imgIds').last();
        var ids = jQuery(imgIdsInput).val();
        if (clear) {
            jQuery(imgIdsInput).val('');
        }else if (del) {
            if (ids) {
                var idsArr = ids.split(',');
                idsArr = arr_remove(id.toString(), idsArr);
                jQuery(imgIdsInput).val(idsArr.join(',') + ',');
            }
        }else if (id) {
            jQuery(imgIdsInput).val(ids + id +',');
        }
    },
    uploadImgForm:function (clsFile, _url, formObj, successFunc, parentBox, notLogin) {
        var uploadIng = false;
        jQuery(document).off('change', clsFile).on('change', clsFile, function(){
            if (!notLogin && !Levme.checkLogin()) return;
            if (uploadIng) {
                showIconLoader(true);
                return false;
            }
            uploadIng = true;
            var obj = this;
            if (jQuery(obj).val()) {
                var formMain = parentBox ? jQuery(this).parents(parentBox).eq(0) : clsFile+'Form';
                var url = _url || jQuery(formMain).attr('form-action');
                showIconLoader(true);
                jQuery(formMain).ajaxSubmit({
                    url: url,
                    data:{tid:jQuery(obj).data('tid'), pid:jQuery(obj).data('pid'), _csrf:_csrf, field:clsFile, _:Math.random()},
                    dataType:'json',
                    type:'POST',
                    processData: false, //需设置为false,因为data值是FormData对象，不需要对数据做处理
                    contentType: false, //需设置为false,因为是FormData对象，且已经声明了属性enctype="multipart/form-data"
                    resetForm: true, //成功提交后，是否重置所有表单元素的值
                    uploadProgress: function (event, position, total, percentComplete) {
                        if(percentComplete >= 100){
                            uploadIng = false;
                            percentComplete = 100;
                        }
                        progress = percentComplete;
                        uploadIngProgressbar(formObj);
                        errortips(event, false);
                    },
                    success: function(data){
                        jQuery(formMain).find('input[type="file"]').val('');
                        uploadIng = false;
                        hideIconLoader();
                        if (data) {
                            if (data.message) {
                                levtoast(data.message);
                            }
                            var status = parseInt(data.status);
                            if (status > 0) {
                                if (jQuery(clsFile + 'Show').length >0 && data.url && data.src) {
                                    jQuery(clsFile +'Show')
                                        .append('<img src="'+
                                            data.url+'" height="40" width="40" data-src="'+
                                            data.src+'" data-dbid="'+
                                            data.dbId+'">');
                                }
                                setImgIds(data.dbId);
                                typeof successFunc === "function" && successFunc(data, formMain);
                            }else if (status === -5) {
                                openLoginScreen();
                            }
                        }else {
                            levtoast('未知错误，上传失败');
                        }
                    },
                    error: function(data) {
                        jQuery(formMain).find('input[type="file"]').val('');
                        uploadIng = false;
                        hideIconLoader();
                        if (Levme.APP.ckAPP() && data.statusText === "error") {
                            //levtoast('图片发送失败，请开启相机权限、存储权限');
                            errortips(data, false);
                        }else {
                            errortips(data);
                        }
                    }
                });
            }
        });

        Levme.onClick(clsFile +'Show img', function () {
            var obj = this;
            var dbid = jQuery(obj).data('dbid');
            if (dbid) {
                Levme.confirm('您确定要删除图片吗？', '', function () {
                    Levme.deleteUploadImg(dbid, function (data) {
                        if (parseInt(data.status) >0) {
                            jQuery(obj).remove();
                            setImgIds(dbid, 1);
                        }
                    });
                });
            }
        });

        function setImgIds(id, del) {
            Levme.setImgIds(id, del);
        }

        var progress = 0;
        function uploadIngProgressbar(boxCls) {
            if (!boxCls) return;
            var container = jQuery(boxCls);
            //don't run all this if there is a current progressbar loading
            if (container.children('.progressbar, .progressbar-infinite').length) return;

            myApp.showProgressbar(container, 0, 'red');

            // Simluate Loading Something
            function simulateLoading() {
                setTimeout(function () {
                    var progressBefore = progress;
                    myApp.setProgressbar(container, progress);
                    if (progressBefore < 100) {
                        simulateLoading(); //keep "loading"
                    } else {
                        jQuery('.progressbar').html('<div class="progressbar-infinite color-multi"></div>');
                        if (!uploadIng) {
                            window.setTimeout(function () {
                                myApp.hideProgressbar(container);
                            }, 1000);
                        }
                    }
                }, Math.random() * 200 + 200);
            }
            simulateLoading();
        }
    },
    setLoadInfiniteDefault:function (infBox) {
        jQuery(infBox).find('.listInfBox').attr('page', "2");
        jQuery(infBox).find('.infiniteStart').html('点击加载更多').attr('disabled', false).removeAttr('disabled');
    },
    setLoadInfiniteNothing:function (infBox, message) {
        jQuery(infBox).find('.listInfBox').attr('page', "-1");
        jQuery(infBox).find('.infiniteStart').html(message ? message : '没有了').attr('disabled', true);
    },
    loadInfiniteAjax:function (infBox, force) {//list-block virtual-list
        var loadInfiniteAjaxForceIng = false;
        if (parseInt(jQuery(infBox).find('.listInfBox').attr('page')) === 2) {
            jQuery(document).on('infinite', infBox, function () {
                if (!jQuery(infBox).find('.infinite-scroll-preloader').hasClass('hiddenx')) {
                    return false;
                }
                parseInt(jQuery(infBox).find('.listInfBox').attr('page')) > 2 && doInfiniteData();
            });
        }
        if (force) {
            if (loadInfiniteAjaxForceIng) {
                showIconLoader(true);
                return false;
            }
            loadInfiniteAjaxForceIng = true;
            doInfiniteData();
            jQuery(infBox).find('.infinite-scroll-preloader').removeClass('hiddenx');
            return false;
        }
        function doInfiniteData() {
            var virtualListBox = jQuery(infBox).find('.listInfBox .virtual-list');
            var page = parseInt(jQuery(infBox).find('.listInfBox').attr('page'));
            jQuery(infBox).find('.infinite-scroll-preloader').removeClass('hiddenx');
            jQuery.ajax({
                url: jQuery(infBox).find('.listInfBox').attr('url') ? jQuery(infBox).find('.listInfBox').attr('url') : '',
                data:{page:page, infinite:1, _:Math.random()},
                dataType:'json',
                type:'get',
                success:function(data){
                    loadInfiniteAjaxForceIng = false;
                    jQuery(infBox).find('.infinite-scroll-preloader').addClass('hiddenx');
                    if (data && data.message) {
                        levtoast(data.message, 5000);
                    }
                    if (parseInt(data.status) === -5) {
                        openLoginScreen();
                    }else if (parseInt(data.status) >0 && data.htms) {
                        jQuery(infBox).find('.listInfBox').attr('page', data.page ? data.page : page+1);
                        virtualListBox.length <1 ?
                            jQuery(infBox).find('.listInfBox').append(
                                data.htms.indexOf('</tr>') >0
                                    ? '<tbody class="virtual-list">'+ data.htms +'</tbody>'
                                    : '<div class="virtual-list">'+ data.htms +'</div>'
                            ) :
                            virtualListBox.append(data.htms);
                    }else if (data.status === -1 || jQuery.trim(data.htms) === "") {
                        Levme.setLoadInfiniteNothing(infBox, data.message);
                    }
                },
                error:function(data){
                    loadInfiniteAjaxForceIng = false;
                    jQuery(infBox).find('.infinite-scroll-preloader').addClass('hiddenx');
                    errortips(data);
                }
            });
        }
    },
    lotteryChartImg:{
        imgBox:'.Html2ImageBox',
        btnObj:null,
        chartIframe:function (obj, code, imgBox) {
            var thisx = Levme.lotteryChartImg;
            thisx.btnObj = obj;
            imgBox && (thisx.imgBox = imgBox);
            var src = levToRoute('lotterys/charts/zs', {editor:1, code:code});
            openziframescreenx(obj, src);
            return false;
        },
        setChartImg:function (src, data) {
            var thisx = Levme.lotteryChartImg;
            if (src) {
                var img = '<img src='+data.localfile+'>';
                typeof window.editor !== "undefined" && window.editor.insertHtml(img);
                jQuery(thisx.imgBox).append('<img class=zsImg src='+data.localfile+'>');
                jQuery(thisx.btnObj).parent().append('<input type="hidden" class="__zsImgSrcs" name="__zsImgSrcs[]" value="'+ data.file +'">');
            }
            Levme.onClick('img.zsImg', function () {
                var obj = this;
                var idx = jQuery(obj).index();
                Levme.confirm('', '删除图片吗？' + idx, function () {
                    jQuery(obj).remove();
                    jQuery('input.__zsImgSrcs').eq(idx).remove();
                });
            });
            myApp.closeModal('.ziframescreen.login-screen');
        },
    },
    marqueeIng:{},
    marquee:function(boxCls, liCls, _h, _n, _speed, _delay, ckH){
        if (Levme.marqueeIng[boxCls]) return 1;
        Levme.marqueeIng[boxCls] = 1;

        liCls = liCls === undefined ? 'li' : liCls;

        var t = null;
        var box = boxCls;
        var boxLi = box + ' '+ liCls;
        var h = _h ? _h : jQuery(boxLi).height();
        var n = _n ? _n : 1;
        var speed = _speed ? _speed : 500;
        var delay = _delay ? _delay : 2000;
        var liLen = jQuery(boxLi).length;
        if (liLen <2) return false;
        if (ckH && jQuery(box).height() >= liLen * jQuery(boxLi).height()) return false;
        jQuery(box).hover(function(){
            clearTimeout(t);
            t = null;
        }, function(){
            t = window.setTimeout(function(){_start(box, h, n, speed)}, delay);
        }).trigger('mouseout');

        //window.setTimeout(function(){_start(box, h, n, speed)}, delay);

        function _start(box, h, n, speed){
            if (t === null || jQuery(box).length <1 || jQuery(box).data('clear')) {
                jQuery(box).removeAttr('data-clear');
            }else {
                t = window.setTimeout(function(){ clearTimeout(t); _start(box, h, n, speed); }, delay);
            }
            jQuery(box).children().eq(0).animate({marginTop: '-='+h}, speed, function(){
                jQuery(this).css({marginTop:'0'}).find(liCls).slice(0,n).appendTo(this);
            })
        }

    },
    jQuery:function (cls, obj) {
        if (obj === 2) return cls;
        cls = typeof cls === "object" //jQuery.isPlainObject(cls)
            ? jQuery(Levme.conf.showPageContainer).find(cls)
            : Levme.conf.showPageContainer +' '+ cls;
        return !obj ? jQuery(cls) : cls;
    },
    tabs:{
        tabShow: {
            loadeds:{},
            ajaxDatas: {},
            virtuallistLoadBoxDatas: {},
            TabDatas:function (tabid, html, virtual) {
                var dkey = Levme.jQuery('').data('page');
                if (virtual) {
                    typeof Levme.tabs.tabShow.virtuallistLoadBoxDatas[dkey] === "undefined" &&
                    (Levme.tabs.tabShow.virtuallistLoadBoxDatas[dkey] = {});
                    if (html === undefined) {
                        return Levme.tabs.tabShow.virtuallistLoadBoxDatas[dkey][tabid];
                    }
                    Levme.tabs.tabShow.virtuallistLoadBoxDatas[dkey][tabid] = html;
                } else {
                    typeof Levme.tabs.tabShow.ajaxDatas[dkey] === "undefined" &&
                    (Levme.tabs.tabShow.ajaxDatas[dkey] = {});
                    if (html === undefined) {
                        return Levme.tabs.tabShow.ajaxDatas[dkey][tabid];
                    }
                    Levme.tabs.tabShow.ajaxDatas[dkey][tabid] = html;
                }
            },
            DataForce:function (tabId) {
                if (Levme.tabs.tabShow.loadeds[tabId]) {
                    Levme.tabs.tabShow.loadeds[tabId] = 0;//console.log('repeat12');
                    //window.setTimeout(function () { myApp.initImagesLazyLoad('.page-content'); }, 103);
                    return;
                }
                Levme.tabs.tabShow.loadeds[tabId] = 1;
                tabId = tabId ? tabId : jQuery(jQuery('.nav-links .tab-link.active').data('tab')).data('id');
                var obj = jQuery('.nav-links .tab-link.tabBtn_'+ tabId);
                if (obj.length >0) {
                    showIconLoader(true);
                    obj.attr('force', 1);
                    jQuery('.tabs .tab.active').removeClass('active');
                    myApp.showTab(obj.data('tab'), false);
                    window.setTimeout(function () { obj.removeAttr('force'); }, 800);
                }
            },
            getTabId:function (tabid) {
                tabid = tabid ? tabid : Levme.jQuery('.mbTabsBox').data('tabid');

                var thisId = jQuery(Levme.conf.showPageContainer).attr('thisShowTabid');
                thisId && jQuery(Levme.conf.showPageContainer).find('.nav-links .tab-link.tabBtn_'+ thisId).length >0 &&
                (tabid = thisId);
                return tabid;
            },
            init:function(deTabId) {
                var ajaxDatas = {}, virtuallistLoadBoxDatas = {};
                var navBoxWidth = jQuery('.nav-links').width();

                deTabId = Levme.tabs.tabShow.getTabId(deTabId);
                var obj = Levme.jQuery('.nav-links .tab-link.tabBtn_'+ deTabId);
                if (obj.length >0) { //showIconLoader(true);
                    window.setTimeout(function () { //myApp.showTab(obj.data('tab'));
                        Levme.tabs.tabShow.DataForce(deTabId);
                    }, 100);
                }

                jQuery(document).off('tab:show', '.tabs .tab').on('tab:show', '.tabs .tab', function () {
                    var objx = this;
                    setTabShowData(objx);
                    Levme.jQuery('.tabs').removeClass('hiddenx');
                    Levme.jQuery('.initLoader.preloader').remove();
                    jQuery(objx).find('.page-content-inner').show();
                    jQuery(objx).find('.initLoader.preloader').hide();
                    jQuery(objx).find('img.lazy').each(function () {
                        jQuery(this).attr('src', jQuery(this).data('src'));
                    });
                    jQuery(objx).find('.toolbar').length ?
                        hideToolbar('.toolbar.common-toolbarx') :
                        showToolbar('.toolbar.common-toolbarx');
                    window.setTimeout(function () {
                        myApp.initImagesLazyLoad('.page-content');
                    }, 480);
                    return false;
                });
                jQuery(document).off('tab:hide', '.tabs .tab').on('tab:hide', '.tabs .tab', function () {
                    setTabHideData(this);
                    return false;
                });

                jQuery(document).off('refresh', '.pull-to-refresh-content.notReload')
                                .on('refresh', '.pull-to-refresh-content.notReload', function (e) {
                    var obj = '.tabs .tab.active';
                    var id = jQuery(obj).data('id');
                    var btnCls = '.tabBtn_' + id;
                    var url = jQuery(btnCls).data('url');

                    showIconLoader(true);
                    getData(url, id, true);
                    Levme.scrollLoad.setDefault('.tab-' + id);
                });

                function setTabShowData(tobj) {
                    var idx = jQuery(tobj).index();
                    var id = jQuery(tobj).data('id');
                    var btnCls = '.tabBtn_' + id;
                    var url = jQuery(btnCls).data('url');

                    jQuery(Levme.conf.showPageContainer).attr('thisShowTabid', id);

                    Levme.jQuery('.nav-links .tab-link.active').removeClass('active');
                    Levme.jQuery(btnCls).addClass('active');
                    levtoMaoCenter(btnCls, '.nav-links .data-table', 300);

                    getData(url, id, Levme.jQuery(btnCls).attr('force'));

                    (idx - 1) > 0 && Levme.jQuery('.tabs .tab:lt(' + (idx - 1) + ')').each(function () {
                        setTabHideData(this);
                    });
                    Levme.jQuery('.tabs .tab:gt(' + (idx + 1) + ')').each(function () {
                        setTabHideData(this);
                    });
                    !Levme.jQuery('.tabs .tab:eq(' + (idx - 1) + ') .listLoadBox').html() && setTabHideData('.tabs .tab:eq(' + (idx - 1) + ')', 1);
                    !Levme.jQuery('.tabs .tab:eq(' + (idx + 1) + ') .listLoadBox').html() && setTabHideData('.tabs .tab:eq(' + (idx + 1) + ')', 1);
                }

                function setTabHideData(tobj, show) {
                    var id = Levme.jQuery(tobj).data('id');
                    var obj = Levme.jQuery(tobj).find('.listLoadBox');
                    if (show) {
                        obj.html(Levme.tabs.tabShow.TabDatas(id));
                    } else {
                        var objBox = obj.find('.virtual-list');
                        if (objBox.html()) {
                            Levme.tabs.tabShow.TabDatas(id, objBox.html(), true);
                            objBox.html('');
                        }
                        if (jQuery.trim(obj.html())) {
                            Levme.tabs.tabShow.TabDatas(id, obj.html());
                            objBox.length <1 && obj.html('');
                        }
                    }
                }

                function getData(url, key, force) {
                    var tabKey = '.tabs .tab.tab-' + key;
                    var htmBox = Levme.jQuery(tabKey + ' .listLoadBox');
                    var keyData = Levme.tabs.tabShow.TabDatas(key);
                    if (!force && keyData) {
                        htmBox.html(keyData);
                        window.setTimeout(function () {
                            var objBox2 = htmBox.find('.virtual-list');
                            objBox2.html(Levme.tabs.tabShow.TabDatas(key, undefined, 1));
                            //myApp.initImagesLazyLoad(tabKey);
                        }, 101);
                        return '';
                    }
                    var listLoadBox = Levme.jQuery(tabKey).find('.listLoadBox');
                    var page = parseInt(listLoadBox.attr('page'));
                    if (!url || page < 0 || parseInt(listLoadBox.attr('not')) === 1) {
                        hideIconLoader();
                        return '';
                    }
                    !force && htmBox.html('<span class="preloader preloader-red"></span>');
                    jQuery.ajax({
                        url: url,
                        data: {_: Math.random()},
                        dataType: 'json',
                        type: 'get',
                        success: function (data) {
                            hideIconLoader();
                            if (data.message) {
                                levtoast(data.message);
                            }
                            var status = parseInt(data.status);
                            if (status > 0) {
                                Levme.tabs.tabShow.TabDatas(key, data.htms);
                                htmBox.html(data.htms);
                                data.htms === "" || data.not
                                    ? Levme.scrollLoad.setNothing(tabKey, data.message)
                                    : Levme.jQuery(tabKey).find('.loadStart.hiddenx').removeClass('hiddenx');

                                //window.setTimeout(function () {myApp.initImagesLazyLoad(tabKey);}, 101);
                            }
                            if (status <1 || data.not) {
                                Levme.scrollLoad.setNothing(tabKey, data.message);
                                htmBox.find('.preloader.preloader-red').hide();
                            }
                        },
                        error: function (data) {
                            hideIconLoader();
                            errortips(data);
                        }
                    });
                }

            }
        }
    },
    scrollLoad:{
        tempPageDatas:{},
        init:function(loadBox) {
            loadBox && parseInt(jQuery(loadBox).find('.listLoadBox').attr('not')) === 1 && Levme.scrollLoad.setNothing(loadBox);

            Levme.onClick('.loadStart', function () {
                var loadBox = Levme.jQuery(jQuery(this).data('box') + '.infinite-scroll', 1);
                if (jQuery(loadBox).length < 1) {
                    loadBox = jQuery(this).parents(jQuery(this).data('box') + '.infinite-scroll');
                    //loadBox = jQuery(this).data('box') + '.infinite-scroll';
                }
                if (jQuery(this).data('box')) {
                    Levme.scrollLoad.ajaxLoad(loadBox, true);//注意空格
                }else {
                    jQuery(this).html('缺少参数[data-box]，无法加载').attr('disabled', true);
                }
            });
        },
        setDefault:function (loadBox) {
            jQuery(loadBox).find('.listLoadBox').attr('page', "2");
            jQuery(loadBox).find('.loadStart').html('点击加载更多').removeClass('hiddenx').attr('disabled', false).removeAttr('disabled');
        },
        setNothing:function (loadBox, message) {
            jQuery(loadBox).find('.infinite-scroll-preloader').addClass('hiddenx');
            jQuery(loadBox).find('.listLoadBox').attr('page', "-1");
            jQuery(loadBox).find('.loadStart').removeClass('hiddenx').html(message ? message : '没有了').attr('disabled', true);
        },
        ajaxLoad:function (loadBox, force) {//list-block virtual-list
            var page = parseInt(jQuery(loadBox).find('.listLoadBox').attr('page'));
            page === -2 && (page = 2) && jQuery(loadBox).find('.listLoadBox').attr('page', page);
            var loadInfiniteAjaxForceIng = false;
            if (page === 2) {
                myApp.attachInfiniteScroll(jQuery(loadBox));//给指定的 HTML 容器添加无限滚动事件监听器
                jQuery(document).on('infinite', loadBox, function () {
                    if (!jQuery(loadBox).find('.infinite-scroll-preloader').hasClass('hiddenx')) {
                        //myApp.detachInfiniteScroll(loadBox);//移除指定容器的无限滚动事件
                        return false;
                    }
                    page >= 2 && doInfiniteData();
                });
            }
            if (force) {
                if (loadInfiniteAjaxForceIng) {
                    showIconLoader(true);
                    return false;
                }
                loadInfiniteAjaxForceIng = true;
                doInfiniteData();
                jQuery(loadBox).find('.infinite-scroll-preloader').removeClass('hiddenx');
                return false;
            }
            function doInfiniteData() {
                var url = jQuery(loadBox).find('.listLoadBox').attr('url');
                var virtualListBox = jQuery(loadBox).find('.listLoadBox .virtual-list');
                virtualListBox = virtualListBox.length <1 ? jQuery(loadBox).find('.listLoadBox.virtual-list') : virtualListBox;
                page = parseInt(jQuery(loadBox).find('.listLoadBox').attr('page'));
                page = isNaN(page) ? 0 : page;
                if (page < 2) return true;
                if (!url) {
                    Levme.scrollLoad.setNothing(loadBox, '未设置[data-url]');
                    return true;
                }
                jQuery(loadBox).find('.infinite-scroll-preloader').removeClass('hiddenx');
                jQuery.ajax({
                    url: url,
                    data:{page:page, infinite:1, _csrf:_csrf, _:Math.random()},
                    dataType:jQuery(loadBox).find('.listLoadBox').attr('jsonp') ? 'jsonp' : 'json',
                    type:'get',
                    success:function(data){
                        loadInfiniteAjaxForceIng = false;
                        jQuery(loadBox).find('.infinite-scroll-preloader').addClass('hiddenx');
                        if (data && data.message) {
                            levtoast(data.message, 5000);
                        }
                        if (parseInt(data.status) === -5) {
                            openLoginScreen();
                        }else if (parseInt(data.status) >0 && data.htms) {
                            var nextPage = data.page ? data.page : page+1;
                            var dataHtms = virtualListBox.length <1 ? '<div class="virtual-list">'+ data.htms +'</div>' : data.htms;
                            Levme.scrollLoad.setTempPageDatas(url, page, dataHtms);
                            jQuery(loadBox).find('.listLoadBox').attr('page', nextPage);
                            virtualListBox.append(dataHtms);
                            window.setTimeout(function () {myApp.initImagesLazyLoad(loadBox);}, 101);
                        }
                        if (data.status === -1 || data.not || jQuery.trim(data.htms) === "") {
                            Levme.scrollLoad.setNothing(loadBox, data.message);
                        }
                    },
                    error:function(data){
                        loadInfiniteAjaxForceIng = false;
                        jQuery(loadBox).find('.infinite-scroll-preloader').addClass('hiddenx');
                        errortips(data);
                    }
                });
            }
        },
        setTempPageDatas(url, page, dataHtms) {
            url = Levme.encodeUrlChinese(url);
            Levme.scrollLoad.tempPageDatas[base64EncodeUrl(url)+'_'+page] = dataHtms;
        },
        getTempPageDatas(url, page) {
            url = Levme.encodeUrlChinese(url);
            return Levme.scrollLoad.tempPageDatas[base64EncodeUrl(url)+'_'+page];
        },
    },
    closePP:function (animated, clearCache, popupCls, btnObj, isActions) {
        if (clearCache) {
            var clsname = jQuery('.refreshPP').attr('clsname');
            Levme.popupAjax.urlPms[clsname] = null;
            Levme.popupAjax.htms[clsname] = null;
        }

        if (!popupCls) {
            popupCls = isActions || jQuery(btnObj).parents('.actions-modal').length >0 ? '.actions-modal' : '.popup';
        }
        myApp.closeModal(popupCls, animated);
        jQuery('.popup-overlay').remove();
        //(animated === false || clearCache) &&
        Levme.setTimeout(function () {
            jQuery('.popup.LevPopupMain.remove-on-close').not('.modal-in').remove();
        }, 404);
    },
    popupAjax:{//无刷新实时记录更新弹窗
        htms:{},
        urlPms:{},
        urlHistory: {//返回上一个 Popup 弹窗 至到剩最后一个。
            isBack:'',
            tempDatas:{},
            backurl (btnObj) {//删除记录并显示上一个弹窗记录
                if (!btnObj) {
                    btnObj = jQuery('.popup.LevPopupMain.modal-in .backHistoryBtn');
                }
                Levme.popupAjax.urlHistory.delurl(jQuery(btnObj).attr('clsname'));

                var keys = Object.keys(Levme.popupAjax.urlHistory.tempDatas);
                if (keys.length < 1) {
                    levtoast('没有了');
                    jQuery(btnObj).addClass('hiddenx');
                    return;
                }
                var clsname = keys.pop();
                Levme.popupAjax.urlHistory.isBack = clsname;
                Levme.popupAjax.urlHistory.delurl(clsname);
                Levme.popupAjax.refreshHtms(clsname, 1);
            },
            seturl (url, clsname) {
                clsname = clsname || Levme.popupAjax.urlHistory.urlToClsname(url);
                Levme.popupAjax.urlHistory.delurl(clsname);
                var urlTotal = Levme.popupAjax.urlHistory.urlTotal();
                if (!Levme.popupAjax.urlHistory.isBack || urlTotal <1) {
                    Levme.popupAjax.urlHistory.tempDatas[clsname] = url;
                }
                if (urlTotal >0) {
                    jQuery('.popup.'+clsname+' .backHistoryBtn').removeClass('hiddenx');
                }else {
                    jQuery('.popup.'+clsname+' .backHistoryBtn').addClass('hiddenx');
                }

                Levme.popupAjax.urlHistory.isBack = '';
            },
            geturl (url, clsname) {
                clsname = clsname || Levme.popupAjax.urlHistory.urlToClsname(url);
                return Levme.popupAjax.urlHistory.tempDatas[clsname];
            },
            delurl (clsname) {
                if (clsname !== undefined) {
                    delete Levme.popupAjax.urlHistory.tempDatas[clsname];
                }
            },
            clearurl () {
                Levme.popupAjax.urlHistory.tempDatas = {};
            },
            urlToClsname (url) {
                return base64EncodeUrl(Levme.encodeUrlChinese(url));
            },
            urlTotal () {
                return Object.keys(Levme.popupAjax.urlHistory.tempDatas).length;
            },
            backHistoryBtna (clsname) {
                return '<a class="backHistoryBtn hiddenx scale6" clsname="'+clsname+'"><svg class="icon"><use xlink:href="#fa-back"></use></svg></a>';
            },
        },
        pushStateListener () {
            jQuery(window).on('popstate', function () {
                Levme.popupAjax.urlHistory.backurl();
                if (Levme.popupAjax.urlHistory.urlTotal() > 0) return false;
            });
            //window.history.pushState('forward', null, '#'); //在IE中必须得有这两行
            //window.history.forward(1);
        },
        clicks () {
            Levme.onClick('.refreshPP', function () {
                var clsname = jQuery(this).attr('clsname');
                Levme.popupAjax.refreshHtms(clsname);
            });
            Levme.onClick('.backHistoryBtn', function () {
                Levme.popupAjax.urlHistory.backurl(this);
                return false;
            });
            //Levme.popupAjax.pushStateListener();
        },
        refreshHtms (clsname, back) {
            if (!Levme.popupAjax.urlPms[clsname]) {
                levtoast(back ? '' : '抱歉，无缓存，刷新失败！');
                return;
            }
            var url  = Levme.popupAjax.urlPms[clsname].url;
            var fullScreen  = Levme.popupAjax.urlPms[clsname].fullScreen;
            var forceRefresh = !back;
            var isActions = Levme.popupAjax.htms[clsname] && Levme.popupAjax.htms[clsname].indexOf('LevActionsMainInner ') >0;
            Levme.popupAjax.showv(url, clsname, fullScreen, forceRefresh, undefined, undefined, isActions);
        },
        showv:function (url, _clsname, fullScreen, forceRefresh, animated, closePPnot, isActions) {
            !closePPnot && Levme.closePP(false, 0,0,0, isActions);
            var clsname = _clsname || Levme.popupAjax.urlHistory.urlToClsname(url);
            var className = jQuery.trim(clsname).replace(/ /g, '.');
            var popupCls = isActions ? jQuery('.'+ className).parents('.actions-modal') : '.popup.' + className;
            if (jQuery(popupCls).length >0) {
                if (forceRefresh) {
                    jQuery(popupCls).remove();
                }else {
                    myApp.popup(popupCls);
                    return;
                }
            }
            var box = isActions ? '.LevActionsMainInner.'+ className : '.LevPopupMain.'+ className + ' .view';
            var forceRefreshes = clsname.indexOf('Live') >=0 || forceRefresh;//是否强制刷新，不优先调用缓存
            clsname = clsname ? clsname : '';
            if (clsname && !forceRefreshes) {
                if (Levme.popupAjax.htms[clsname]) {
                    Levme.popupAjax.openPopupActJs(box, Levme.popupAjax.htms[clsname], animated, url, clsname, isActions);
                    return;
                }
            }
            var ppupHtml = '';
            var boxType = 'popup LevPopupMain ';
            var full = fullScreen ? ' tablet-fullscreen' : '';
            var rtbs = fullScreen === 100 ? 'z-index:911001;' : 'max-height:calc(100% - 40px);z-index:911001;';
            rtbs += 'overflow:unset';
            url = isActions ? changeUrlArg(url, 'isActions', 5) : url;
            showIconLoader(true);
            Levme.ajaxv.getv(changeUrlArg(url, 'ziframescreen', 5), function (data, status) {
                hideIconLoader();
                if (data.error || !data.htms) {
                    !data.message && levtoast('未接收到数据');
                    return;
                }

                var forceRefreshAttr = ' force-refresh-swipe-page=1 ';
                var closeBtn = Levme.popupAjax.urlHistory.backHistoryBtna(clsname)
                        +(!clsname ? '' : '<a class="refreshPP scale6" clsname="'+clsname
                        +'"><svg class="icon"><use xlink:href="#fa-refresh"></use></svg></a>') +
                    '<a class="closePP"><svg class="icon"><use xlink:href="#fa-closer"></use></svg></a>';
                var Htms = data.htms || data.message;
                var PageHtms = jQuery(Htms).hasClass('page') ? Htms : '<div class="page">'+ Htms +'</div>';
                if (isActions) {
                    boxType = 'LevActionsMainInner '+ clsname;
                    ppupHtml = '<div clsname="' + clsname + '" class="'+boxType + '" ' + forceRefreshAttr + ' style="' + rtbs + '">' +
                        '<div class="close-pp">' + closeBtn + '</div>' + PageHtms + '</div>';
                }else {
                    var nextPageUrl = '';
                    if (url.indexOf('nextPageShow') >0) {
                        var nextNum = (parseInt(url.split('nextNum=')[1]) || 0) + 1;
                        nextPageUrl = changeUrlArg(url, 'nextNum', nextNum);
                    }
                    boxType += clsname + full;
                    ppupHtml = '<div clsname="' + clsname + '" class="'+boxType + '" ' + forceRefreshAttr + ' style="' + rtbs + '">' +
                        '<div class="view"><div class="close-pp">' + closeBtn + '</div>' + PageHtms + '</div>' +
                        '<botm class="hiddenx">点击这片区域关闭</botm>' +
                        (nextPageUrl ? '<botm class="nextPopupAjaxBtn" href="'+nextPageUrl+'">翻页</botm>' : '') +
                        '</div>';
                }
                if (clsname && status >0) {
                    Levme.popupAjax.htms[clsname] = ppupHtml;
                    Levme.popupAjax.urlPms[clsname] = {url:url,fullScreen:fullScreen};
                }
                Levme.popupAjax.openPopupActJs(box, ppupHtml, animated, url, clsname, isActions);

                if ( data.assets ) {
                    var assetsBox = 'PopupAssets-'+ data.assetsMd5;
                    jQuery('.'+ assetsBox).length <1 &&
                    jQuery('body').append('<hiddenx class="'+ assetsBox +'">'+ data.assets +'</hiddenx>');
                }
            });

            if ( clsname ) {
                jQuery(document).off('close', '.LevPopupMain.' + clsname).on('close', '.LevPopupMain.' + clsname, function () {
                    if (Levme.popupAjax.htms[clsname] !== null) {//被清理，关闭时不再存储
                        Levme.popupAjax.htms[clsname] =
                            '<div class="popup LevPopupMain ' + clsname + full + '" style="' + rtbs + '">' + jQuery(this).html() + '</div>';
                    }
                });
                var _clsn = '.actions-modal';
                jQuery(document).off('close', _clsn).on('close', _clsn, function () {
                    var _cbox = _clsn+' .LevActionsMainInner.'+ clsname;
                    if (jQuery(_cbox).length >0 && Levme.popupAjax.htms[clsname] !== null) {//被清理，关闭时不再存储
                        var _boxType = 'LevActionsMainInner '+ clsname;
                        Levme.popupAjax.htms[clsname] =
                            '<div clsname="' + clsname + '" class="'+_boxType + '" style="' + rtbs + '">' + jQuery(_cbox).html() + '</div>';
                    }
                });
            }
        },
        openPopupActJs:function (box, htm, animated, url, clsname, isActions) {
            if (isActions || htm) {
                isActions = htm.indexOf('LevActionsMainInner ') >0;
            }
            if (isActions) {
                var btns = [{
                    text: '<style>.actions-modal-label{padding:0}.actions-modal-group{background:#fff}</style><div class="LevActionsMain" style="width:100%">' + htm + '</div>',
                    bold: false,
                    close: false,
                    label: true,
                    //disabled:true,
                    onClick: function () {
                    }
                }];
                myApp.actions([btns]);
            }else {
                myApp.popup(htm, true, animated);
                Levme.popupAjax.urlHistory.seturl(url, clsname);
            }
            Levme.setTimeout(function () {
                jQuery(box).html(jQuery(box).html());//载入js
                Levme.setTimeout(function () {
                    Levme.showNotices();
                    myApp.initImagesLazyLoad('.popup.LevPopupMain');
                    myApp.initImagesLazyLoad('.LevActionsMain');
                }, 10);
            }, 100);
        },
    },
    actions (actinsHtml, onClickFunc) {
        var btns = [{
            text: '<div class="LevActionsLabelMain" style="width:100%">' + actinsHtml + '</div>',
            bold: false,
            close: false,
            label: true,
            //disabled:true,
            onClick: onClickFunc
        }];
        myApp.actions([btns]);
    },
    actionsAlert: {
        showIframe(obj, src, title, reload, animated) {
            myApp.closeModal('.actions-modal', false);
            Levme.popupIframe.popupShow(obj, src, title, false, reload, animated, true);
            return false;
        },
        showAjax(obj, src, title, reload, animated) {
            myApp.closeModal('.actions-modal', false);
            Levme.popupIframe.popupShow(obj, src, title, false, reload, animated, true, true);
            return false;
        },
    },
    popupIframe:{
        config:{},
        ppBox:'',
        iframeBox:'',
        init:function () {
            var clsName = 'LevPopupIframeMain';
            var ppBox = '.' + clsName;
            var open = '.openPP';
            var iframeBox = ppBox + ' iframeBox';
            Levme.popupIframe.ppBox = ppBox;
            Levme.popupIframe.iframeBox = iframeBox;

            Levme.onClick(open, function(){
                myApp.closeModal(ppBox, false);
                Levme.popupIframe.popupShow(this);
                return false;
            });
            Levme.onClick('.nextPopupAjaxBtn', function(){
                var url = jQuery(this).attr('href');
                Levme.popupAjax.showv(url);
                return false;
            });
            Levme.onClick('.openActions', function(){
                Levme.actionsAlert.showIframe(this);
                return false;
            });
            Levme.onClick('.ppBack, .ppClose', function(){
                myApp.closeModal(ppBox, false);
            });
            Levme.onClick('.closePP', function(){
                Levme.closePP(true, undefined, undefined, this);
                parent.Levme.closePP(true, undefined, undefined, this);
            });

            Levme.onClick('.popupFullBtn', function () {
                var obj = jQuery('.popup.modal-in');
                obj.length <1 && (obj = parent.jQuery('.popup.modal-in'));
                obj.hasClass('tablet-fullscreen') ? obj.removeClass('tablet-fullscreen') : obj.addClass('tablet-fullscreen');
            });

            jQuery(document).off('closed', '.actions-modal').on('closed', '.actions-modal', function(){
                if (jQuery('.popup.modal-in').length >0 && jQuery('.popup-overlay.modal-overlay-visible').length <1) {
                    jQuery('body').append('<div class="popup-overlay modal-overlay-visible"></div>');
                }
            });

            Levme.copyright.act();
        },
        popupShow:function (obj, src, title, fullScreen, reload, animated, isActions, inajax) {
            if (obj) {
                src = src ? src : jQuery(obj).data('src');
                src = src ? src : jQuery(obj).attr('href');
                src = src ? src : (jQuery(obj).attr('routev') ? levToRoute(jQuery(obj).attr('routev')) : 0);
                title = title ? title : (jQuery(obj).data('title') || jQuery(obj).attr('title'));
                fullScreen = fullScreen ? fullScreen : jQuery(obj).data('full');
                reload = reload ? reload : jQuery(obj).data('reload');
            }
            if (!src) {
                return true;
            }
            var isInajaxSrc = inajax || src.indexOf('?inajax=') > 0 || src.indexOf('&inajax=') > 0 || src.indexOf('-inajax') > 0;
            if (isInajaxSrc || jQuery(obj).attr('clsname') || jQuery(obj).hasClass('Inajax')) {
                var clsname = jQuery(obj).attr('clsname');
                return Levme.popupAjax.showv(src, clsname, parseInt(jQuery(obj).attr('full')), undefined, animated, undefined, isActions);
            }
            isActions ? myApp.closeModal('.actions-modal', false) : myApp.closeModal('.popup', false);

            var hide = '', style = '', full = '';
            if (!title || jQuery(obj).attr('hidetitle')) {
                hide = ' hiddenx';
                style = 'padding:0 !important;';
            }
            if (fullScreen) {
                full = ' tablet-fullscreen';
            }
            src = changeUrlArg(src, 'ziframescreen', full ? 3 : 2);
            src = isActions ? changeUrlArg(src, 'isActions', 5) : src;
            var _ppBox = isActions ? jQuery('.page .swipesBox').last().parents('.actions-modal') : Levme.popupIframe.ppBox;
            var _iframeBox = isActions ? '.LevActionsIframeMain iframeBox' : Levme.popupIframe.iframeBox;
            var rtbs = 'max-height:calc(100% - 40px);z-index:911001;';//'border-radius: 0 30px 0 0;';
            var iframe = '<iframe width=100% height=100% frameborder=0 marginheight=0 src='+ src +'></iframe>';
            if (isActions) {
                var actinsHtml = '<iframeBox class="content-block" style="margin:0;padding:0;"></iframeBox>';
                var btns = [{
                    text: '<div class="LevActionsIframeMain" style="width:100%">' + actinsHtml + '</div>',
                    bold: false,
                    close: false,
                    label: true,
                    //disabled:true,
                    onClick: function () {
                    }
                }];
                myApp.actions([btns]);
            }else {
                var ppupHtml = '<div class="popup LevPopupIframeMain' + full + '" style="' + rtbs + '">' +
                    '<div class="view"><div class="page">' +
                    '<div class="navbar navbar-bgcolor-red' + hide + '">' +
                    '<div class="navbar-inner">' +
                    '<div class="left"><a class="ppBack link icon-only scale9"><svg class="icon"><use xlink:href="#fa-back"></use></svg></a></div>' +
                    '<div class="center">' + title + '</div>' +
                    '<div class="right"><a class="popupFullBtn link icon-only"><svg class="icon"><use xlink:href="#fa-datac"></use></svg></a>' +
                    '<a class="ppClose link icon-only scale9"><svg class="icon"><use xlink:href="#fa-close"></use></svg></a></div></div>' +
                    '</div>' +
                    '<div class="page-content" style="margin:0 !important;' + style + 'overflow:hidden !important;">' +
                    '<iframeBox class="content-block" style="margin:0;padding:0;"></iframeBox>' +
                    '</div>' +
                    '</div></div>' +
                    '</div>';
                myApp.popup(ppupHtml, true, animated);
            }

            showIconLoader(true);
            jQuery(document).off('opened', _ppBox).on('opened', _ppBox, function(){
                hideIconLoader();
                jQuery(_iframeBox).html(iframe);
            });

            jQuery(document).on('close', _ppBox, function(){
                if (reload) {
                    window.location.reload();
                }
            });

            return false;
        }
    },
    mainView:{},
    conf: {
        pageBack:0,
        pageReinit:0,
        showPageing:null,
        showPageContainer:'.pages .page',
        showPageContainerCls:function () {
            if (Levme.conf.showPageing) {
                return '.pages .' + jQuery(Levme.conf.showPageing.container).attr('class').replace(/ /g, '.');
            }
            return Levme.conf.showPageContainer;
        },
        init:function () {
            //ajax page 调用
            jQuery(document).on('page:afteranimation', function (e) {
                Levme.conf.showPageing = e.detail.page;
                window.setTimeout(function () {
                    Levme.conf.showPageContainer = Levme.conf.showPageContainerCls();
                    var _loadPageAjaxJS = jQuery(Levme.conf.showPageContainer).find('.LoadPageAjaxJS');
                    if (_loadPageAjaxJS.length >0) {//加载JS
                        _loadPageAjaxJS.each(function () {
                            //jQuery(this).html(jQuery(this).prop("outerHTML"));
                            jQuery(this).html(jQuery(this).html());
                        })
                    }
                    window.setTimeout(function () { Levme.showNotices(); }, 102);
                    Levme.tabs.tabShow.init();
                    Levme.scrollLoad.init();
                }, 101);
            });

            jQuery(document).on('page:afterback', function () {
                Levme.conf.pageBack = 1;
            });

            jQuery(document).on('page:reinit', function () {
                Levme.conf.pageReinit = 1;
            });

            Levme.onOnce('change', '.srhSelectChangeBtn', function() {
                var url = jQuery(this).attr('href') || Levme.locationHref();
                var keyName = jQuery(this).attr('name');
                var keyVal = jQuery(this).val();
                window.location = changeUrlArg(url, keyName, keyVal);
            });

            Levme.onClick('.loopUrlBtn', function () {
                var url = jQuery(this).attr('href');
                var confirmmsg = jQuery(this).attr('confirmmsg');
                var toastmsg = jQuery(this).attr('toastmsg');
                var loopsec = parseInt(jQuery(this).attr('loopsec')) || 1000;
                if (!url) {
                    levtoast('未设置元素href的值');
                    return false;
                }
                if (confirmmsg) {
                    myApp.confirm(confirmmsg, '', function() {
                        loopUrl(url);
                    })
                } else {
                    loopUrl(url);
                }
                if (toastmsg) {
                    levtoast(toastmsg);
                }
                function loopUrl(url) {
                    showIconLoader(true);
                    Levme.ajaxv.getv(url, function (data, status) {
                        if (data.loopUrl) {
                            Levme.setTimeout(function() {
                                loopUrl(data.loopUrl);
                            }, loopsec)
                        }
                    })
                }
                return false;
            });

            Levme.onClick('.showTitleBtn', function () {
                var title = jQuery(this).attr('title') || jQuery(this).attr('alt') || jQuery(this).attr('placeholder');
                levtoast(title);
            });

            Levme.onClick('.ajaxBtn', function () {
                var url = jQuery(this).attr('href');
                var target = jQuery(this).attr('target');
                var confirmmsg = jQuery(this).attr('confirmmsg');
                var toastmsg = jQuery(this).attr('toastmsg');
                if (confirmmsg) {
                    myApp.confirm(confirmmsg, '', function () {
                        ajaxBtnFunc(url, target);
                    })
                }else if (toastmsg) {
                    levtoast(toastmsg);
                }else {
                    ajaxBtnFunc(url, target);
                }
                return false;
            });
            function ajaxBtnFunc(url, target) {
                if (!url) return;
                url = changeUrlArg(url, 'ajaxBtn', 1);
                if (target === '_top') {
                    window.top.location = url;
                    return;
                }

                showIconLoader(true);
                Levme.ajaxv.getv(url, function (json, status) {
                    if (json.confirmurl) {
                        myApp.confirm(json.message, '', function () {
                            Levme.ajaxv.getv(json.confirmurl, function (data, status) {
                            }, {confirmdoit:1});
                        });
                        return false;
                    }
                    json.message && levtoast(json.message);
                    status === -5 && openLoginScreen();
                }, {inajax:1});
            }

            Levme.onClick('.formFieldBox .bigObjx input[type=checkbox]', function () {
                setFormFieldCheckBoxStrValue(this);
            });
            jQuery(document).off('change', '.formFieldBox .bigObjx .checkBoxStrValue')
                .on('change', '.formFieldBox .bigObjx .checkBoxStrValue', function () {
                    var strValues = jQuery(this).val();
                    var box = jQuery(this).parents('.bigObjx').eq(0);
                    var obj = box.find('input[type=checkbox]');
                    if (strValues) {
                        var valarr = strValues.split(',');
                        obj.each(function () {
                            if (valarr.indexOf(this.value) >=0) {
                                jQuery(this).prop('checked', true);
                            }else {
                                jQuery(this).prop('checked', false);
                            }
                        });
                    }
                });

            function setFormFieldCheckBoxStrValue(inputObj) {
                var val = jQuery(inputObj).val();
                var box = jQuery(inputObj).parents('.bigObjx').eq(0);
                var obj = box.find('.checkBoxStrValue');
                if (obj.length > 0) {
                    var strval = jQuery(obj).val();
                    strval = strval ? strval.split(',') : [];
                    if (jQuery(inputObj).is(':checked')) {
                        strval.push(val);
                    } else {
                        strval = arr_remove(val, strval);
                    }
                    jQuery(obj).val(strval.join(','));
                }
            }

            Levme.onClick('.setBigBox', function () {
                var box = jQuery(this).parents('.card-footer').eq(0);
                var obj = box.find('.bigObjx');
                if (obj.hasClass('bigBox')) {
                    obj.removeClass('bigBox');
                    jQuery(this).html('<absxk>放大</absxk>');
                    box.find('.hint-block').show();
                }else {
                    obj.addClass('bigBox');
                    jQuery(this).html('<absxg>缩小</absxg>');
                    box.find('.hint-block').hide();
                }
            });

//开关按钮设置值，将checkbox 当radio使用
            jQuery(document).on('change', '.setToggleValue input[type=checkbox]', function () {
                var ckval = jQuery(this).parents('.setToggleValue').eq(0).find('input[type=hidden]').val();
                ckval = ckval ==="" || parseInt(ckval) ===0 || !ckval ? 1 : 0;
                if ( jQuery(this).is(':checked') ) {
                    jQuery(this).parents('.setToggleValue').eq(0).find('input[type=hidden]').val(ckval);
                }else {
                    jQuery(this).parents('.setToggleValue').eq(0).find('input[type=hidden]').val(ckval);
                }
            });

            Levme.mainView = myApp.addView('.view-main', {
                // Because we use fixed-through navbar we can enable dynamic navbar
                //dynamicNavbar: true,
                // Enable Dom Cache so we can use all inline pages
                //domCache: true
            });

            //Pull to refresh content 下拉刷新
            jQuery(document).on('pullstart', '.pull-to-refresh-content', function (e) {
                jQuery('.pull-to-refresh-layer .pull-to-refresh-arrow').show();
                window.setTimeout(function () {
                    myApp.pullToRefreshDone();
                    jQuery('.pull-to-refresh-layer .pull-to-refresh-arrow').hide();
                }, 2000);
            });
            jQuery(document).on('refresh', '.pull-to-refresh-content', function (e) {
                if (!jQuery(this).hasClass('notReload')) {
                    showIconLoader();
                    window.setTimeout(function () {
                        //window.location.href = window.location.href;
                        window.location.reload();
                        myApp.pullToRefreshDone();
                        jQuery('.pull-to-refresh-layer .pull-to-refresh-arrow').hide();
                    }, 1000);
                }
            });

            Levme.onClick('a, ax, input[type="button"]', function(){
                if (jQuery(this).hasClass('openPP')) return true;

                var _thrf = jQuery(this).attr('href');
                if (jQuery(this).hasClass('is_ajax_a')) {
                    myApp.closeModal('.login-screen', false);
                }
                var aonclick = jQuery(this).attr('onclick');
                if (aonclick && aonclick.indexOf('showWindow') !==-1) return false;
                var _bk = jQuery(this).attr('_bk');
                if (!_bk && _thrf) {
                    if (typeof (siteUri) != 'undefined' && (_thrf + '/').indexOf(siteUri) === -1 && _thrf.indexOf('http') === 0) {
                        return aToLoginScreenForce(this, _thrf);
                    }
                    if (_thrf.indexOf('javascript:') < 0 && jQuery(this).attr('target') === "_blank") {
                        return aToLoginScreenForce(this, _thrf);
                    }
                }

            });

            Levme.onClick('ax, input[type="button"]', function(){
                if (jQuery(this).hasClass('openPP')) return true;
                var _thrf = jQuery(this).attr('href');
                if (jQuery(this).hasClass('is_ajax_a')) {
                    Levme.mainView.router.loadPage(_thrf);
                    return false;
                }else if (_thrf) {
                    window.location = _thrf;
                }
            });

            Levme.onClick('.closescreen', function(){
                Levme.closePP(false);
                myApp.closeModal('.login-screen,.login_screen');
            });

            Levme.onClick('.editField', function () {
                var pobj   = this;
                var url    = jQuery(pobj).attr('href');
                var opid   = jQuery(pobj).attr('opid');
                var opname = jQuery(pobj).attr('opname');
                var opval  = jQuery(pobj).attr('opval');
                var title  = jQuery(pobj).attr('title');
                myApp.prompt(title || '修改:'+ opname, function (opval) {
                    jQuery('.modal-inner .input-field inputs input').each(function () {
                        url = changeUrlArg(url, this.name, this.value || '');
                    });
                    Levme.ajaxv.getv(changeUrlArg(url, opname, opval), function (data, status) {
                        if ( status >0 ) {
                            jQuery(pobj).parent().find(opname).html(opval);
                            jQuery('.ET_'+ opname + opid).html(opval);
                            jQuery(pobj).attr('opval', opval);
                        }
                        data.tourl && window.setTimeout(function () {window.location = data.tourl}, 1000);
                    });
                });
                window.setTimeout(function () {
                    jQuery('.modal-inner .modal-text-input').val(opval).attr('placeholder', title);
                    jQuery(pobj).find('inputs').length >0 &&
                    jQuery('.modal-inner .input-field').append('<inputs>'+ jQuery(pobj).find('inputs').html() + '</inputs>');
                    jQuery('.modal-inner .input-field inputs input').addClass('modal-text-input');
                }, 500);
                return false;
            });
        },
        onClick:function (cls, func) {
            jQuery(document).off('click', cls).on('click', cls, func);
        },
    },
    tablesnav:{
        init:function () {
            Levme.tablesnav.addTabTr();
            Levme.tablesnav.openChild();
            Levme.tablesnav.addChild();
            Levme.tablesnav.delChild();
            Levme.onClick('.bigvBtn', function () {
                Levme.tablesnav.setBigv(jQuery(this).parents('tabbox'));
            });
            actionLocalStorage('tabboxBigv') && jQuery('tabbox').addClass('bigv');

            Levme.tablesnav.smartSelect();
        },
        smartSelect:function () {
            var smartSelectObj = null;
            var popupBox = '.popup.smart-select-popup ';
            jQuery(document).on('keyup', popupBox+' input[type=search]', function (e) {
                var val = this.value;
                var key = e.keyCode ? e.keyCode : e.which;
                if (this.value && key == '13') {
                    var trid = '.trid-'+jQuery(popupBox+' hiddenx idk').html();
                    var inputName = jQuery(popupBox+' hiddenx na').html();//console.log(inputName);

                    var opt = jQuery(smartSelectObj).find('select optgroup').length >0
                        ? jQuery(smartSelectObj).find('select optgroup').last()
                        : jQuery(smartSelectObj).find('select');
                    myApp.smartSelectAddOption(opt, '<option value="'+val+'" selected>'+val+'</option>');
                    jQuery(smartSelectObj).find('select[name="'+inputName+'"]').val(val);//console.log(val);
                    jQuery(smartSelectObj).find('.item-after').html(val);
                    Levme.showNotices('已设置为['+ val +'] 保存后生效', 'success', 1500);
                }
            });
            Levme.onClick('.smart-select', function () {
                smartSelectObj = this;
            })
        },
        setBigv:function (objBox) {
            if ( objBox.hasClass('bigv') ) {
                objBox.removeClass('bigv');
                actionLocalStorage('tabboxBigv', 0, 1);
            } else {
                objBox.addClass('bigv');
                actionLocalStorage('tabboxBigv', 1);
            }
        },
        getMaxId:function (obj) {
            var maxNum = 0;
            jQuery(obj).each(function () {
                var num = parseInt(this.value);
                num = isNaN(num) ? 0 : num;
                maxNum = num > maxNum ? num : maxNum;
            });
            return maxNum;
        },
        addTabTr:function () {
            Levme.onClick('.addRowBtn', function () {
                jQuery('#'+ jQuery(this).attr('clickfor')).click();
            });
            Levme.onClick('.addTabTr', function () {
                var inId = Levme.tablesnav.getMaxId(jQuery(this).parents('tabbox').find('td.fd-id input')) +1;

                var inputname = jQuery(this).data('input');
                var trBox = '.newTabTrBox-'+ inputname;
                var trCon = jQuery(this).parents('.myAddTrx').eq(0).html();
                trCon && (trCon = trCon.replace(/__addtr\[/g, '[').replace(/\[\]\[\]/g, '[idk___'+inId+'][]'));
                jQuery(trBox).append('<tr>'+trCon+'</tr>');
                jQuery(trBox+ ' tr').last().find('td').last().html('<a class="date delTr">x</a>');

                jQuery(trBox+ ' tr').last().find('td.fd-id input').val(inId);
                jQuery(trBox+ ' tr').last().find('td.fd-order input').val(inId);
            });
        },
        openChild:function () {
            Levme.onClick('.tablesnavOpenChildBtn', function () {
                var cls = jQuery(this).data('cls');
                var obj = jQuery('.childBox_'+ cls);
                obj.hasClass('hiddenx') ? obj.removeClass('hiddenx') : obj.addClass('hiddenx');
            });
        },
        addChild:function () {
            Levme.onClick('.tablesnavAddChildBtn', function () {
                var pId = Levme.tablesnav.getMaxId(jQuery(this).parents('tabbox').find('td.fd-id input'));
                var inId = pId +1;

                var cls     = jQuery(this).data('cls');
                var obj     = '.childBox_'+ cls;
                var trBox   = jQuery('.navBox_'+ cls);
                var trCon   = trBox.html();
                trCon && (trCon = trCon.replace(/__addtr\[/g, '[')
                    .replace(/tables\[/g, 'tables[cld__'+ cls +'__')
                    .replace(/tablesFormv\[/g, 'tablesFormv[cld__'+ cls +'__')
                    .replace(/\[\]\[\]/g, '[idk___'+inId+'][]')
                    .replace(new RegExp("\\[idk___" + trBox.data('idk') +"\\]\\[\\]", 'g'), '[idk___'+inId+'][]'));
                jQuery(obj + '.Navsx').append('<tr class="add-c">'+trCon+'</tr>');
                jQuery(obj + '.Navsx tr').last().find('td').eq(0).html('<p class="date">&angrt;</p>');

                jQuery(obj + '.Navsx tr').last().find('td.fd-id input').eq(0).val(inId);
                jQuery(obj + '.Navsx tr').last().find('td.fd-order input').eq(0).val(inId);
            });
        },
        delChild:function () {
            Levme.onClick('.delTr', function () {
                var cls = jQuery(this).data('cls');
                jQuery(this).parents('tr').eq(0).remove();
                if (jQuery(this).parents('tr.navBox_' + cls).length > 0) {
                    jQuery('tbody.childBox_'+ cls).remove();
                }
            });
        }
    },
    dosaveForm:{
        inited:false,
        init:function () {
            if ( !Levme.dosaveForm.inited ) {
                Levme.script.ajaxFormJs();
                Levme.dosaveForm.inited = 1;
            }
            Levme.onClick('.dosaveFormBtn', function () {
                return Levme.dosaveForm.dosubmit(jQuery(this).parents('#saveForm').eq(0));
            });
            Levme.onClick('.submitFormvBtn', function () {
                return Levme.dosaveForm.dosubmitV2(this);
            });
        },
        dosubmit:function (formObj, func) {
            formObj = formObj ? formObj : '#saveForm';
            showIconLoader(true);
            jQuery(formObj).ajaxSubmit({
                url: jQuery(formObj).attr('action') ? jQuery(formObj).attr('action') : '',
                data: {dosubmit:1, _csrf:_csrf, inajax:1, _:Math.random()},
                type:'post',
                dataType: 'json',
                success: function(data){
                    hideIconLoader();
                    var status = parseInt(data.status);
                    if (status === -5) {
                        openLoginScreen();
                        return false;
                    }
                    if (status >0) {
                        levtoast(data.message);
                        !func && !data.notReload &&
                        window.setTimeout(function () {
                            var reload = parseInt(data.reload);
                            if (reload === 4) {
                                window.location = Levme.locationHref();
                            }else if (reload === 3) {
                                window.location.reload();
                            }else if (reload === 2) {
                                window.top.location.reload();
                            }else {
                                data.tourl ?
                                    (data.local ? window.location = data.tourl : window.top.location = data.tourl) :
                                    (window.location = window.location);
                            }
                        }, 400);
                    }else if (data && data.message) {
                        levtoast(data.message, 15000);
                    }
                    showFormErrors(data.errors);

                    typeof func === "function" && func(data, status);
                },
                error: function(data) {
                    hideIconLoader();
                    errortips(data);
                }
            });
            return false;
        },
        dosubmitIng:{},
        dosubmitV2 (submitBtnObj, formObj, formAction, func, _data) {
            submitBtnObj    = submitBtnObj  || '.submitFormvBtn';
            formObj         = formObj       || '.FormvMain';
            formAction      = formAction    || jQuery(formObj).attr('action');

            if (Levme.dosaveForm.dosubmitIng[formAction]) {
                console.log('正在提交：', Levme.dosaveForm.dosubmitIng[formAction]);
                showIconLoader(true);
                return;
            }
            Levme.dosaveForm.dosubmitIng[formAction] = [submitBtnObj, formObj, formAction, func];

            showIconLoader(true);
            var data = _data || {};
            data.dosubmit = 1;
            data['_csrf'] = _csrf;
            data.inajax = 1;
            data['_'] = Math.random();
            jQuery(formObj).ajaxSubmit({
                url: formAction,
                data: data,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    hideIconLoader();
                    Levme.dosaveForm.dosubmitIng[formAction] = null;

                    var status = parseInt(data.status);
                    if (status === -5) {
                        openLoginScreen();
                        return false;
                    }
                    if (status > 0) {
                        Levme.showNotices(data.message);
                        !func && !data.notReload &&
                        window.setTimeout(function () {
                            var reload = parseInt(data.reload);
                            if (reload === 3) {
                                window.location.reload();
                            }else if (reload === 2) {
                                window.top.location.reload();
                            }else {
                                data.tourl ?
                                    (data.local ? window.location = data.tourl : window.top.location = data.tourl) :
                                    (window.location = window.location);
                            }
                        }, 400);
                    } else if (data && data.message) {
                        Levme.showNotices(data.message, 'error', 151000);
                    }
                    showFormErrors(data.errors);

                    typeof func === "function" && func(data, status);
                },
                error: function (data) {
                    Levme.dosaveForm.dosubmitIng[formAction] = null;

                    hideIconLoader();
                    errortips(data);
                }
            });
            return false;
        }
    },
    openWX: function () {
        var appObj = ckAPP();
        if (appObj !== false) {
            appObj.openWX();
        }else {
            window.location = 'weixin://';
        }
    },
    openQQ: function () {
        var appObj = ckAPP();
        if (appObj !== false) {
            appObj.openQQ();
        }else {
            window.location = 'mqq://';
        }
    },
    openAlipay: function () {
        var appObj = ckAPP();
        if (appObj !== false) {
            //appObj.openAlipay();
            appObj.openAlipayQrcode("");
        }else {
            window.location = 'alipays://';
        }
    },
    shareWsa(phone) {
        const curHref = Levme.locationHref();
        // content是自己定义的一些需要分享的内容;
        let shareStr = jQuery('title').first().text();
        let content = phone ? shareStr : shareStr;
        content += '\n' + curHref;
        if (jQuery('body.mobile-1').length <1) {
            levtoast('请在手机上进行操作');
        } else {
            // 调起 WhatsApp,自己选择联系人进行内容分享
            let shareLink = 'whatsapp://send?text=' + encodeURIComponent(content);
            // 调起 WhatsApp 给指定手机号发消息
            let toSomeoneLink = 'whatsapp://send?phone=' + phone + '&text=' + encodeURIComponent(content);
            window.location.href = phone ? toSomeoneLink : shareLink;
        }
    },
    shareFbk(phone) {
        const curHref = Levme.locationHref();
        // content是自己定义的一些需要分享的内容;
        let shareStr = jQuery('title').first().text();
        let content = phone ? shareStr : shareStr;
        content += "\n" + curHref;
        if (jQuery('body.mobile-1').length <1) {
            levtoast('请在手机上进行操作');
        } else {
            window.location.href = 'fb://share/?link=' + encodeURIComponent(content);
        }
    },
    shareTlg(phone) {//toSomeoneLink
        const curHref = Levme.locationHref();
        // content是自己定义的一些需要分享的内容;
        let shareStr = jQuery('title').first().text();
        let content = phone ? shareStr : shareStr;
        content += "\n" + curHref;
        if (jQuery('body.mobile-1').length <1) {
            levtoast('请在手机上进行操作');
        } else {
            window.location.href = 'tg://msg?text='+encodeURIComponent(content)+'&to=+' + phone;
        }
    },
    checkXssCode:function (a) {
        a += "";
        return a.indexOf('<') >=0 || a.indexOf('>') >=0 || a.indexOf('(') >=0 || a.indexOf(')') >=0 || a.indexOf('"') >=0 || a.indexOf("'") >=0;
    },
    encodeHTML: function(a){
        a += "";
        return a.replace(/&/g, "&0amp;").replace(/</g, "&0lt;").replace(/>/g, "&0gt;").replace(/"/g, "&0quot;").replace(/'/g, "&0apos;")
            .replace(/\(/g, '&99z;').replace(/\)/g, '&00z;');
    },
    decodeHTML: function(a){
        a += "";
        return a.replace(/&amp;/g, "&").replace(/&0amp;/g, "&").replace(/&0lt;/g, "<").replace(/&0gt;/g, ">").replace(/&0quot;/g, '"').replace(/&0apos;/g, "'")
            .replace(/&99z;/g, '(').replace(/&00z;/g, ')');
    },
    iconfonts:function () {
        var icons = {};
        jQuery('svg:not(.icon) symbol').each(function (e) {
            icons[this.id] = this.id;
        });
        return icons;
    },
    iconSelectWin:function (func, btnObj) {
        var icons = Levme.iconfonts();
        var htm = '<div class="iconSelectWinBox flex-box" style="flex-wrap:wrap;height:100px;width:300px;overflow:auto;">';
        for (var k in icons) {
            htm += '<a class="copyBtn" copy-txt="'+icons[k]+'" style="margin:5px;color:#fff;" title="'+icons[k]
                +'"><svg class="icon" style="font-size:18px"><use xlink:href="#'+icons[k]+'"></use></svg></a>';
        }
        htm += '</div>';
        Levme.confirm(htm,'', function () {
            var value = jQuery('.iconSelectWinBox a.icond').attr('title');
            jQuery(btnObj).parent().find('input').last().val(value);
            if (func) {
                if (typeof func === "function") {
                    func(value);
                }else {
                    jQuery(func).val(value);
                }
            }
        });
    },
    iconSelectWinBtnReg:function (func) {
        Levme.onClick('.iconSelectWinBox a', function () {
            jQuery('.iconSelectWinBox a').removeClass('icond');
            jQuery(this).addClass('icond');
            return false;
        });
        Levme.onClick('.iconSelectWinBtn', function () {
            Levme.iconSelectWin(func ? func : jQuery(this).data('funcname'), this);
        });
    },
    disableBack:function () {
        //禁止后退页面
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
    },
    checkbox:function (cls, func) {
        /*
        使用
        <label class="ckdbox childsCkbox opCkboxBtn" val="2">
            <svg class="icon icon-ckbox white"><use xlink:href="#fa-ckd"></use></svg>&nbsp;
            全部安装
        </label>
        <svg class="icon icon-ckbox childsCkbox" val="1"><use xlink:href="#fa-ckd"></use></svg>
        */
        Levme.onClick(Levme.jQuery(cls, 1), function () {
            var ckd = jQuery(this).hasClass('ckd');
            if (jQuery(this).hasClass('opCkboxBtn')) {
                !ckd ? Levme.jQuery(cls).addClass('ckd') : Levme.jQuery(cls).removeClass('ckd');
            }
            if (ckd) {
                jQuery(this).removeClass('ckd');
            }else {
                jQuery(this).addClass('ckd');
            }

            typeof func === "function" && func(Levme.checkboxvals(cls), ckd, this);
        });
    },
    checkboxvals: function (cls) {
        var ckdvals = [];
        var val = Levme.jQuery(cls +'.opCkboxBtn').attr('val');
        val !== undefined && ckdvals.push(val);
        jQuery(cls +'.ckd').not('.opCkboxBtn').each(function (n) {
            var val = jQuery(this).attr('val');
            val !== undefined && ckdvals.push(val);
        });
        return ckdvals;
    },
    textSelect:function (inputname) {
        var cls = '._'+ inputname + '_slt';
        var slt = '.on'+ inputname + 'Slt';
        var opCls = Levme.jQuery(cls, 1);
        var onSlt = Levme.jQuery(slt, 1);

        if (jQuery(opCls).length < 1) opCls = cls;
        if (jQuery(onSlt).length < 1) onSlt = slt;

        jQuery(document).on('blur', onSlt, function () {
            var sval = this.value;
            if (sval === "") return;

            var sltd = false;
            var opClsBlur = jQuery(this).parents('.textSelectBox').find('.textSelectSlt');
            opClsBlur.find('option').each(function () {
                if (sval === jQuery(this).text()) {
                    sltd = true;
                    sval = jQuery(this).attr('value');
                    return null;
                }
            });
            if (!sltd) {
                if (opClsBlur.find('.myselfop').length > 0) {
                    opClsBlur.find('.myselfop').attr('value', sval).html(sval);
                }else {
                    jQuery(opClsBlur).append('<option class="myselfop red" value="' + sval + '" selected>' + sval + '</option>');
                }
            }
            jQuery(opClsBlur).val(sval);
        });

        jQuery(document).on('change', opCls, function () {
            setonSlt(jQuery(this).find('option:selected').text(), jQuery(this).parents('.textSelectBox').find('.textSelectTxt'));
        });

        !jQuery(onSlt).val() && jQuery(opCls).find('option:selected').text() &&
        setonSlt(jQuery(opCls).find('option:selected').text());
        function setonSlt(sval, obj) {

            jQuery(obj ? obj : onSlt).val(sval);
        }
    },
    script: {
        loaded:{},
        get(url, func, cache) {
            jQuery.ajax({
                url: url,
                type: "GET",
                cache: cache,
                success: function () {
                    typeof func === "function" && func();
                },
                dataType: "script"
            });
        },
        check:function (key) {
            if (Levme.script.loaded[key]) {
                console.log('loaded：', key);
                return true;
            }
            Levme.script.loaded[key] = 1;
            return false;
        },
        /**
         * @return {boolean}
         */
        ClipboardJS:function (func) {
            if (Levme.script.check('ClipboardJS')) return true;

            typeof func !== "function" && (func = function () {});
            typeof ClipboardJS === "undefined"
                ? Levme.script.get(assets + '/statics/common/clipboard.min.js', function () { func(); }, true) : func();
        },
        markeDownJs:function (func) {
            if (Levme.script.check('marked.min.js')) return true;

            typeof func !== "function" && (func = function () {});
            if ( typeof marked === "undefined" ) {
                jQuery('body').append('<link rel="stylesheet" type="text/css" href="'+ assets + '/statics/common/markdown.css" />');
                Levme.script.get(assets + '/statics/common/marked.min.js', function () {
                    marked.setOptions({
                        renderer: new marked.Renderer(),
                        highlight: function(code, lang) {
                            //const hljs = require('highlight.js');
                            if (typeof hljs !== "undefined") {
                                const language = hljs.getLanguage(lang) ? lang : 'plaintext';
                                return hljs.highlight(code, {language}).value;
                            }
                        },
                        langPrefix: 'hljs language-', // highlight.js css expects a top-level 'hljs' class.
                        pedantic: false,
                        gfm: true,
                        breaks: true,//（单个回车换行）默认 false
                        sanitize: false,//忽略html
                        smartLists: true,
                        smartypants: false,
                        xhtml: false
                    });
                    func();
                }, true);
            }else {
                func();
            }
        },
        ajaxFormJs:function (func) {
            if (Levme.script.check('jquery.form.min.js')) return true;

            typeof func !== "function" && (func = function () {});
            typeof jQuery('d').ajaxSubmit === "undefined"
                ? Levme.script.get(assets + '/statics/common/jquery.form.min.js', function () { func(); }, true) : func();
        },
        QRCode:function (func) {
            if (Levme.script.check('QRCode')) return true;

            typeof func !== "function" && (func = function () {});
            typeof QRCode === "undefined"
                ? Levme.script.get(assets + '/statics/common/qrcode.min.js', function () { func(); }, true) : func();
        },
        Highcharts:function (func, theme) {
            if (Levme.script.check('Highcharts')) return true;
            typeof func !== "function" && (func = function () {});
            typeof Highcharts === "undefined"
                ? Levme.script.get(assets + '/statics/common/highstock.js', function () { loadjs(); }, true) : loadjs();

            function loadjs() {
                if (theme) {
                    Levme.script.get(assets + '/statics/common/highstock-dark-unica.js', function () { func(); }, true);
                }else {
                    func();
                }

            }
        },
        loadOnce (src, func, checkFuncName) {
            if (Levme.script.check(src)) return true;
            typeof func !== "function" && (func = function () {});
            if (!checkFuncName || typeof window[checkFuncName] === "undefined") {
                Levme.script.get(src, function () { func(); }, true);
            }else {
                func();
            }
        },
        loadOnceAndActionFunc(src, func, checkFuncName) {
            if ( Levme.script.loadOnce(src, func, checkFuncName) === true) {
                typeof func === "function" && func();
            }
        },
    },
    setQrcode(boxid, txt, clsname, width, height) {
        width = width || 148;
        height = height || width;
        jQuery('#'+ boxid).html('').removeClass('hiddenx');
        jQuery('.'+ clsname).removeClass('hiddenx');
        new QRCode(boxid, {
            text: txt,
            width: width,
            height: height,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });
    },
    clipboardv:function (cls) {
        cls = cls || '.copyBtn';
        Levme.script.ClipboardJS(initCopy) && initCopy();

        function initCopy() {
            var clipboard = new ClipboardJS(cls, {
                text: function(e) {
                    var copyTxt = jQuery(e).attr('copy-txt');
                    if (!copyTxt) {
                        var copyInput = jQuery(e).attr('copy-input');
                        if (copyInput) {
                            copyTxt = typeof copyInput === "function" ? copyInput(e) : jQuery(jQuery(e).attr('copy-input')).val();
                        }else if (jQuery(e).find('tips').text() === '复制') {
                            copyTxt = jQuery(e).text().replace(/复制/g, '');
                        }
                    }
                    return copyTxt;
                }
            });

            clipboard.on('success', function(e) {
                var copymsg = jQuery(e.trigger).attr('copy-msg') || '复制成功';
                Levme.showNotices(copymsg +'<br>'+ e.text);
            });

            clipboard.on('error', function(e) {
                levtoast('复制失败');
            });
        }

    },
    locationHref:function () {
        var href = window.location.href.split('#!/').pop();
        return href.indexOf('http') !== 0 ? homeUrl + href : href;
    },
    swiper:function (inputname) {
        var swiper = new Swiper('.slides-input-'+inputname+' .swiper-container', {
            speed: 800,
            spaceBetween: 10,
            //preloadImages: true,
            lazyLoading: true,
            lazyLoadingClass: 'lazy',
            pagination: '.slides-input-'+inputname+' .swiper-pagination',
            paginationClickable: true,
            autoplay: 5000,
            //slidesPerView: 2,
            //onSlideChangeEnd: function () {},
            // onSlideChangeStart: function () {
            //     window.setTimeout(function () { myApp.initImagesLazyLoad('.pages'); }, 201);
            // }
        });
    },
    setTimeout(func, seconds) {
        var timeoutv = window.setTimeout(function () {
            func();
            window.clearTimeout(timeoutv);
        }, seconds);
    },
    timer: {
        myTimers:{},
        isTimeout:{},
        timeOut (timerId) {
            Levme.timer.myTimers[timerId]  = undefined;
            Levme.timer.isTimeout[timerId] = 1;
        },
        start (timerId, timestamp, timerOutFunc, timerFunc) { //2小时倒计时：timestamp = new Date().getTime() + 2 * 3600 * 1000;
            var timerObj = Levme.timer;
            if (timerObj.myTimers[timerId] === undefined) {
                timerObj.myTimers[timerId] = timestamp;
            } else if (timestamp) {
                timerObj.myTimers[timerId] = timestamp;
                console.log('计时时间重置'+ timerId);
                return false;
            }
            timerObj.isTimeout[timerId] = 0;
            var myTimestamp = timerObj.myTimers[timerId];
            var obj = jQuery(timerId);
            var nowTime = new Date().getTime();
            var seconds_ = Math.round((myTimestamp - nowTime)/1000);
            var htmstr = '';
            if (seconds_ > 0) {
                var dd = parseInt(seconds_ / 60 / 60 / 24, 10);//计算剩余的天数
                var hh = parseInt(seconds_ / 60 / 60 % 24, 10) + dd * 24;//计算剩余的小时数
                var mm = parseInt(seconds_ / 60 % 60, 10);//计算剩余的分钟数
                var ss = parseInt(seconds_ % 60, 10);//计算剩余的秒数
                hh = hh ? '<h>'+ (hh < 10 ? "0" + hh : hh) + '</h><mh>:</mh>' : '';
                mm = mm < 10 ? "0" + mm : mm;
                ss = ss < 10 ? "0" + ss : ss;
                htmstr = '<tmr>'+ hh +'<m>' + mm + "</m><mh>:</mh><s>" + ss +'</s></tmr>';
                obj.html(htmstr);
                Levme.setTimeout(function () {
                    timerObj.start(timerId, false, timerOutFunc, timerFunc);
                }, 1000);
                typeof timerFunc === "function" && timerFunc(seconds_, timerId, timestamp);
            } else {
                timerObj.timeOut(timerId);
                typeof timerOutFunc === "function" && timerOutFunc(timerId, timestamp);
            }
        },
    },
    setTransformAuto (boxCls, reset) {
        var outerBox = jQuery(boxCls).parent();
        if (reset) {
            jQuery(outerBox).removeClass('maxW100');
            Levme.setTransform(1, outerBox, boxCls, 0.2, 0.2);
        }else {
            jQuery(outerBox).addClass('maxW100');
            Levme.setTransform(undefined, outerBox, boxCls, 0.2, 0.2);
        }
    },
    setTransform(scale, winCls, boxCls, min1, min2) {
        winCls = winCls || '.zoushi_container';//需要缩放的外部容器 用于固定高度
        boxCls = boxCls || '.transformbox';//需要缩放的容器
        min1 = parseFloat(min1) || 0.65;
        min2 = parseFloat(min2) || 0.72;

        var winObj    = jQuery(winCls);
        var boxObj    = jQuery(boxCls);
        if (scale === undefined) {
            var screen_wd = window.screen.availWidth;//屏幕可用工作区宽度window.screen.width;//手机获取可视宽度
            var real_wd = boxObj.outerWidth(true);
            var win_wd = winObj.width();//PC获取容器宽度
            win_wd = win_wd < screen_wd ? win_wd : screen_wd;
            var sf_wd = win_wd / real_wd;
            var minScale = win_wd > 660 ? min1 : min2;
            sf_wd = sf_wd < minScale ? minScale : sf_wd;
            if (sf_wd < Levme.getScale(boxCls)) {
                scale = sf_wd;
            }
        }
        if (scale !== undefined) {
            boxObj.css({transform: 'scale('+ scale +')'});
            var height = boxObj.outerHeight(true) * scale;
            winObj.css('height', height + 'px');
            if (scale !== 1) {
                winObj.addClass('isScaled');
            }else {
                winObj.removeClass('isScaled');
            }
        }
    },
    getScale(cls) {
        var scale = 1;
        var trans = jQuery(cls).css('transform');
        if (trans && trans.indexOf(',') >0) {
            var arr = trans.split(',');
            scale = parseFloat(arr[3]) || scale;
        }
        return scale;
    },
    strToIcons (txt) {
        txt = Levme.strToIcon(txt, '#fa-');
        txt = Levme.strToIcon(txt, '#icon-');
        return txt;
    },
    strToIcon(txt, iconPre) {
        if (txt) {
            iconPre = iconPre || '#fa-';
            var arr = txt.split(iconPre);
            for (var k in arr) {
                if (arr[k] && arr[k].indexOf('</code>') <0) {
                    var icons = arr[k].split('#');
                    var icon = iconPre + icons[0];
                    txt = txt.replace(icon + '#', '<svg class="icon"><use xlink:href="'+icon+'"></use></svg>');
                }
            }
        }
        return txt;
    },
    MarkedParse (txt, markedHtmBox) {
        if (typeof marked === "undefined") {
            Levme.script.markeDownJs(markedParse);
        }else {
            return markedParse();
        }
        function markedParse() {
            txt = marked.parse(txt);
            if (txt) {//过滤掉 script 标签
                txt = txt.replace(/<script/gi, '<pre><code');
                txt = txt.replace(/<\/script/gi, '</code></pre');
            }
            txt = Levme.strToIcons(txt);
            markedHtmBox && jQuery(markedHtmBox).html(txt);
            return txt;
        }
    },
    focusAddData (inputCls, Value, focus){ // 光标处输入

        var focus = focus === undefined ? true : focus;
        var inputObj = jQuery(inputCls)[0]; // 插入值所在的input框

        if (document.selection) {

            inputObj.focus();

            sel = document.selection.createRange();

            sel.text = Value;

            inputObj.focus();

        } else if (inputObj.selectionStart || inputObj.selectionStart == '0') {

            var startPos = inputObj.selectionStart;

            var endPos = inputObj.selectionEnd;

            var scrollTop = inputObj.scrollTop;

            inputObj.value = inputObj.value.substring(0, startPos) + Value + inputObj.value.substring(endPos, inputObj

                .value.length);

            focus && inputObj.focus();

            inputObj.selectionStart = startPos + Value.length;

            inputObj.selectionEnd = startPos + Value.length;

            inputObj.scrollTop = scrollTop;

        } else {

            this.value += Value;

            focus && inputObj.focus();

        }

    },
    EditerMarkedInit(id) {
        var textareaObj = 'textarea.'+ id;
        var markedContentObj = '.markedContent.'+ id;
        var showEditMark = '.showEditMarkBtn.'+ id;

        jQuery(document).on('focusout', textareaObj, function () {
            var txt = jQuery(textareaObj).val();
            if (txt) {
                jQuery(textareaObj).text(txt);
                txt = Levme.MarkedParse(txt);
                jQuery(markedContentObj).html(txt).removeClass('hiddenx');
                jQuery(textareaObj).addClass('hiddenx');
            }
        });

        Levme.onClick(showEditMark, function () {
            showMarkedTextarea(markedContentObj);
        });
        Levme.onDbClick(markedContentObj, function (thisObj) {
            showMarkedTextarea(thisObj);
        });

        function showMarkedTextarea(boxObj) {
            var ht = jQuery(boxObj).height();
            jQuery(boxObj).addClass('hiddenx').html('');
            jQuery(textareaObj).removeClass('hiddenx').height(ht);
            jQuery(textareaObj).focus();
        }
    },
    actPayLocalStorage(url, tradeno, pre) {
        var key = 'checkTradeNoIng';
        if (url === undefined) {//消除
            actionLocalStorage(key, 0, 1);
        }else if (url) {//存储
            actionLocalStorage(key, JSON.stringify([new Date().getTime(), url, tradeno, pre]));
        }else {//复核查
            var keystring = actionLocalStorage(key);
            if (keystring) {
                var keyInfo = JSON.parse(keystring);
                if (keyInfo && keyInfo[1]) {
                    Levme.ajaxv.getv(keyInfo[1], function (data, status) {
                        Levme.actPayLocalStorage();//清除
                    }, {storagev: 1});
                    Levme.ajaxv.isGetv = 0;
                }
            }
        }
    },
    setIpLogin (pwd) {
        var key = 'ipLoginInfo';
        var inAndroid = Levme.APP.ckAPP();
        var inAndroidFunc = inAndroid && typeof inAndroid.getSPData === "function";
        if (pwd === undefined) {
            var myPwd = inAndroidFunc ? inAndroid.getSPData(key) : '';
            return myPwd || actionLocalStorage(key);
        }
        actionLocalStorage(key, pwd);
        inAndroidFunc && inAndroid.saveData(key, pwd);
    },
    swipePageTemp:{
        datas:{},
        slides:{},
        infinites: {},
        attachInfiniteScrolls: {},
    },
    swipePage (_pageName, subnavs, deTabId, forceRefresh, initInfinite) {
        subnavs = subnavs || {};

        var pageName = _pageName;

        var deSlideid,loadDataShowBoxCls,slideBtnBoxCls,slideBtnPre,slideBtnCls,deSlideidCls;

        forceRefresh = forceRefresh || jQuery('.'+ pageName).parents('.popup.LevPopupMain').attr('force-refresh-swipe-page');
        if (Levme.swipePageTemp.datas[pageName] && !forceRefresh) {
            sinit();
            return;
        }else {
            showIconLoader(true);
            jQuery(function () {
                Levme.swipePageTemp.slides[pageName]['initInfinite'] = initInfinite;
                sinit();
            });
        }

        function sinit() {

            Levme.swipePageTemp.slides[pageName].initSlide();

            deSlideid = Levme.swipePageTemp.slides[pageName].getDeTabId();

            loadDataShowBoxCls  = Levme.swipePageTemp.slides[pageName].getLoadDataShowBoxCls();
            slideBtnBoxCls      = Levme.swipePageTemp.slides[pageName].getSlideBtnBoxCls();
            slideBtnPre         = Levme.swipePageTemp.slides[pageName].slideBtnPre;
            slideBtnCls         = Levme.swipePageTemp.slides[pageName].getCommonSlideBtn();
            deSlideidCls        = Levme.swipePageTemp.slides[pageName].getSlideBtn(deSlideid);

            Levme.setTimeout(function () {
                levtoMaoCenter(deSlideidCls, slideBtnBoxCls, 300);
                changeTab(deSlideid);
                Levme.swipePageTemp.infinites[pageName].initInfinite(Levme.swipePageTemp.slides[pageName].getActiveLoadDataShowBoxCls(), Levme.swipePageTemp.slides[pageName].getPageClass());
            }, 200);
        }

        Levme.swipePageTemp.datas[pageName] = {
            dataLoading: 0,
            loadData: {},
            loaded: {},
            loadedNavSlideid: {},
            timeoutShowData: {},
        };

        Levme.swipePageTemp.slides[pageName] = {
            pageName: pageName,
            deTabId: deTabId,
            subnavs: subnavs,
            loadDataShowBoxCls: '.loadDataShowBox',
            slideBtnBoxCls: '.slideBtnBox',
            slideBtnPre: '.slideBtn_',
            Swipev:null,
            initSlide() {
                //Levme.swipePageTemp.slides[pageName].Swipev !== null && Levme.swipePageTemp.slides[pageName].Swipev.destroy(true, true);
                var SwiperContainer = Levme.swipePageTemp.slides[pageName].getPageClass() + ' .swipeMainBox.swiper-container';
                Levme.swipePageTemp.slides[pageName].Swipev = new Swiper(SwiperContainer, {
                    initialSlide: Levme.swipePageTemp.slides[pageName].getInitialSlide(),
                    onInit: function(swiper){//Swiper初始化了

                    },
                    onSlideChangeStart: function (swiper) {
                        var index = swiper.activeIndex;
                        var slideid = jQuery(Levme.swipePageTemp.slides[pageName].getCommonSlideBtn()).eq(index).data('slideid');
                        var loadRoute = jQuery(Levme.swipePageTemp.slides[pageName].getSlideBtn(slideid)).attr('href');
                        Levme.swipePageTemp.slides[pageName].setEmptyLoadDataShowBox();
                        changeTab(slideid, loadRoute);
                    },
                    onSlideChangeEnd: function (swiper) {
                    }
                });

                Levme.onClick(Levme.swipePageTemp.slides[pageName].getCommonSlideBtn(), function () {
                    var index = jQuery(this).index();
                    Levme.swipePageTemp.slides[pageName].Swipev.slideTo(index, undefined, true);
                    return false;
                });
            },
            getPageClass() {
                return '.'+ Levme.swipePageTemp.slides[pageName].pageName
            },
            setEmptyLoadDataShowBox (slideid) {
                if (slideid === undefined) {
                    jQuery(Levme.swipePageTemp.slides[pageName].getLoadDataShowBoxCls(slideid)).each(function () {
                        var thisHtms = jQuery(this).html();
                        var _slideid = jQuery(this).attr('data-boxid');
                        thisHtms && saveTempDataHtms(_slideid, thisHtms, pageName);
                    })
                }else {
                    saveTempDataHtms(slideid, jQuery(Levme.swipePageTemp.slides[pageName].getLoadDataShowBoxCls(slideid)).html(), pageName);
                }
                jQuery(Levme.swipePageTemp.slides[pageName].getLoadDataShowBoxCls(slideid)).html('');
            },
            getLoadDataShowBoxCls (slideid) {
                var swipeSlideBox = slideid === undefined ? '' : ' .swipeSlideBox_'+ slideid;
                return Levme.swipePageTemp.slides[pageName].getPageClass() + swipeSlideBox +' '+ Levme.swipePageTemp.slides[pageName].loadDataShowBoxCls;
            },
            getActiveSlide() {
                var slideid = jQuery(Levme.swipePageTemp.slides[pageName].getCommonSlideBtn() +'.active').attr('data-slideid');
                var swipeSlideBox = ' .swipeSlideBox_'+ slideid;
                return Levme.swipePageTemp.slides[pageName].getPageClass() + swipeSlideBox;
            },
            getActiveLoadDataShowBoxCls () {
                return Levme.swipePageTemp.slides[pageName].getActiveSlide() +' '+ Levme.swipePageTemp.slides[pageName].loadDataShowBoxCls;
            },
            getSlideBtn(slideid) {
                return Levme.swipePageTemp.slides[pageName].getPageClass() +' '+ Levme.swipePageTemp.slides[pageName].slideBtnBoxCls +' '+ Levme.swipePageTemp.slides[pageName].slideBtnPre + slideid;
            },
            getCommonSlideBtn() {
                return Levme.swipePageTemp.slides[pageName].getPageClass() +' '+ Levme.swipePageTemp.slides[pageName].slideBtnBoxCls +' '+ Levme.swipePageTemp.slides[pageName].slideBtnPre;
            },
            getSlideBtnBoxCls () {
                return Levme.swipePageTemp.slides[pageName].getPageClass() +' '+ Levme.swipePageTemp.slides[pageName].slideBtnBoxCls;
            },
            getInitialSlide() {
                var _deTabId = Levme.swipePageTemp.slides[pageName].getDeTabId();
                return jQuery('.'+pageName + ' ' +Levme.swipePageTemp.slides[pageName].slideBtnPre + _deTabId).index(Levme.swipePageTemp.slides[pageName].getCommonSlideBtn());
            },
            getDeTabId() {
                return jQuery(Levme.swipePageTemp.slides[pageName].getCommonSlideBtn() + '.active').attr('data-slideid') || Levme.swipePageTemp.slides[pageName].deTabId;
            },
        };

        function infiniteScrollv(btnObj) {
            Levme.loadDataToSlideFuncs.infiniteScrollv(btnObj, pageName);
        }

        Levme.swipePageTemp.infinites[pageName] = {
            tempData: {
                loading:{},
                btnTips:{},
            },
            tempPageDatas:{},
            initInfinite:function(dataInfiniteBox, pageClass) {
                Levme.swipePageTemp.infinites[pageName].isInfiniteFinish(dataInfiniteBox) &&
                Levme.swipePageTemp.infinites[pageName].setInfiniteFinish(dataInfiniteBox);

                Levme.onClick('.infinite-scroll .startInfiniteScrollBtn', function () {
                    infiniteScrollv(this);
                    return false;
                });
            },
            getInfiniteBoxCls(boxid) {
                return '.infiniteBox_'+ boxid;
            },
            getStartInfiniteScrollBtn(dataInfiniteBox) {
                return jQuery(dataInfiniteBox).parents('.infinite-scroll').find('.startInfiniteScrollBtn');
            },
            setDefault (dataInfiniteBox, message) {
                Levme.swipePageTemp.infinites[pageName].setInfiniteFinish(dataInfiniteBox, message, true);
            },
            setInfiniteFinish (dataInfiniteBox, message, reset) {
                var startInfiniteScrollBtnObj = Levme.swipePageTemp.infinites[pageName].getStartInfiniteScrollBtn(dataInfiniteBox);
                if (reset) {
                    jQuery(dataInfiniteBox).attr('page', "2").attr('not', "0");
                    //jQuery(dataInfiniteBox).find('.dataInfiniteBox').attr('page', "2").attr('not', "0");
                    startInfiniteScrollBtnObj.removeClass('InfiniteFinished disabled');
                    startInfiniteScrollBtnObj.html(message ? message : '点击加载更多').removeClass('hiddenx').attr('disabled', false);
                }else {
                    jQuery(dataInfiniteBox).attr('page', "-1").attr('not', "1");
                    //jQuery(dataInfiniteBox).find('.dataInfiniteBox').attr('page', "-1").attr('not', "1");
                    startInfiniteScrollBtnObj.addClass('InfiniteFinished disabled');
                    startInfiniteScrollBtnObj.html(message !== undefined ? message : '没有了').removeClass('hiddenx').attr('disabled', true);
                }
            },
            isInfiniteFinish (dataInfiniteBox) {
                return parseInt(jQuery(dataInfiniteBox).attr('not')) === 1;
            },
            ajaxLoadData (InfiniteBox, force, infiniteInit) {//list-block virtual-list
                var dataInfiniteBox = InfiniteBox + ' .dataInfiniteBox';

                var page = parseInt(jQuery(dataInfiniteBox).attr('page'));
                !infiniteInit &&
                page === -2 && (page = 2) && jQuery(dataInfiniteBox).attr('page', page);
                if (page === 2 || infiniteInit || !Levme.swipePageTemp['attachInfiniteScrolls'][InfiniteBox]) {
                    console.log(InfiniteBox, 'Infi');
                    Levme.swipePageTemp['attachInfiniteScrolls'][InfiniteBox] = InfiniteBox;
                    myApp.attachInfiniteScroll(jQuery(InfiniteBox));//给指定的 HTML 容器添加无限滚动事件监听器
                    jQuery(document).off('infinite', InfiniteBox).on('infinite', InfiniteBox, function (e) {
                        if (infiniteInit || Levme.swipePageTemp.slides[pageName].Swipev.animating) {
                            infiniteInit = 0;
                            //levtoast('切换期间，禁止自动加载数据');
                            return;
                        }
                        page >= 2 && doInfiniteData();
                    });
                }
                if (force) {
                    doInfiniteData();
                    return false;
                }
                function doInfiniteData() {
                    if (Levme.swipePageTemp.infinites[pageName].getLoadingStatus(dataInfiniteBox)) {
                        showIconLoader(true);
                        return false;
                    }

                    var jsonp = jQuery(dataInfiniteBox).attr('jsonp') ? 'jsonp' : 'json';
                    var url = jQuery(dataInfiniteBox).attr('url');
                    page = parseInt(jQuery(dataInfiniteBox).attr('page'));
                    page = isNaN(page) ? 0 : page;
                    if (page < 2) return true;
                    if (!url) {
                        return true;
                    }
                    Levme.swipePageTemp.infinites[pageName].setLoadingStatus(dataInfiniteBox, 1);
                    jQuery.ajax({
                        url: url,
                        data:{page:page, infinite:1, _csrf:_csrf, _:Math.random()},
                        dataType:jsonp,
                        type:'get',
                        success:function(data){
                            Levme.swipePageTemp.infinites[pageName].setLoadingStatus(dataInfiniteBox, 0);
                            var trimHtms = jQuery.trim(data.htms);
                            if (trimHtms) {
                                var status = parseInt(data.status);
                                //data.message && levtoast(data.message, 5000);
                                Levme.viptoast(data, 5000);

                                if (status === -5) {
                                    openLoginScreen();
                                } else if (status > 0) {
                                    var nextPage = data.page ? data.page : page + 1;
                                    var dataHtms = Levme.swipePageTemp.infinites[pageName].formatDataHtms(page, data.htms);
                                    Levme.swipePageTemp.infinites[pageName].setTempPageDatas(url, page, dataHtms);
                                    jQuery(dataInfiniteBox).attr('page', nextPage);
                                    if (jQuery(InfiniteBox).hasClass('infinite-scroll-top')) {
                                        if (dataHtms.indexOf('<lev-page>') >= 0) {
                                            jQuery(dataInfiniteBox + ' lev-page').prepend(jQuery(dataHtms).find('lev-page').html());
                                        } else {
                                            jQuery(dataInfiniteBox).prepend(dataHtms);
                                        }
                                    }else {
                                        if (dataHtms.indexOf('<lev-page>') >= 0) {
                                            jQuery(dataInfiniteBox + ' lev-page').append(jQuery(dataHtms).find('lev-page').html());
                                        } else {
                                            jQuery(dataInfiniteBox).append(dataHtms);
                                        }
                                    }
                                    Levme.swipePageTemp.infinites[pageName].cutMaxDataHtms(dataInfiniteBox, data.maxPage);
                                    Levme.setTimeout(function () {
                                        myApp.initImagesLazyLoad(dataInfiniteBox);
                                        myApp.initImagesLazyLoad('.page-content');
                                        saveTempDataHtms(jQuery(dataInfiniteBox).attr('data-boxid'), jQuery(dataInfiniteBox).html(), pageName);
                                    }, 101);
                                }
                            }
                            if (!data || data.status <0 || parseInt(data.not) || !trimHtms) {
                                var message = data && data.message ? data.message : undefined;
                                Levme.swipePageTemp.infinites[pageName].setInfiniteFinish(dataInfiniteBox, message);
                            }
                        },
                        error:function(data){
                            Levme.swipePageTemp.infinites[pageName].setLoadingStatus(dataInfiniteBox, 0);
                            errortips(data);
                        }
                    });
                }
            },
            cutMaxDataHtms (dataInfiniteBox, maxPage) {
                var InfinitePageObj = jQuery(dataInfiniteBox).find('.InfinitePage_');
                if (maxPage === undefined) {
                    maxPage = 100;
                }
                if (InfinitePageObj.length > maxPage) {
                    //console.log(datahtms.length);
                    if (InfinitePageObj.length >1) {
                        InfinitePageObj.eq(0).remove();
                    }
                }
            },
            formatDataHtms (page, htms) {
                return '<div class="InfinitePage_ InfinitePage_'+ page +'" page="'+ page +'">'+ htms +'</div>';
            },
            setTempPageDatas(url, page, dataHtms) {
                url = Levme.encodeUrlChinese(url);
                Levme.swipePageTemp.infinites[pageName].tempPageDatas[base64EncodeUrl(url)+'_'+page] = dataHtms;
            },
            getTempPageDatas(url, page) {
                url = Levme.encodeUrlChinese(url);
                return Levme.swipePageTemp.infinites[pageName].tempPageDatas[base64EncodeUrl(url)+'_'+page];
            },
            setLoadingStatus(dataInfiniteBox, value) {
                Levme.swipePageTemp.infinites[pageName].tempData.loading[dataInfiniteBox] = value;
                var btn = Levme.swipePageTemp.infinites[pageName].getStartInfiniteScrollBtn(dataInfiniteBox);
                if (Levme.swipePageTemp.infinites[pageName].tempData.btnTips[dataInfiniteBox] === undefined) {
                    Levme.swipePageTemp.infinites[pageName].tempData.btnTips[dataInfiniteBox] = btn.html();
                }
                if (value) {
                    btn.html('<div class="preloader preloader-white"></div>');
                }else {
                    hideIconLoader();
                    btn.html(Levme.swipePageTemp.infinites[pageName].tempData.btnTips[dataInfiniteBox] || '点击加载更多');
                }
            },
            getLoadingStatus(dataInfiniteBox) {
                return Levme.swipePageTemp.infinites[pageName].tempData.loading[dataInfiniteBox];
            },
        };

        Levme.onClick('a.loadData,.loadDataToSlide', function () {
            return aclick(this);
        });
        Levme.onClick(loadDataShowBoxCls +' a', function () {
            return aclick(this);
        });

        function aclick(aobj) {
            return Levme.loadDataToSlide(aobj, pageName);
        }

        function changeTab(slideid, loadRoute, __pageName) {
            var myPageName = __pageName || pageName;
            var tabCls = slideBtnPre + slideid;
            var btnObj = jQuery(slideBtnCls + tabCls);

            jQuery(slideBtnCls).removeClass('active');
            btnObj.addClass('active');
            levtoMaoCenter(tabCls, slideBtnBoxCls, 300);

            loadRoute = loadRoute || btnObj.attr('href');
            var showSlideid = Levme.swipePageTemp.datas[myPageName].loadedNavSlideid[slideid] || slideid;
            loadData(showSlideid, loadRoute, undefined, myPageName);
        }

        function loadData(slideid, loadRoute, force, pageName) {
            Levme.loadDataToSlideFuncs.loadDataBy(slideid, loadRoute, force, pageName);
        }


        function saveTempDataHtms(slideid, htms, __pageName) {
            Levme.loadDataToSlideFuncs.saveTempDataHtms(slideid, htms, __pageName || pageName);
        }

    },
    loadDataToSlide (aobj, pageName, _loadRoute) {
        if (jQuery(aobj).hasClass('zstLoadDataBtn') || jQuery(aobj).parents('.apisDataBox').length >0) return;
        pageName = pageName || jQuery('.page .swipesBox').last().parents('.page').attr('data-page');
        var loadRoute = _loadRoute || jQuery(aobj).attr('href');
        if (!loadRoute || loadRoute === '#' || jQuery(aobj).hasClass('except') || jQuery(aobj).attr('_bk')) return;
        if (jQuery(aobj).hasClass('openPP')) return;

        var slideid = jQuery(aobj).data('slideid');
        if (!slideid) {
            var eurl = Levme.encodeUrlChinese(loadRoute);
            slideid = 'slide-x'+base64EncodeUrl(eurl);
        }

        var myPageName = jQuery(aobj).attr('page-name') || jQuery(aobj).parents('.page').attr('data-page') || pageName;
        var mySlideBtnCls = Levme.swipePageTemp.slides[myPageName].getCommonSlideBtn();
        var NavSlideid = jQuery(mySlideBtnCls+'.active').data('slideid');
        Levme.swipePageTemp.datas[myPageName].loadedNavSlideid[NavSlideid] = slideid;

        Levme.loadDataToSlideFuncs.loadDataBy(slideid, loadRoute, undefined, myPageName);
        return false;
    },
    loadDataToSlideFuncs : {
        infiniteScrollv(btnObj, pageName, infiniteInit) {
            if (pageName && !btnObj) {
                btnObj = jQuery(Levme.swipePageTemp.slides[pageName].getActiveSlide()).find('.infinite-scroll .startInfiniteScrollBtn')
            }
            var boxid = jQuery(btnObj).parents('.infinite-scroll').find('.dataInfiniteBox').attr('data-boxid');
            var thisInfiniteBox = '.'+ pageName + ' '+ Levme.swipePageTemp.infinites[pageName].getInfiniteBoxCls(boxid) + '.infinite-scroll';
            Levme.swipePageTemp.infinites[pageName].ajaxLoadData(thisInfiniteBox, !infiniteInit, infiniteInit);
        },
        saveTempDataHtms(slideid, htms, __pageName) {
            var myPageName = __pageName;// || pageName;
            if (!htms) {
                return;
            }
            if (!Levme.swipePageTemp.datas[myPageName].loadData[slideid]) {
                Levme.swipePageTemp.datas[myPageName].loadData[slideid] = {};
            }
            Levme.swipePageTemp.datas[myPageName].loadData[slideid].htms = htms;
        },
        loadDataBy(slideid, loadRoute, force, pageName) {
            //if (Levme.swipePageTemp.datas[pageName].dataLoading) return;
            Levme.swipePageTemp.datas[pageName].dataLoading = 1;

            if (!loadRoute) return;
            if (force === undefined) {
                force = parseInt(jQuery('.'+pageName).find('.slideBtn_.slideBtn_'+ slideid).attr('force'));
            }
            if ( !force && Levme.swipePageTemp.datas[pageName].loadData[slideid] ) {
                showData(slideid, undefined, pageName)
            } else {
                ajaxData();
            }

            function ajaxData() {
                showIconLoader(true);
                Levme.ajaxv.getv(loadRoute, function (data, status) {
                    Levme.swipePageTemp.datas[pageName].dataLoading = 0;
                    Levme.swipePageTemp.datas[pageName].loadData[slideid] = data;
                    if (data.htms) {
                        data.htms = Levme.swipePageTemp.infinites[pageName].formatDataHtms(1, data.htms);
                    }
                    showData(slideid, data, pageName);
                }, {inajax: 1});
            }

            function showData(slideid, data, pageName) {
                hideIconLoader();
                Levme.swipePageTemp.datas[pageName].dataLoading = 0;

                data = data || Levme.swipePageTemp.datas[pageName].loadData[slideid];
                if (data.error) {
                    //levtoast(data.message);
                    Levme.viptoast(data);
                    return;
                }

                var ActiveLoadDataShowBoxCls = Levme.swipePageTemp.slides[pageName].getActiveLoadDataShowBoxCls();

                var htms = jQuery(ActiveLoadDataShowBoxCls).html();
                if (htms) {
                    if (Levme.swipePageTemp.datas[pageName].loaded[slideid]) {
                        var thisSlideid = jQuery(ActiveLoadDataShowBoxCls).attr('temp-slideid');
                        if (thisSlideid === slideid && Levme.swipePageTemp.datas[pageName].timeoutShowData[slideid] > new Date().getTime() - 10 * 1000) {
                            //相同Slideid 10秒后可再次点击 重载数据
                            return;
                        }
                        Levme.loadDataToSlideFuncs.saveTempDataHtms(thisSlideid, htms, pageName);
                    }
                    Levme.swipePageTemp.datas[pageName].timeoutShowData[slideid] = new Date().getTime();
                }

                jQuery(ActiveLoadDataShowBoxCls).attr('temp-slideid', slideid);
                jQuery(ActiveLoadDataShowBoxCls).html(data.htms);
                Levme.setTimeout(function () {
                    myApp.initImagesLazyLoad('.page-content');
                    if (Levme.swipePageTemp.slides[pageName]['initInfinite']) {
                        Levme.loadDataToSlideFuncs.infiniteScrollv(0, pageName, 1);
                        //Levme.swipePageTemp.slides[pageName]['initInfinite'] = 0;
                    }
                }, 200);
                parseInt(data.not) === 1 && Levme.swipePageTemp.infinites[pageName].setInfiniteFinish(ActiveLoadDataShowBoxCls, data.message);

                if (!Levme.swipePageTemp.datas[pageName].loaded[slideid]) {
                    Levme.setTimeout(function () {
                        if (jQuery(ActiveLoadDataShowBoxCls).attr('temp-slideid') === slideid) {
                            Levme.swipePageTemp.datas[pageName].loaded[slideid] = slideid;
                            //Levme.swipePageTemp.datas[pageName].loadData[slideid].htms = jQuery(ActiveLoadDataShowBoxCls).html();//初始化
                            Levme.loadDataToSlideFuncs.saveTempDataHtms(slideid, jQuery(ActiveLoadDataShowBoxCls).html(), pageName);
                        }
                    }, 408);
                }
            }
        },
    },
    encodeUrlChinese (url) {
        return encodeURIComponent(url).replace(/%/g, '');
    },
    emailIden:{
        tempDatas: {
            emailids:{},
            tmps:{},
        },
        attrBtnPre (emailid) {
            return 'showEmailid_'+ emailid;
        },
        clearsAll() {
            Levme.emailIden.clearMyEmailUID();
            Levme.emailIden.clearLocalStorageNewEmailCount();
            Levme.emailIden.clearMyReadedEmailIds();
            Levme.emailIden.clearMyReadedMaxEmailId();

            Levme.emailIden.clearStorageShows();
        },
        updateMyLocalStorages(data) {
            window.UID = data.uid;
            Levme.emailIden.setMyEmailUID(data.uid);
            Levme.emailIden.setLocalStorageNewEmailCount(data.NewEmailCount);
            Levme.emailIden.setMyReadedEmailIds(data.myReadedEmailIds);
            Levme.emailIden.setMyReadedMaxEmailId(data.myReadedMaxEmailId);
        },
        checkMyEmailUID () {
            var euid = parseInt(Levme.emailIden.opMyEmailUID());
            var UID = parseInt(window.UID);
            return UID <1 || euid === UID;
        },
        getMyEmailUID () {
            return Levme.emailIden.opMyEmailUID();
        },
        setMyEmailUID (uid) {
            return Levme.emailIden.opMyEmailUID(uid);
        },
        clearMyEmailUID () {
            return Levme.emailIden.opMyEmailUID(0, 1);
        },
        opMyEmailUID (uid, clear) {
            var Key = 'myEmailUID';
            if (clear) {
                return actionLocalStorage(Key, 0, 1);
            }
            if (uid === undefined) {
                return actionLocalStorage(Key);
            }else {
                actionLocalStorage(Key, uid);
            }
        },
        clearMyReadedMaxEmailId () {
            var Key = 'myReadedMaxEmailId';
            actionLocalStorage(Key, 0, 1);
        },
        setMyReadedMaxEmailId (emailid) {
            return Levme.emailIden.myReadedMaxEmailId(emailid);
        },
        myReadedMaxEmailId (emailid) {//通知到的最大邮件ID
            var Key = 'myReadedMaxEmailId';
            var maxid = actionLocalStorage(Key);
            if (emailid === undefined) {
                return maxid;
            }
            emailid = parseInt(emailid);
            if (emailid) {
                if (!maxid || emailid > maxid) {
                    actionLocalStorage(Key, emailid);
                }
            }
        },
        setMyReadedEmailIds (emailidsArr) {
            Levme.emailIden.opMyReadedEmailIds(emailidsArr);
        },
        myReadedEmailIds (emailid, remove) {//全部已读ID
            var eids = Levme.emailIden.opMyReadedEmailIds();
            if (remove) {
                removeEmailid();
            }else {
                if (emailid === undefined) {
                    return eids;
                }
                emailid = parseInt(emailid);
                if (!Levme.emailIden.checkReaded(emailid)) {
                    eids.push(emailid);
                    Levme.emailIden.opMyReadedEmailIds(eids);
                }
            }

            function removeEmailid() {
                eids = arr_remove(emailid, eids);
                Levme.emailIden.opMyReadedEmailIds(eids);
            }
        },
        clearMyReadedEmailIds () {
            Levme.emailIden.opMyReadedEmailIds(0, 1);
        },
        opMyReadedEmailIds (data, clear) {
            var Key = 'myReadedEmailIds';
            if (clear) {
                return actionLocalStorage(Key, 0, 1);
            }
            var eids = actionLocalStorage(Key);
            eids = eids ? eids.split(',') : [];
            if (data === undefined) {
                return eids;
            } else {
                actionLocalStorage(Key, data.join(','));
            }
        },
        clearStorageShows () {
            Levme.emailIden.opStorageShows(0, 1);
        },
        opStorageShows(data, clear) {
            var key = 'emailNoticeLiveJs_showTmp';
            if (clear) {
                return actionLocalStorage(key, 0, 1);
            }
            var str = actionLocalStorage(key);
            var myArrs = str && str.indexOf('}') >0 ? JSON.parse(str) : {};
            var myArr = myArrs[window.UID] || {};
            if (!data) {
                return myArr;
            }else if (typeof data === "object") {
                myArrs[window.UID] = data;
                actionLocalStorage(key, JSON.stringify(myArrs));
            }
        },
        checkReaded (emailid) {
            var ids = Levme.emailIden.myReadedEmailIds();
            return !!(ids && ids.indexOf(emailid) >= 0);

        },
        clearLocalStorageNewEmailCount () {
            var Key = 'NewEmailCount';
            return actionLocalStorage(Key, 0, 1);
        },
        setLocalStorageNewEmailCount (count, add) {
            var Key = 'NewEmailCount';
            count = parseInt(count) || 0;
            if (count > 0) {
                if (add) {
                    count += parseInt(actionLocalStorage(Key) || 0);
                }
                actionLocalStorage(Key, count);
            }else {
                actionLocalStorage(Key, 0, 1);
            }
            return count;
        },
        getLocalStorageNewEmailCount () {
            var Key = 'NewEmailCount';
            return actionLocalStorage(Key);
        },
        setNewCountNeml(count, box) {
            if (count === undefined) count = Levme.emailIden.getLocalStorageNewEmailCount();
            count = parseInt(count) || 0;
            if (count < 1) {
                jQuery('n-eml').addClass('hiddenx');
                return;
            }else {
                count = count >999 ? '999+' : count;

                jQuery('n-eml').removeClass('hiddenx').removeClass('v_dofade');
                jQuery('n-eml').html(count);
            }
            if (box) {
                if (jQuery(box).find('n-eml').length > 0) {
                    jQuery(box).find('n-eml').html(count);
                } else {
                    jQuery(box).append('<n-eml>' + count + '</n-eml>');
                }
            }
        },
        setNewCountNemlNewid (newid) {
            if (jQuery('n-eml').length >0) {
                newid = parseInt(newid) || 0;
                var reid = parseInt(Levme.emailIden.myReadedMaxEmailId()) || 0;
                if (reid < newid) {
                    var total = Levme.emailIden.setLocalStorageNewEmailCount(newid - reid, true);
                    Levme.emailIden.myReadedMaxEmailId(newid);
                    Levme.emailIden.setNewCountNeml(total);
                    jQuery('n-eml').addClass('v_dofade');
                    Levme.popupAjax.htms['MyEmail'] = undefined;
                }
            }
        },
    },
    actionTimeout(key, sec, func) {
        key += '___actionTimeout';
        if (typeof func !== "function") {//删除本地缓存
            actionLocalStorage(key, 0, 1);
            return true;
        }
        var nowTime = new Date().getTime();
        var time = parseInt(actionLocalStorage(key));
        if (!time || time < nowTime) {
            actionLocalStorage(key, nowTime + sec*1000);
            func();
            return false;
        }
        return true;
    },
    actionOnce(key, del) {
        key += '___';
        if (del) {
            Levme.tempDatas[key] = 0;
            return true;
        }
        if (Levme.tempDatas[key]) {
            console.log('仅调用一次，禁止重复调用'+ key);
            return true;
        }
        Levme.tempDatas[key] = 1;
        return false;
    },
    alertMini(text, title, func, cancelFunc, canText, cancelText) {
        Levme.alert(text, title, func, cancelFunc, canText, cancelText, false);
    },
    alert (text, title, func, cancelFunc, canText, cancelText, autoWidth) {
        myApp.closeModal('.modal.modal-in');
        var btns = [];
        if (cancelFunc) {
            btns.push({text: cancelText || '取消', onClick: cancelFunc});
        }
        btns.push({text: canText || '确定', bold: true, onClick: func});
        myApp.modal({
            text: text || '',
            title: title || '',
            buttons: btns,
        });
        (autoWidth === undefined || autoWidth) && jQuery('.modal.modal-in').addClass('auto-width');
        Levme.onOnce('close', '.modal.modal-in', function () {
            if (jQuery('.actions-modal.modal-in').length >0 && jQuery('.modal-overlay.modal-overlay-visible').length <1) {
                jQuery('body').append('<div class="modal-overlay modal-overlay-visible"></div>');

            }
            if (jQuery('.popup.modal-in').length >0 && jQuery('.popup-overlay.modal-overlay-visible').length <1) {
                jQuery('body').append('<div class="popup-overlay modal-overlay-visible"></div>');
            }
        });
    },
    copyright:{
        do:10,
        act:function () {
            if (!Levme.copyright.do) {
                Levme.copyright.do = 1;
            }
        }
    },
    JSONParse (value, defaultValue, returnObject) {
        if (!value || typeof value !== 'string') {
            return defaultValue;
        }
        try {
            if (value.length >1024*1024) {//超出1M清空
                json = {}
            } else {
                var sstr = value.slice(0, 1);
                var estr = value.slice(-1);
                var json = (sstr === '[' && estr === ']') || (sstr === '{' && estr === '}') ? JSON.parse(value) : {};
                if (returnObject && typeof json !== "object") json = {};
            }
            return json;
        } catch (e) {
            return defaultValue;
        }
    },
    languageid (langid) {
        var key = 'myLanguageid';
        if (langid === undefined) {
            return actionLocalStorage(key);
        }
        return actionLocalStorage(key, langid);
    },
    setLanguage (btnObj) {
        var langid = jQuery(btnObj).attr('langid');
        Levme.ajaxv.getv(levToRoute('language/set-lang', {id:'languages',langid:langid}), function (data, status) {
            if (status > 0) {
                Levme.languageid(data['langid']);
                Levme.setTimeout(function () {
                    window.location = Levme.locationHref();
                }, 1000);
            }
        });
    },
    console: {
        close () {
            console.log = function () {};
        },
        log(key) {
            if (key !== undefined) {
                var k = 0;
                for (let arg of arguments) {
                    console.log(k++, arg);
                }
            }else {
                console.log(key);
            }
        },
    }
};

Levme.APP.init();
Levme.popupIframe.init();
Levme.tablesnav.init();
Levme.dosaveForm.init();
Levme.conf.init();
Levme.loginv.btns();
Levme.setups.btn();
Levme.sign.init();
Levme.clipboardv();
Levme.iconSelectWinBtnReg();
Levme.popupAjax.clicks();
Levme.onClick('.photoShowsBox img', function () {
    Levme.photoShows(this, '.photoShowsBox img');
});
Levme.onClick('.clearCacheBtn', function () {
    Levme.APP.clearCache();
});
Levme.onClick('.form-group img, .photoShowBtn', function () {
    Levme.photoShow(jQuery(this).attr('src'));
});
Levme.onClick('.wxLoginBtn', function () {
    Levme.APP.wxLogin();
});
Levme.onClick('.setLanguageBtn', function () {
    Levme.setLanguage(this);
});
Levme.onClick('.sendEmailBugBtn', function () {
    Levme.sendEmailBug(this);
});
Levme.onClick('.sendEmailBtn', function () {
    Levme.sendEmailBtn(this);
});
Levme.onClick('.playVideoBtn', function () {
    Levme.playVideoBtnFunc(this);
});
Levme.onDbClick('.formFieldBox .hint-block', function (thisObj) {
    var htm = jQuery(thisObj).html();
    htm && Levme.alert('<div class="show-content-detail">'+ htm +'</div>');
    //Levme.showNotices(htm, 'info', 3600000, 0, false);
});
Levme.onClick('.refreshPageBtn', function () {
    if (jQuery(this).parents('.view').find('.refreshPP').length > 0) {
        jQuery(this).parents('.view').find('.refreshPP').click();
    } else {
        window.location = Levme.locationHref();
    }
});


function APPmyPhoneLogin(phone) { Levme.APP.myPhoneLogin(phone); }

//【注意】不能encode中文
function base64EncodeUrl(str) { return window.btoa((str)).replace(/=/g, '').replace(/\//g, '_').replace(/\+/g, '-');}
function base64DecodeUrl(str) { return (window.atob(str.replace(/_/g, '/').replace(/-/g, '+'))); }

function loadInfiniteAjax(infBox, force) { return Levme.loadInfiniteAjax(infBox, force); }

function appNoticeLive(uid) { Levme.APP.appNoticeLive(uid); }

function ckAPP() { return Levme.APP.ckAPP(); }

function openHtml2imageWin(obj, code, imgBox) { return Levme.lotteryChartImg.chartIframe(obj, code, imgBox) }
function getHtml2image(src, data) { return Levme.lotteryChartImg.setChartImg(src, data) }

jQuery(function(){
    if (typeof(__mobile__moveFlag__) !='undefined') {
        jQuery(".draggable_btn").draggable({ cursor: "move",
            stop: function() {
                jQuery(this).css({'right':'unset', 'bottom':'unset'});
            }
        });//拖动
    }

    Levme.onClick('.IpLoginBtn', function () {
        var url = jQuery(this).attr('href') || jQuery(this).attr('src');
        Levme.ajaxv.getv(url, function (data, status) {
            data.ippwd && Levme.setIpLogin(data.ippwd);

            status >0 && Levme.setTimeout(function () {
                window.location = Levme.locationHref();
            }, 300);
        }, {ipcode:Levme.setIpLogin()});
        return false;
    });

    jQuery(document).on('click', '.swipeback-page-opacity', function(){
        jQuery('.swipeback-page-opacity').remove();
    });

    appNoticeLive(actionLocalStorage('UID'));

    Levme.emailIden.setNewCountNeml();

    window.setTimeout(function () {
        Levme.showNotices();
        Levme.actPayLocalStorage(false);//支付核查
    }, 101);
});

var levsignJs ={};
(function () {
    'use strict';

    jQuery(function () {
        levsignJs.init();
    });

    levsignJs = {
        config:{},
        init:function () {
            var d = new Date();//console.log(d.getDate());
        },
        signBtn:function (typeid) {
            Levme.sign.signBtn(typeid);
        },
    }

})();