<?php !defined('INLEV') && exit('Access Denied LEV');
 return array(
'name'=>'LevApp',
'identifier'=>'levs',
'classdir'=>'0',
'descs'=>'Lev 轻量系统是一款极其精简的模块化系统，除去常用静态文件，大小不足1M。支持php composer 安装',
'copyright'=>'Lev',
'version'=>'20210621.37',
'versiontime'=>'1678434770',
'settings'=>serialize(array (
  '_adminNavs' => 
  array (
    1 => 
    array (
      'id' => 1,
      'order' => '1',
      'name' => '组件管理',
      'target' => '1',
      'status' => '0',
      'tableName' => '0',
      'ctype' => '0',
      'link' => 'superman/modules',
      'forceGen' => '1',
    ),
    2 => 
    array (
      'id' => 2,
      'order' => '2',
      'name' => '模块管理',
      'target' => '1',
      'status' => '',
      'tableName' => '{{%lev_modules}}',
      'ctype' => '0',
      'link' => 'admin-modules',
      'forceGen' => '0',
    ),
    3 => 
    array (
      'id' => 3,
      'order' => '3',
      'name' => '登陆用户',
      'target' => '1',
      'status' => '1',
      'tableName' => '{{%lev_users_login}}',
      'ctype' => '1',
      'link' => 'admin-users',
      'forceGen' => '0',
    ),
    4 => 
    array (
      'id' => 4,
      'order' => '4',
      'name' => '用户资料',
      'target' => '1',
      'status' => '',
      'tableName' => '{{%lev_users}}',
      'ctype' => '1',
      'link' => 'admin-user-detail',
      'forceGen' => '',
    ),
  ),
  '_adminClassify' => 
  array (
    'app' => 
    array (
      'id' => 'app',
      'order' => '0',
      'status' => '0',
      'name' => '网站导航',
      'descs' => '',
    ),
    'welcome' => 
    array (
      'id' => 'welcome',
      'order' => '3',
      'status' => '1',
      'name' => '欢迎界面',
      'descs' => '',
    ),
    'webpg' => 
    array (
      'id' => 'webpg',
      'order' => '5',
      'status' => '0',
      'name' => '全局导航',
      'descs' => '',
    ),
    'login' => 
    array (
      'id' => 'login',
      'order' => '7',
      'status' => '0',
      'name' => '登陆设置',
      'descs' => '',
    ),
    'global' => 
    array (
      'id' => 'global',
      'order' => '10',
      'status' => '0',
      'name' => '公共设置',
      'descs' => '',
    ),
    'seo' => 
    array (
      'id' => 'seo',
      'order' => '20',
      'status' => '0',
      'name' => 'SEO优化',
      'descs' => '',
    ),
    'other' => 
    array (
      'id' => 'other',
      'order' => '100',
      'status' => '1',
      'name' => '其它设置',
      'descs' => '',
    ),
    'receivePushSet' => 
    array (
      'id' => 'receivePushSet',
      'order' => '212',
      'status' => '1',
      'name' => '推送设置',
      'descs' => '',
    ),
    'die' => 
    array (
      'id' => 'die',
      'order' => '333',
      'status' => '1',
      'name' => '死亡',
      'descs' => '',
    ),
  ),
  'dropTables' => 
  array (
  ),
  '_navTitle' => 'APP',
  '_dzinstall' => '0',
  '_pageNavs' => 
  array (
    1 => 
    array (
      'id' => 1,
      'order' => '1',
      'status' => '',
      'name' => '管理快捷导航',
      'link' => 'admin-quick-nav',
      'forceGen' => '1',
      'gen' => '0',
      'descs' => '添加、删除、修改',
    ),
    2 => 
    array (
      'id' => 2,
      'order' => '2',
      'status' => '',
      'name' => '个人设置',
      'link' => 'my-setting',
      'forceGen' => '1',
      'gen' => '0',
      'descs' => '',
    ),
  ),
)),
'displayorder'=>'0',
'uptime'=>'1678009468',
'addtime'=>'1638201511',
);;