<?php !defined('INLEV') && exit('Access Denied LEV');
 return array(
'name'=>'禁止IP访问',
'identifier'=>'ipban',
'classdir'=>'levs',
'descs'=>'禁止IP直接EXIT，或者rewrite IP',
'copyright'=>'Lev',
'version'=>'20210919.04',
'versiontime'=>'1674967818',
'settings'=>serialize(array (
  '_adminNavs' => 
  array (
    1 => 
    array (
      'id' => 1,
      'order' => '1',
      'name' => '访问记录',
      'target' => '1',
      'status' => '',
      'tableName' => '{{%ipban_record}}',
      'link' => 'admin-record',
      'forceGen' => '0',
    ),
    3 => 
    array (
      'id' => 3,
      'order' => '3',
      'name' => 'IP统计',
      'target' => '1',
      'status' => '',
      'tableName' => '{{%ipban_ranks}}',
      'link' => 'admin-ranks/ip',
      'forceGen' => '',
    ),
    4 => 
    array (
      'id' => 4,
      'order' => '4',
      'name' => '用户统计',
      'target' => '1',
      'status' => '',
      'tableName' => '0',
      'link' => 'admin-ranks/user',
      'forceGen' => '1',
    ),
    6 => 
    array (
      'id' => 6,
      'order' => '6',
      'name' => '页面统计',
      'target' => '1',
      'status' => '',
      'tableName' => '0',
      'link' => 'admin-ranks/page',
      'forceGen' => '1',
    ),
    5 => 
    array (
      'id' => 5,
      'order' => '8',
      'name' => '汇总',
      'target' => '1',
      'status' => '',
      'tableName' => '0',
      'link' => 'admin-ranks/total',
      'forceGen' => '1',
    ),
    2 => 
    array (
      'id' => 2,
      'order' => '20',
      'name' => 'IP统计(旧)',
      'target' => '1',
      'status' => '',
      'tableName' => '{{%ipban_record_census}}',
      'link' => 'admin-census',
      'forceGen' => '',
    ),
  ),
  '_adminClassify' => 
  array (
    'ipban' => 
    array (
      'id' => 'ipban',
      'order' => '1',
      'status' => '0',
      'name' => '禁止IP设置',
      'descs' => '',
    ),
    'iprecord' => 
    array (
      'id' => 'iprecord',
      'order' => '2',
      'status' => '0',
      'name' => 'IP统计设置',
      'descs' => '按天统计访问IP',
    ),
  ),
  'dropTables' => 
  array (
    0 => '{{%ipban_record}}',
    1 => '{{%ipban_record_census}}',
    2 => '{{%ipban_ranks}}',
  ),
)),
'displayorder'=>'0',
'uptime'=>'1667798815',
'addtime'=>'1638247212',
);;