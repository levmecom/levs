<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-25 09:57
 *
 * 项目：rm  -  $  - headerHtm.php
 *
 * 作者：liwei 
 */

?>

<div class="card card-header">
    <div class="flex-box">
        <select class="changeStatusv">
            <option value="all">全部</option>
            <?php foreach ($statusArr as $k => $name) {?>
                <option value="<?=$k?>" <?=is_numeric($status) && $k==$status ? 'selected' : ''?>><?=$name?></option>
            <?php }?>
        </select>
        <div class="buttons-row" style="margin-left: 10px"><?=$as?></div>
        <div class="flex-box">
            <input type="text" class="wd100 srhUid showTitleBtn" style="margin: 0 10px" name="srhUid" value="<?=Lev::GPv('uid')?>" placeholder="UID搜索直达，回车提交">
        </div>
    </div>
    <?php if (\modules\levs\modules\ipban\helpers\ipbanSetHelper::openIPrecord() == '2') {?>
    <div class="buttons-row scale8">
        <a class="button-fill button button-small ajaxBtn censusIpBtn" href="<?=Lev::toReRoute(['admin-record/census-ip'])?>">更新</a>
        <a class="button-fill button button-small ajaxBtn censusIpBtn color-blackg" href="<?=Lev::toReRoute(['admin-record/census-ip', 'h'=>24])?>">实时</a>
    </div>
    <?php }?>
</div>

<script>
    jQuery(function () {
        Levme.onClick('.censusIpBtn', function () {
            showIconLoader(true);
            Levme.showNotices('【提示】统计可能需要较长时间，请耐心等候');
        });
        Levme.onOnce('change', '.changeStatusv', function () {
            var val = parseInt(jQuery(this).val());
            window.location = changeUrlArg(Levme.locationHref(), 'status', val);
        });
        Levme.onOnce('keyup', 'input.srhUid', function (e) {
            if (e.which === 13) {
                var uid = parseInt(jQuery(this).val());
                if (!uid || uid <1) {
                    levtoast('用户UID，必须是大于0的整数');
                    return;
                }
                window.location = changeUrlArg(Levme.locationHref(), 'uid', uid);
            }
        });
    });
</script>