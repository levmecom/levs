<?php
/**
 * Copyright (c) 2022-2222   All rights reserved.
 *
 * 创建时间：2022-11-04 11:12
 *
 * 项目：levs  -  $  - ipbanColumnsHelper.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\levs\modules\ipban\helpers;

class ipbanColumnsHelper
{

    public static function setopenIPrecord() {
        return [
            0 => '关闭',
            1 => '直接存入数据库(荐)',
            2 => '写入文件(旧版)',
        ];
    }

    public static function statusArr() {
        return [
            0 => '正常',
            1 => '关闭',
            2 => '蜘蛛',
            3 => '微信',
            4 => '支付宝',
            5 => '百度',
            6 => 'QQ',
            7 => 'APP'
        ];
    }
}