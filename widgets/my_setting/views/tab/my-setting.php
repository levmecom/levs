<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-03-05 11:38
 *
 * 项目：levs  -  $  - my-setting.php
 *
 * 作者：likeqian
 */

//!defined('INLEV') && exit('Access Denied LEV');
?>

<div class="my-setting-main">

    <?php if (\lev\widgets\login\loginWidget::otherLogin()):?>
        <div class="list-block card no-hairlines linef6">
            <ul>
                <?php if ($apkInfo = \lev\widgets\login\loginWidget::getApk()){?>
                    <li class="apkDownBox">
                        <a class="item-content item-content-32 item-link <?php echo $apkInfo['AclassEnd']?>">
                            <div class="item-media icon-img">
                                <?php echo $apkInfo['=logoupload']?>
                            </div>
                            <div class="item-inner bs14">
                                <div class="item-title scale8 transl">APP 下载</div>
                                <div class="item-after date transr">v<?php echo $apkInfo['version']?></div>
                                <div class="item-after date transr" style="max-width:40%">
                                    <p style="width:100%;text-overflow: ellipsis;overflow: hidden"><?php echo $apkInfo['desc']?></p>
                                    <cir v="<?php echo $apkInfo['version']?>" class="inblk scale9 bg-red v_dofade" style="position: absolute;top: -5px;right: -20px;z-index: 9999999999;height: 10px;border-radius: 50%;width: 10px !important;border: 1px solid #fbf85d;display:none"></cir>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php }?>
                <li>
                    <a class="item-content item-content-32 item-link swiper-no-swiping clearCacheBtn">
                        <div class="item-media"><svg class="icon scale7" style="font-size:20px"><use xlink:href="#fa-trash"></use></svg></div>
                        <div class="item-inner bs14">
                            <div class="item-title scale8 transl">清理缓存</div>
                            <div class="item-after date transr cacheSizeBox">0B</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    <?php endif;?>

    <?php if (\lev\helpers\ModulesHelper::isInstallModule('languages')) {?>
        <div class="card list-block no-hairlines linef6">
            <a class="item-content item-content-32 item-link swiper-no-swiping openActions Inajax" href="<?=\modules\levs\modules\languages\helpers\languagesUrlHelper::languages()?>">
                <div class="item-media"><svg class="icon"><use xlink:href="#fa-hkj"></use></svg></div>
                <div class="item-inner bs14">
                    <div class="item-title scale8 transl"><?=\modules\levs\modules\languages\helpers\languagesColumnsHelper::myLanguage()?></div>
                    <div class="item-after date transr">语言切换</div>
                </div>
            </a>
        </div>
    <?php }?>

    <div class="list-block card no-hairlines linef6">
        <ul>
            <?php if (Lev::$app['uid'] <1):?>
                <li>
                    <a class="item-content item-content-32 openLoginBtn item-link">
                        <div class="item-media">
                            <svg class="icon"><use xlink:href="#fa-user"></use></svg>
                        </div>
                        <div class="item-inner bs14">
                            <div class="item-title scale8">登陆</div>
                            <div class="item-after date"></div>
                        </div>
                    </a>
                </li>
            <?php else:?>
                <li class="item-content item-content-32 item-link">
                    <div class="item-media">
                        <a class="is_ajax_a flex-box alncter" href="<?=Lev::toReWrRoute(['upload/set-avatar'])?>">
                            <img src="<?php echo \lev\helpers\UserHelper::avatar()?>" style="border-radius:50%;width:15px;height:15px">
                        </a>
                    </div>
                    <div class="item-inner bs14">
                        <?php if (!Lev::stget('openEditUsername', 'levs')):?>
                            <a class="item-title scale8 editField transl color-black" href="<?=Lev::toReRoute(['login/edit-username'])?>" title="<?=\lev\widgets\login\loginWidget::editUsernameTips()?>" opname="username" opval="<?=Lev::$app['username']?>">
                                <svg class="icon"><use xlink:href="#fa-compose"></use></svg>
                                <username style="font-size:16px"><?php echo Lev::$app['username']?></username>
                                <inputs class="hiddenx">
                                    <input type="password" name="pwd" value="" placeholder="请输入密码">
                                </inputs>
                            </a>
                        <?php else:?>
                            <a class="item-title scale8 transl color-black"><?=Lev::$app['username']?></a>
                        <?php endif;?>
                        <a class="item-after date exitLoginOutBtn">退出</a>
                    </div>
                </li>
                <li class="item-content item-content-32 item-link">
                    <div class="item-media">
                        <svg class="icon"><use xlink:href="#fa-lock"></use></svg>
                    </div>
                    <div class="item-inner bs14">
                        <a class="item-title scale8 editField transl color-black" href="<?=Lev::toReRoute(['login/edit-password'])?>" title="请输入新密码" opname="newpwd" opval="">
                            修改密码
                            <inputs class="hiddenx">
                                <input type="password" name="pwd" value="" placeholder="请输入旧密码，未设置请留空">
                            </inputs>
                        </a>
                        <a class="item-after date" onclick="Levme.showNotices('忘记密码，请通过绑定三方登陆找回')">忘记密码?</a>
                    </div>
                </li>
            <?php endif;?>
        </ul>
    </div>
    <?php if (\lev\helpers\ModulesHelper::isOpenModule('levbdu')):?>
        <a class="list-block card flex-box ju-sa button-fill button color-blackg" href="<?=\lev\helpers\UrlHelper::toModule('levbdu')?>">切换账号</a>
    <?php endif;?>
</div>