<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-25 11:51
 *
 * 项目：levs  -  $  - default3.php
 *
 * 作者：likeqian
 */

use lev\helpers\ModulesHelper;
use lev\helpers\UrlHelper;

//!defined('INLEV') && exit('Access Denied LEV');
?>

<div class="list-block mg0 no-hairlines linef6">
    <ul>
        <?php foreach ($quicknav as $v) {?>
            <li class="item-content item-content-32 item-link">
                <div class="item-media">
                    <label class="delQuickNavBtn" link="<?=$v['link']?>">
                        <svg class="icon font12 color-gray"><use xlink:href="#fa-trash"></use></svg>
                    </label>
                </div>
                <a class="item-inner bs14 color-blackg" href="<?=$v['link']?>">
                    <div class="item-title font14 wdmin"><?=$v['title']?></div>
                    <div class="item-after">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#<?=$v['icon']?>"></use></svg>
                    </div>
                </a>
            </li>
        <?php }?>
    </ul>
</div>
