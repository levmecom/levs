<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-25 11:51
 *
 * 项目：levs  -  $  - default3.php
 *
 * 作者：likeqian
 */

use lev\helpers\ModulesHelper;
use lev\helpers\UrlHelper;

//!defined('INLEV') && exit('Access Denied LEV');
?>

<div class="list-block mg0 no-hairlines linef6">
    <ul>
        <?php foreach ($modules as $r) { $setNav = ModulesHelper::getClassify($r['identifier'], 1, 1);?>
            <li class="item-content item-content-38 item-link">
                <div class="item-media wd40">
                    <btn class="addAdminQuickNavBtn" href="<?=\modules\levs\helpers\levsUrlHelper::addAdminQuickNav($r['identifier'], $r['id'], 'identifier')?>"><svg class="icon color-blackg"><use xlink:href="#fa-add"></use></svg></btn>
                </div>
                <a class="item-inner bs14 color-blackg" href="<?=Lev::toReRoute(['admin-modules', 'id'=>'levs', 'identifier'=>$r['identifier']])?>">
                    <div class="item-title font14 wdmin"><?=$r['name']?><absxg><?=$r['identifier']?></absxg></div>
                    <div class="item-text scale8 transr"><?=$r['descs']?></div>
                </a>
            </li>
            <?php foreach ($setNav as $key => $v) { $href = UrlHelper::supermanSettings($r['identifier'], $key);?>
                <li class="item-content item-content-38 item-link">
                    <div class="item-media wd40">
                        <btn class="addAdminQuickNavBtn" href="<?=\modules\levs\helpers\levsUrlHelper::addAdminQuickNav($r['identifier'], $v['id'], 'settings')?>"><svg class="icon color-blackg"><use xlink:href="#fa-add"></use></svg></btn>
                    </div>
                    <a class="item-inner bs14 color-blackg" href="<?=$href?>">
                        <div class="item-title font14"><?=$v['name']?></div>
                        <div class="item-after date transr"><?=$v['descs']?></div>
                    </a>
                </li>
            <?php }?>
        <?php $setNav = ModulesHelper::getAdminNavs($r);?>
            <?php foreach ($setNav as $key => $v) { $href = $v['link']; ?>
                <li class="item-content item-content-38 item-link">
                    <div class="item-media wd40">
                        <btn class="addAdminQuickNavBtn" href="<?=\modules\levs\helpers\levsUrlHelper::addAdminQuickNav($r['identifier'], $v['id'], 'adminNav')?>"><svg class="icon color-black"><use xlink:href="#fa-add"></use></svg></btn>
                    </div>
                    <a class="item-inner bs14 color-black <?=$v['_target'].$v['_link']?>">
                        <div class="item-title font14"><?=$v['name']?></div>
                        <div class="item-after date transr"><?=$v['descs']?></div>
                    </a>
                </li>
            <?php }?>
        <?php }?>
    </ul>
</div>
