<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-25 12:42
 *
 * 项目：levs  -  $  - levsCacheFileHelper.php
 *
 * 作者：likeqian
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\levs\helpers;

use Lev;
use lev\helpers\cacheFileHelpers;

class levsCacheFileHelper extends cacheFileHelpers
{

    public static $cacheDir = '/cache/levs';

    public static $fileExt = '.bin';

    public static function getAdminQuickNavs($key = null) {
        return static::adminQuickNavs(null, $key);
    }

    public static function setAdminQuickNavs(array $val) {
        $pkey = 'adminQuickNavs';
        static::setc($pkey, $val);
    }

    public static function delAdminQuickNavs($key = null) {
        static::adminQuickNavs(null, $key, true);
    }

    public static function joinAdminQuickNavs(array $val, $key) {
        static::adminQuickNavs($val, $key);
    }

    public static function adminQuickNavs(array $val = null, $key = null, $del = false) {
        $pkey = 'adminQuickNavs';
        $arr =  static::joinc($pkey);
        if ($del) {
            if ($key === null) {
                $arr = [];
            }else {
                unset($arr[$key]);
            }
            static::setAdminQuickNavs($arr);
        }else if ($val !== null) {
            $arr =  static::joinc($pkey, $val, $key);
        }else if ($key !== null) {
            return Lev::arrv($key, $arr);
        }
        return $arr;
    }
}