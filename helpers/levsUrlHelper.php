<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-25 15:28
 *
 * 项目：levs  -  $  - levsUrlHelper.php
 *
 * 作者：likeqian
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\levs\helpers;

use Lev;

class levsUrlHelper
{
    public static function mySetting()
    {
        return Lev::toReWrRoute(['my-setting', 'id'=>'levs']);
    }

    public static function clearCache() {
        return Lev::toReWrRoute(['my-setting/clear-cache', 'id'=>'levs']);
    }

    public static function addAdminQuickNav($iden, $navid, $navtype) {
        return Lev::toReRoute(['admin-quick-nav/add', 'id'=>'levs', 'iden'=>$iden, 'navid'=>$navid, 'navtype'=>$navtype]);
    }

    public static function adminQuickNav()
    {
        return Lev::toReRoute(['admin-quick-nav', 'id'=>'levs']);
    }

}