<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-25 19:20
 *
 * 项目：levs  -  $  - levsColumnsHelper.php
 *
 * 作者：likeqian
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace modules\levs\helpers;

use Lev;
use lev\helpers\ModulesHelper;

class levsColumnsHelper
{

    public static function setadminQuickNavs() {
        $modules = ModulesHelper::findAll(1);
        foreach ($modules as $v) {
            $res[$v['identifier']] = '['.$v['identifier'].']'.$v['name'];
        }
        return $res;
    }

}