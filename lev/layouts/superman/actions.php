<?php
/**
 * Copyright (c) 2022-2222   All rights reserved.
 *
 * 创建时间：2022-11-27 13:35
 *
 * 项目：levs  -  $  - actions.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');
?>


<div class="page page-admin">
    <?php Lev::navbarAdmin('','','',1); Lev::toolbarAdmin(); ?>

    <div class="page-content" style="padding-top: 75px !important;">
        <div class="page-content-inner card" style="max-width:calc(100% - 20px)">
            <div class="card-header">
                <div>模块磁盘路径：<tips><?=$fileDir?></tips></div>
            </div>
            <div class="card-content-inner data-xtable">
                <table>
                    <tr>
                        <th>方法名</th>
                        <th>注释/访问</th>
                    </tr>
                    <?php foreach ($controllers as $v) { $actions = Lev::getActionsInController($v); ?>
                        <tr>
                            <td colspan="2" style="padding: 0;height: 20px"><tips>控制器磁盘路径：<?=$v?></tips></td>
                        </tr>
                        <?php foreach ($actions as $r) {?>
                        <tr>
                            <td><?=$r['func']?></td>
                            <td>
                                <p class="gray scale8 transl" title="<?=$r['note']?>"><?=Lev::cutString($r['note'], 80)?></p>
                                <p>
                                    <a class="transl button-fill button button-small scale8 wd40 wdmin" target="_blank" _bk="1" href="<?=Lev::toReRoute([$r['route'], 'id'=>$iden])?>"><?=$r['navname']?></a>
                                </p>
                            </td>
                        </tr>
                        <?php }?>
                    <?php }?>
                </table>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
