<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-07 22:45
 *
 * 项目：upload  -  $  - LevModulesModel.php
 *
 * 作者：liwei 
 */

namespace lev\helpers;

use Lev;
use lev\base\Migrationv;
use lev\base\Modelv;
use lev\widgets\inputs\inputsWidget;
use modules\levs\helpers\levsSetHelper;

!defined('INLEV') && exit('Access Denied LEV');


class LevSettingsModel extends Modelv
{

    public static function tableName($val = '', $prefix = true) {
        return parent::tableName($val ?: '{{%lev_settings}}', $prefix);
    }

    public static function getColumns() {
        return dbHelper::getTableColumns(static::tableName());
    }

    public static function getCheckModuleSettings($iden) {
        $ckres = [];
        $lists = static::findAll(['moduleidentifier'=>$iden], '', [], 'moduleidentifier,id,classify,inputname');
        foreach ($lists as $v) {
            $ckres[$v['inputname'].'_'.$v['classify']] = $v;
        }
        return $ckres;
    }

    /**
     * 更新配置项及缓存
     * @param $iden
     * @param $inputname
     * @param $inputvalue
     * @param int $status
     * @return array|string
     */
    public static function updateSettingsOne($iden, $inputname, $inputvalue, $status = 0) {
        if ($ck = static::findOne(['moduleidentifier'=>$iden, 'inputname'=>$inputname, 'status'=>$status])) {
            static::update(['inputvalue'=>$inputvalue], ['id'=>$ck['id']]);
            inputsWidget::setCaches($iden);
        }
        return $ck;
    }

    public static function updateSettings($iden, array $rows = null) {
        $count = 0;
        if ($rows) {
            $mysets = static::getCheckModuleSettings($iden);
            foreach ($rows as $v) {
                if (!empty($v['_delete']) && $v['_delete'] === true) {
                    SettingsHelper::delete(['inputname'=>$v['inputname'], 'classify'=>$v['classify'], 'moduleidentifier'=>$iden]);
                }else {
                    unset($v['id']);
                    $v['moduleidentifier'] = $iden;
                    $data = SettingsHelper::safeColumns($v);
                    if ($data) {
                        $ckKey = $v['inputname'].'_'.$v['classify'];
                        if (isset($mysets[$ckKey])) {
                            unset($data['inputvalue'], $data['addtime']);
                            SettingsHelper::update($data, ['id' => $mysets[$ckKey]['id']]);
                        } else {
                            SettingsHelper::insert($data);
                        }
                        $count ++;
                    }
                }
            }
        }
        return $count;
    }

    public static function getModuleSettingsInfo($iden, $key = '', $classify = '') {
        $where = $classify ? " AND classify='$classify' " : '';
        return static::findAll("moduleidentifier='$iden' $where ORDER BY displayorder ASC", $key);
    }

    public static function getModuleTabSettings($iden, $classify = '', $keyfield = '') {
        $where = $classify ? " AND classify='$classify' " : '';
        return static::findAll("moduleidentifier='$iden' $where AND status>0 ORDER BY status ASC, displayorder ASC", $keyfield);
    }

    public static function getModuleSetSettings($iden, $classify = '') {
        $where = $classify ? " AND classify='$classify' " : '';
        return static::findAll("moduleidentifier='$iden' $where AND status=0 ORDER BY displayorder ASC");
    }

    public static function setLinkAccess() {
        return levsSetHelper::setLinkAccess();
    }

    public static function field_scoretype() {
        return ScoreHelper::scoretypesyy();
    }
    public static function setscoretype() {
        return ScoreHelper::scoretypesyy();
    }
    public static function setscoretypesyy() {
        return ScoreHelper::scoretypesyy();
    }

    public static function tabClassify($iden) {
        $mudInfo = ModulesHelper::getModuleInfo($iden);
        $tabs = Lev::getSettings($mudInfo['settings'], 'dropTables');
        $tabClassify = [];
        if ($tabs) {
            foreach ($tabs as $v) {
                $tab = Modelv::quoteTableName(Modelv::preTableName($v), false);
                $tabClassify[$tab] = $tab;
            }
        }
        if ($iden === 'levmodules') {
            $tabClassify += static::baseTabClassify();
        }
        return $tabClassify;
    }
    public static function baseTabClassify() {
        $tabClassify = [];

        $tabs = Migrationv::getLevBaseTables();
        foreach ($tabs as $v) {
            $tab = Modelv::quoteTableName(Modelv::preTableName($v), false);
            $tabClassify[$tab] = $tab;
        }
        return $tabClassify;
    }

    /**
     * @param array $fieldInfo
     * @return array
     */
    public static function setclassify($fieldInfo = []) {
        $iden = Lev::stripTags(Lev::GPv('iden')) ?: Lev::$app['iden'];
        $classify = ModulesHelper::getClassify($iden) + static::tabClassify($iden);
        return ModulesHelper::getSetClassify($iden, $classify);
    }
}