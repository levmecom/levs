<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-29 15:34
 *
 * 项目：upload  -  $  - ScoreHelper.php
 *
 * 作者：liwei 
 */

namespace lev\helpers;

use Lev;
use lev\base\Modulesv;

!defined('INLEV') && exit('Access Denied LEV');


class ScoreHelper
{

    public static function myScoresStr($key = null, $outerBox = true) {
        $htm = '';
        $myScores = static::myScores('=');
        if ($key !== null) {
            //$key = ltrim($key, '=');
            if (isset($myScores[$key])) {
                $icon = static::scoreIcon($key);
                $htm = '<my-sc class="s-'.$icon.'" title="'.$myScores[$key]['score'].'">'.static::svgScoreIcon($key).' '.Lev::formatNumber($myScores[$key]['score'], 10).$myScores[$key]['title'].'</my-sc>';
            }
        }else {
            foreach ($myScores as $v) {
                $htm .= static::myScoresStr($v['id'], false);
            }
        }
        return $outerBox ? '<my-scores>'.$htm.'</my-scores>' : $htm;
    }

    public static function scoreByJson($json, $format = false, $showType = false, $default = '空')
    {
        $arr = is_array($json) ? $json : json_decode($json, true);
        if (empty($arr[1])) {
            return $default;
        }
        $score = floatval($arr[0]);
        $scoretype = trim($arr[1]);
        if ($score <1 || !static::scorenamex($scoretype, '', '')) {
            return $default;
        }
        $num = floatval($arr[0]);
        $icon = '';
        $sname = static::scorenamex($arr[1]);
        if ($showType) {
            if ($showType === 1) {
                $icon = static::svgScoreIcon($arr[1]);
                $sname = '';
            }else if ($showType === 3) {
                $icon = static::svgScoreIcon($arr[1]);
            }
        }
        return $icon.($format ? Lev::formatNumber($num) : $num).$sname;
    }

    public static function svgScoreIcon($key) {
        $icon = static::scoreIcon($key);
        return '<svg class="icon sc-'.$icon.'" aria-hidden="true"><use xlink:href="#fa-'.$icon.'"></use></svg>';
    }
    public static function scoreIcon($key) {
        $icons = [
            '=1' => 'jyz',
            '=2' => 'copper',
            '=3' => 'exp',
            '1' => 'jyz',
            '2' => 'copper',
            '3' => 'exp',
            'cnymoney' => 'cnymoney',
            'vcoin' => 'vcoin',
            'money' => 'money',
            'diamond' => 'diamond',
            'gold' => 'gold',
            'coins' => 'jb',
            'moneys' => 'rmb',
        ];
        return isset($icons[$key]) ? $icons[$key] : 'score';
    }

    public static function scoretypes($tip = '(DZ)') {
        empty(Lev::$app['scoretypes']) && Lev::$app['scoretypes'] = static::scoretypesyy($tip);
        return Lev::$app['scoretypes'];
    }

    public static function scoretypesyy($tip = '（论坛）') {
        $scoretypes = Lev::actionObjectMethodIden('levyy', 'modules\levyy\table\userHelper', [], 'wealths') ?: [];
        if (!empty(Lev::$app['isDiscuz'])) {//static::scoretypes();
            if ($scoretype = Lev::actionObjectMethod('lev\dz\discuzHelper', [], 'scoretypes')) {
                foreach ($scoretype as $k => $name) {
                    $scoretypes['=' . $k] = $name . $tip;
                }
            }
        }
        return $scoretypes;
    }

    public static function scorenamex($scoreid, $tip = '', $default = null) {
        $scoretypes = static::scoretypesyy($tip);
        $scoretypes+= static::scoretypes();
        return Lev::arrv($scoreid, $scoretypes, $default);
    }

    public static function scorename($scoreid, $yy = 1, $tip = '') {
        if ($yy) {
            $scoretypes = static::scoretypesyy($tip);
            return Lev::arrv($scoreid, $scoretypes);
        }else {
            return static::scorenamex($scoreid, $tip);
        }
    }

    public static function acscoreUse($spend, $notice = '', $scoretype = '', $uid = 0, $title = '', $scoreArr = [], $emailSubtypeid = 0) {
        if (!static::acscore($spend, $notice, $scoretype, $uid, $title, $scoreArr, $emailSubtypeid)) {
            return Lev::responseMsg(-1041, '抱歉【'.static::scorenamex($scoretype).'】积分不足');
        }
        $tip = static::scorenamex($scoretype) . ($spend >0 ? ' <b class=yellow> +'.$spend : ' <b class=gray> '.$spend).'</b> ';
        return Lev::responseMsg(1, $tip);
    }
    public static function acscore($spend, $notice = '', $type = 0, $uid = 0, $title = '', $scoreArr = [], $emailSubtypeid = 0) {
        return Lev::getDB()->acscore($spend, $notice, $type, $uid, $title, $scoreArr, $emailSubtypeid);
    }
    public static function acscores($uid, $scoresarr, $logTitle = '', $logDescs = '', $safeFlag = 0) {
        foreach ($scoresarr as $scoreid => $score) {
            break;
        }
        return static::acscoreUse($score, $logTitle, $scoreid, $uid, $logDescs, $scoresarr);
    }

    public static function setMyScores($scoreid, $scoreTotal) {
        empty(Lev::$app['myScores']) && static::myScores('=');
        Lev::$app['myScores'][$scoreid]['score'] = $scoreTotal;
        Lev::$app['myScores'][ltrim($scoreid, '=')]['score'] = $scoreTotal;
    }

    public static function myScores($pre = '', $force = false) {
        if ($force || empty(Lev::$app['myScores'])) {
            $scores = Lev::getDB()->myScores($pre);
            Lev::$app['myScores'] = $scores + static::myWealth();
        }
        return Lev::$app['myScores'];
    }

    /**
     * @return array
     * @see \modules\levyy\table\userHelper::myWealth()
     */
    public static function myWealth() {
        if (!Modulesv::isInstallModule('levyy')) {
            return [];
        }
        return Lev::actionObjectMethod('modules\levyy\table\userHelper', [], 'myWealth') ?: [];
    }
    public static function useWealth($uid, $wealthArr, $ignore = false, $descs = '', $emailSubtypeid = 0, $identypeid = 0) {
        if (!Modulesv::isInstallModule('levyy')) {
            return Lev::responseMsg(-2200, '抱歉，请先安装【来赚钱插件】');
        }
        //return Lev::actionObjectMethod('modules\levyy\table\userHelper', [$uid, $wealthArr, $ignore, $descs], 'useWealth');
        return \modules\levyy\table\userHelper::useWealth($uid, $wealthArr, $ignore, $descs, $emailSubtypeid, $identypeid);
    }

    public static function acscoreUses($spend, $notice = '', $scoretype = '', $uid = null, $title = '', $scoreArr = [], $emailSubtypeid = 0, $identypeid = 0) {
        //$uid === null && $uid = Lev::$app['uid'];
        !$uid && $uid = Lev::$app['uid'];
        $key = static::isSysscore($scoretype);
        if (!empty(Lev::$app['isDiscuz']) || $key) {
            if ($spend === null) {
                foreach ($scoreArr as $scoreid => $spend) {
                    $key = ltrim($scoreid, '=');
                    break;
                }
            }
            $msg = static::acscoreUse($spend, $notice, $key, $uid, $title, $scoreArr, $emailSubtypeid);
        }else {
            $spend !== null &&
            $scoreArr[$scoretype] = $spend;
            $msg = static::useWealth($uid, $scoreArr, true, $notice.$title, $emailSubtypeid, $identypeid);
        }
        return $msg;
    }

    public static function isSysscore($key) {
        return strpos($key, '=') === 0 ? substr($key, 1) : '';
    }

}