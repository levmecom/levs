<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-01-18 18:38
 *
 * 项目：levs  -  $  - EditorsHelper.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace lev\helpers;

use Lev;
use modules\editors\helpers\editorsColumnsHelper;
use modules\editors\helpers\editorsSetHelper;

class EditorsHelper
{

    /**
     * @return int
     * @see editorsSetHelper::defaultEditor();
     */
    public static function defaultEditor() {
        return intval(Lev::stget('defaultEditor', 'editors'));
    }

    public static function editors() {
        if (ModulesHelper::isInstallModule('editors')) {
            return editorsColumnsHelper::editors();
        }
        return [
            0 => 'wangEditor',
        ];
    }
}