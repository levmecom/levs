<?php
/**
 * Copyright (c) 2022-2222   All rights reserved.
 *
 * 创建时间：2022-07-26 16:55
 *
 * 项目：levs  -  $  - EmailHelper.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace lev\helpers;

use Lev;
use modules\email\table\emailFormHelper;

class EmailHelper
{

    public static function scoreUseEmail($subtypeid, $uid, $title, $contents = '') {
        if ($subtypeid) {
            $eupData['contents'] = $contents ?: '您的财富值发生了新的变化！';
            $eupData['tmp'] = 0;//不弹出
            $eupData['tmptype'] = 0;
            $eupData['subtypeid'] = $subtypeid;
            $eupDataForce['subtypeid'] = $subtypeid;
            EmailHelper::saveFormUids($uid, $title, [], 7, $eupData, $eupDataForce);
        }
    }

    public static function sendEmail($uid, $title, $contents) {
        $eupData['contents'] = $contents;
        $eupData['tmp'] = 0;//不弹出
        $eupData['tmptype'] = 0;
        $eupData['typeid'] = 3;
        $msg = EmailHelper::saveFormUids($uid, $title, [], 7, $eupData);
        if (empty($msg[0])) {
            return Lev::responseMsg(-44, '发送失败');
        }elseif (isset($msg[0]['error'])) {
            return $msg[0];
        }
        return Lev::responseMsg(1, '邮件已发送');
    }

    /**
     * 邮件具有唯一约束性组合字段。
     * 在约束字段 uid_iden_subtypeid_addtime 都相同的情况下，请合并成一条再插入
     * 只插入新数据，不更新
     *
     * @param int|array $uids 1||[1,2,3,4,5,6]
     * @param string $title
     * @param array $scores
     * @param int $outday
     * @param array $upData
     * @param array $upDataForce
     * @return bool|mixed
     * @see emailFormHelper::saveFormUids();
     */
    public static function saveFormUids($uids, $title, $scores = [], $outday = 7, $upData = [], $upDataForce = null) {
        $param = [$uids, $title, $scores, $outday, $upData, $upDataForce, []];
        return Lev::actionObjectMethodIden('email', 'modules\email\table\emailFormHelper', $param, 'saveFormUids');
    }

    /**
     * 邮件具有唯一约束性组合字段。
     * 在约束字段 uid_iden_subtypeid_addtime 都相同的情况下，请合并成一条再插入
     * 只插入新数据，不更新
     *
     * @param $title
     * @param int $tmp
     * @param int $uid
     * @param array $scores
     * @param string $contents
     * @param int $tmptype
     * @param int $outday
     * @param array $upData
     * @return bool|mixed
     * @see emailFormHelper::saveFormCall();
     */
    public static function saveFormCall($title, $tmp = 3, $uid = 0, $scores = [], $contents = '', $tmptype = -2, $outday = 7, $upData = []) {
        $param = [$title, $tmp, $uid, $scores, $contents, $tmptype, $outday, $upData, null, []];
        return Lev::actionObjectMethodIden('email', 'modules\email\table\emailFormHelper', $param, 'saveFormCall');
    }

}