<?php
/**
 * Copyright (c) 2022-2222   All rights reserved.
 *
 * 创建时间：2022-04-17 23:36
 *
 * 项目：levs  -  $  - swipe.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');

empty($param) || extract($param);
?>

<div class="page <?=Lev::$app['iden'],' ',$pageName,' ',$tabViewName?>" data-page="<?=$pageName?>">
    <?php include \lev\widgets\tab_view\tabViewWidget::toolbarFile(); ?>

    <!--navbar-->
    <div class="navbar navbar-bgcolor-red app-navbar">
        <div class="navbar-inner">
            <?php include \lev\widgets\tab_view\tabViewWidget::navbarInnerFile(); ?>

            <!--导航-->
            <?php if (!empty($subnavs)):?>
                <div class="subnavbar nav-links <?=$subnavHide?>">
                    <div class="data-table slideBtnBox">
                        <?php foreach ($subnavs as $v):?>
                            <a class="slideBtn_ slideBtn_<?=$v['tabid'],' ',$v['attr']?>" data-slideid="<?=$v['tabid']?>" href="<?=$v['url']?>">
                                <?=$v['name']?>
                            </a>
                        <?php endforeach;?>
                    </div>
                    <a class="moreIcon more-icon link icon-only button button-fill color-gray hiddenx">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#fa-bars"></use></svg>
                    </a>
                </div>
            <?php endif;?>

        </div>

    </div>

    <div class="swipeMainBox swiper-container appbg" style="height:100%">

        <div class="swipesBox swiper-wrapper" data-showid="<?php echo $deTabId?>">

            <?php foreach ($subnavs as $v) { include __DIR__ . '/slide.php'; }?>

        </div>
    </div>

    <div class="fotter-box-gen">
        <?php include \lev\widgets\tab_view\tabViewWidget::footerFile(); ?>
    </div>

    <div class="LoadPageAjaxJS hiddenx">
        <script>
            jQuery(function () {
                if (typeof Levme.swipePage === "function") {
                    Levme.swipePage('<?=$pageName?>', <?=json_encode($subnavs ?: [])?>, '<?=$deTabId?>', <?=$noCache?:0?>);
                }else {
                    Levme.showNotices('页面JS过期，请清理缓存并刷新页面');
                }
            });
        </script>
    </div>
</div>
