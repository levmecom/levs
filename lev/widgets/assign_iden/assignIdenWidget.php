<?php
/**
 * Copyright (c) 2022-2222   All rights reserved.
 *
 * 创建时间：2022-06-24 10:35
 *
 * 项目：levs  -  $  - assignIdenWidget.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');

//指向其它模块，无报错 调用其功能

namespace lev\widgets\assign_iden;

use Lev;
use lev\base\Controllerv;
use lev\base\Widgetv;
use lev\helpers\ModulesHelper;
use modules\email\emailHelper;
use modules\email\helpers\emailUrlHelper;
use modules\levmb\helpers\levmbInterfaceHelper;
use modules\levmb\sdk\myphone\myphoneAuthLogin;
use modules\levs\modules\citys\table\citys\citysModelHelper;
use modules\levs\modules\languages\helpers\languagesColumnsHelper;
use modules\levs\modules\languages\helpers\languagesUrlHelper;
use modules\levsign\levsignHelper;

class assignIdenWidget extends Widgetv
{
    /**
     * @return bool|mixed
     * @see levmbInterfaceHelper::authEnvLoginBtn();
     */
    public static function authEnvLoginBtn()
    {
        return Lev::actionObjectMethodIdenOpen('levmb', 'modules\levmb\helpers\levmbInterfaceHelper', [], 'authEnvLoginBtn');
    }

    /**
     * @see levmbInterfaceHelper::statelogin();
     */
    public static function statelogin($redirect = false) {
        if (Lev::$app['uid'] <1) {
            $responeMsg = Lev::actionObjectMethodIdenOpen('levmb', 'modules\levmb\helpers\levmbInterfaceHelper', [], 'statelogin', true);
//            if ($redirect && !empty($responeMsg['referer'])) {
//                Controllerv::redirect($responeMsg['referer']);
//            }
        }
    }

    /**
     * 邮件提醒按钮
     * @return string
     * @see emailUrlHelper::myEmail();
     */
    public static function emailNoticeBtn() {
        if ($href = Lev::actionObjectMethodIdenOpen('email', 'modules\email\helpers\emailUrlHelper', [], 'myEmail')) {
            return '<a class="link icon-only openPP Inajax MyEmailBtn" href="'.$href.'" clsname="MyEmail">
                <svg class="icon"><use xlink:href="#fa-email"></use></svg>
                <n-eml></n-eml>
            </a>';
        }
        return '';
    }

    /**
     * 邮件呼吸JS 每隔一段时间从服务器上检查是否有新邮件
     * @return bool|mixed
     * @see emailHelper::emailNoticeLiveJs();
     */
    public static function emailNoticeLiveJs() {
        return Lev::actionObjectMethodIdenOpen('email', 'modules\email\emailHelper', [], 'emailNoticeLiveJs', true);
    }

    /**
     * @return bool|mixed
     * @see levsignHelper::signNavbarBtn();
     */
    public static function signBtn($openType = 'is_ajax_a')
    {
        return Lev::actionObjectMethodIdenOpen('levsign', 'modules\levsign\levsignHelper', [$openType], 'signNavbarBtn');
    }

    /**
     * @return bool|mixed
     * @see myphoneAuthLogin::APPdownload();
     */
    public static function appDownload()
    {
        return Lev::actionObjectMethodIden('levmb', 'modules\levmb\sdk\myphone\myphoneAuthLogin', [], 'APPdownload');
    }

    /**
     * @param $city
     * @return bool|mixed
     * @see citysModelHelper::cityNameToCityInfo();
     */
    public static function cityNameToCityInfo($cityName)
    {
        return Lev::actionObjectMethodIden('citys', 'modules\levs\modules\citys\table\citys\citysModelHelper', [$cityName], 'cityNameToCityInfo');
    }

    public static function languageBtn()
    {
        if (ModulesHelper::isInstallModule('languages')) {
            return '<a class="link icon-only openActions Inajax" href="'.languagesUrlHelper::languages().'">'.languagesColumnsHelper::myLanguage().'</a>';
        }
    }

}