<?php
/**
 * Copyright (c) 2023-2222   All rights reserved.
 *
 * 创建时间：2023-02-24 21:36
 *
 * 项目：levs  -  $  - flash_v2.php
 *
 * 作者：likeqian
 */

//!defined('INLEV') && exit('Access Denied LEV');
?>


<style>
    .slides-bbox .swiper-slide {height: <?=$height?>px;max-width: 100%}
    .slides-bbox .swiper-slide img {height:<?=$height?>px;/*max-width: 100%*/;max-height: 100%;}
    .slides-bbox .swiper-slide a {align-items: center;display: flex;text-align: center;justify-content: center;height: 100%;background: rgba(0,0,0,0.31);overflow: hidden;}
</style>

<div class="slides-bbox slides-input-2topFlash">
    <div class="swiper-container slides-swiper slides-top-flash">
        <div class="swiper-wrapper">
            <?php foreach ($slidesArr as $v): ?>
                <div class="swiper-slide">
                    <a class="<?=$v['_target'],$v['_link']?>">
                        <img class="lazy" data-src="<?=$v['_src']?>">
                        <?php if (!empty($v['name'])) {?>
                            <p class="flash-name-box"><?=$v['name']?></p>
                        <?php }?>
                    </a>
                </div>
            <?php endforeach;?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>

<?php if (empty($notJs)) {?>
    <script>
        jQuery(function () {
            window.setTimeout(function () { Levme.swiper('2topFlash'); }, 100);
        });
    </script>
<?php }?>

