<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-21 13:38
 *
 * 项目：upload  -  $  - wangeditor.php
 *
 * 作者：liwei 
 */

echo \lev\base\Assetsv::registerCssFile(\lev\base\Assetsv::highlight(1, 2), true);
?>

<div id="wangEditorMain<?php echo $input['inputname']?>" class="noActiveState wangEditorMain<?php echo $input['inputname']?>"></div>
<textarea class="hiddenx wangEditorTextarea<?php echo $input['inputname']?>" name="<?php echo $inputname?>"><?php echo $inputvalue?></textarea>
<input type="hidden" name="editors" value="1">
<input type="hidden" class="_imgIds" name="imgids">

<!-- 引入 wangEditor.min.js -->
<script type="text/javascript">
    (function () {
        'use strict';

        jQuery(function () {
            Levme.script.loadOnce('<?=\lev\base\Assetsv::highlight(1, 1)?>');
            Levme.script.loadOnceAndActionFunc('<?=Lev::$aliases['@assets']?>/statics/editor/wangEditor.min.js', function () {
                wangEditor.init();
            });
        });

        var wangEditor = {
            init:function () {
                var textareaCls = 'textarea.wangEditorTextarea<?php echo $input['inputname']?>';
                var tempKey = "#wangEditorMain<?php echo $input['inputname']?>";
                jQuery(tempKey).removeAttr('data-we-id');
                jQuery(tempKey).html('');

                const E = window.wangEditor;
                const editor = new E(tempKey);
                // 或者 const editor = new E( document.getElementById('div1') );

                editor.config.height = <?php echo floatval($editorHeight)?>;
                // 配置菜单栏，设置不需要的菜单
                editor.config.excludeMenus = [
                    'emoticon',
                    'video'
                ]


                //图片上传接口
                editor.config.uploadImgServer = '<?=$uploadUrl?>';
                editor.config.uploadFileName = 'editor';
                editor.config.uploadImgMaxLength = 1;// 一次最多上传 1 个图片
                editor.config.uploadImgMaxSize = 5 * 1024 * 1024; // 5M
                editor.config.uploadImgHooks = {
                    // 图片上传并返回了结果，想要自己把图片插入到编辑器中
                    // 例如服务器端返回的不是 { errno: 0, data: [...] } 这种格式，可使用 customInsert
                    customInsert: function(insertImgFn, result) {
                        // result 即服务端返回的接口
                        //console.log('customInsert', result)

                        // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
                        result && result.realSrc && insertImgFn(result.realSrc);
                        Levme.setImgIds(result.dbId);
                    }
                }


                // 挂载highlight插件
                typeof hljs !== "undefined" && (editor.highlight = hljs);

                editor.config.placeholder = <?php echo json_encode($input['placeholder'], JSON_UNESCAPED_UNICODE)?>;

                if (jQuery('.mobile-1').length >0) {
                    editor.config.placeholder += "！！！长按输入！！！";
                }

                    editor.create();

                //初始化 编辑器 内容
                Levme.tempDatas[tempKey] = Levme.tempDatas[tempKey] || Levme.decodeHTML(jQuery(textareaCls).val());
                editor.txt.html(Levme.tempDatas[tempKey]);

                editor.config.onchange = function (html) {
                    jQuery(textareaCls).val(Levme.encodeHTML(html));
                    Levme.tempDatas[tempKey] = html;
                }

            }
        };
    })();
</script>

