<?php
/**
 * Copyright (c) 2021-2222   All rights reserved.
 *
 * 创建时间：2021-12-06 14:55
 *
 * 项目：levs  -  $  - usetypescoreyyType.php
 *
 * 作者：liwei
 */

//!defined('INLEV') && exit('Access Denied LEV');


namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\helpers\ScoreHelper;

class usetypescoreyyType
{

    public static function formatViewData($input, $value) {
        $scoretypes = ScoreHelper::scoretypesyy();
        $value = !is_array($value) ? json_decode($value, true) : $value;
        return $value[0].Lev::arrv($value[1], $scoretypes);
    }

}