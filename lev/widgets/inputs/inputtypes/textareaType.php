<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-12 17:41
 *
 * 项目：rm  -  $  - textareaType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;

!defined('INLEV') && exit('Access Denied LEV');


class textareaType
{

    public static function formatViewData($input, $value) {
        return '<div class="show-content-detail">'.$value.'</div>';
    }

}