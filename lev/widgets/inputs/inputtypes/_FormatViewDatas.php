<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-17 13:34
 *
 * 项目：rm  -  $  - _FormatViewDatas.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\helpers\ScoreHelper;
use lev\widgets\inputs\inputsFormWidget;

!defined('INLEV') && exit('Access Denied LEV');


class _FormatViewDatas
{

    /**
     * @param $input
     * @param $value
     * @return false|string
     *
     * @see inputsFormWidget::inputtype();
     */
    public static function run($input, $value) {
        return method_exists(__CLASS__, $method = $input['inputtype'].'Format')
            ? static::$method($input, $value)
            : static::jsonEncodev($input, $value);
    }

    /**
     * 格式化字段数据
     * @param $input
     * @param $value
     * @return false|string
     *
     * @see htmlTextareaType::formatViewData();
     */
    public static function jsonEncodev($input, $value)
    {
        return Lev::actionObjectMethod('lev\widgets\inputs\inputtypes\\'.$input['inputtype'].'Type', [$input, $value], 'formatViewData')
            ?: (is_array($value) ? json_encode($value, JSON_UNESCAPED_UNICODE) : $value);
    }

    public static function slidesFormat($input, $value) {
        return slidesType::formatViewData($input, $value);
    }

    public static function navsFormat($input, $value) {
        return navsType::formatViewData($input, $value);
    }

    public static function tablesnavFormat($input, $value) {
        return navsType::formatViewData2($input, $value);
    }

    public static function wangeditorFormat($input, $value) {
        return wangeditorType::formatViewData($input, $value);
    }

    public static function tablesFormFormat($input, $value) {
        return tablesFormType::showTablesData($input, $value);
    }

    public static function tabletrFormFormat($input, $value) {
        return tablesFormType::showTablesData($input, $value);
    }

    public static function tableSubnavFormFormat($input, $value) {
        return tablesFormType::showTablesData($input, $value, 1);
    }

    public static function radioFormat($input, $value) {
        return inputsFormWidget::radioType($input['inputname'], $value, ['_disabled'=>1]);
    }

    public static function uploadattachFormat($input, $value) {
        return uploadattachType::formatViewData($input, $value);
    }

    public static function uploadimgFormat($input, $value) {
        return uploadimgType::formatViewData($input, $value);
    }

    public static function usetypescoreFormat($input, $value) {
        $scoretypes = ScoreHelper::scoretypes();
        $value = !is_array($value) ? json_decode($value, true) : $value;
        return $value[0].Lev::arrv($value[1], $scoretypes);
    }

    public static function selectsFormat($input, $value) {
        $item = inputsFormWidget::getSelectItem($input);
        return inputsFormWidget::getSelectsShow($item, $value);
    }

    public static function selectscodeFormat($input, $value) {
        return static::selectsFormat($input, $value);
    }

    public static function selectFormat($input, $value) {
        return Lev::arrv($value, inputsFormWidget::getSelectItem($input));
    }

    public static function selectcodeFormat($input, $value) {
        return static::selectFormat($input, $value);
    }

    public static function selectSearchFormat($input, $value) {
        return static::selectFormat($input, $value);
    }

    public static function textSelectFormat($input, $value) {
        return static::selectFormat($input, $value);
    }

    public static function textareaFormat($input, $value) {
        return textareaType::formatViewData($input, $value);
    }

}