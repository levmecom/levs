<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-21 22:39
 *
 * 项目：rm  -  $  - selectsRangeType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\widgets\inputs\tablesForm;
use lev\widgets\inputs\inputsFormWidget;

!defined('INLEV') && exit('Access Denied LEV');


class selectsRangeType
{

    public static function input($name, $value, $v) {
        $options = inputsFormWidget::getSelectItem($v, true);

        $name = tablesForm::inputName($name, $v);

        $input = inputsFormWidget::bigBtn($name);
        $input.= '<div class="checkbox-list bigObjx">';

        $value && !is_array($value) && $value = json_decode($value, true);
        $values = $value ? array_flip($value) : [];
        foreach ($options as $vid => $vtitle) {
            $ckd    = isset($values[$vid]) ? ' checked' : '';
            $input .= '<label><input type="checkbox" name="'.$name.'[]" value="'.$vid.'"'.$ckd.'>'.$vtitle.'</label>';
        }
        return $input .'</div>';
    }

    public static function formatViewData($input, $value) {
        return static::getSelectsShow(inputsFormWidget::getSelectItem($input), $value);
    }

    public static function getSelectsShow($item, $inputvalue, $pre = ', ') {
        if (!$inputvalue) return '';
        !is_array($inputvalue) && $inputvalue = json_decode($inputvalue, true);
        $_res = [];
        foreach ($inputvalue as $v) {
            $_res[] = $item[$v];
        }
        return implode($pre, $_res);
    }
}