<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-10 15:13
 *
 * 项目：rm  -  $  - uploadattachType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\widgets\inputs\inputsWidget;
use lev\widgets\inputs\inputsFormWidget;

!defined('INLEV') && exit('Access Denied LEV');


class uploadattachType
{

    public static function getSettings($settings) {
        return Lev::getSettings(Lev::getSettings($settings, 'set_uploadattach'))[0];
    }

    public static function input($name, $value, $v, $isimg = null)
    {
        $v['settings'] = inputsFormWidget::formatUploadSettings($v['settings'], $isimg);
        return inputsWidget::uploadattachType($name, $value, $v);
    }

    public static function formatViewData($input, $value) {
        return !$value ? '没有附件' : '<a href="'.Lev::uploadRealSrc($value).'" class="openPP">下载附件</a>';
    }

}