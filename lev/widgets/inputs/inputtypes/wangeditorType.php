<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-10 14:26
 *
 * 项目：rm  -  $  - wangeditorType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\base\Assetsv;
use lev\widgets\editors\editorWidget;

!defined('INLEV') && exit('Access Denied LEV');


class wangeditorType
{

    public static function input($name, $value, $v) {
        return editorWidget::wangEditor($name, $value, $v);
    }

    public static function formatViewData($input, $inputvalue) {
        return Assetsv::highlight(1).'<div class="w-e-text w-e-text-container">'.Lev::decodeHtml($inputvalue).'</div>';
    }

}