<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-16 11:31
 *
 * 项目：rm  -  $  - selectRangeType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\widgets\inputs\inputsFormWidget;

!defined('INLEV') && exit('Access Denied LEV');


class selectRangeType
{

    public static function input($name, $value, $v) {
        $tabid = !empty($v['idk___']) ? $v['idk___'] : '';
        $opInputname = $v['inputname'] . $tabid;

        $options = inputsFormWidget::getSelectItem($v, true);

        $selected  = (isset($options[$value]) || $value) ? '<option value="'.$value.'">'.Lev::arrv($value, $options).'</option>' : '';
        foreach ($options as $_key => $_name) {
            $selected .= '<option value="'.$_key.'">'.$_name.'</option>';
        }

        return <<<html

<div class=" inputname-{$opInputname}">
    <label class="inblk scale9 transl" style="max-width: 256px;width: calc(100% - 15px);">
        <select class=" form-control" name="{$name}" style="height: 24px;max-height: 24px;width:calc(100% + 25px);">
            {$selected}
        </select>
    </label>
</div>

html;

    }

    public static function formatOptions(array $options)
    {
        $result = [];
        foreach ($options as $v) {
            if (empty($v['status']) || Lev::$app['isAdmin']) {
                $num = is_numeric($v['title']) ? $v['title'] : floatval($v['title']);
                $unit = explode($num, $v['title'])[1];
                isset($start) || $start = $num;
                if ($num > $start) {
                    for ($i = $start; $i <= $num; $i++) {
                        $result[$i] = $i . $unit;
                    }
                } else {
                    $result[$v['id']] = $v['title'];
                }
                $start = $num;
            }
        }
        return $result;
    }
}