<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-16 11:34
 *
 * 项目：rm  -  $  - _InputTypes.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use lev\widgets\inputs\inputsWidget;

!defined('INLEV') && exit('Access Denied LEV');


class _InputTypes extends inputsWidget
{

    public static $inputtype = [
        'textSelect' => '可输入下拉框(textSelect)',
        'selectRange' => '数字范围单选项(selectRange)',
        'selectsRange' => '数字范围多选项(selectsRange)',
        'htmlTextarea' => '支持html文本框(htmlTextarea)',
        'order' => '子表单数据排序(order)',
    ];

    public static $inputInfos = [
        'selectRange' => [
            'type' => 'selectRange',
            'name' => '数字范围选项(selectRange)',
            'desc' => '自动补充选项名称之间缺失的数字，可以带单位。例：10号',
        ],
    ];

    public static function selectsRangeType($name, $value, $v) {
        return selectsRangeType::input($name, $value, $v);
    }

    public static function htmlTextareaType($name, $value, $v) {
        return htmlTextareaType::input($name, $value, $v);
    }

    public static function selectRangeType($name, $value, $v) {
        return selectRangeType::input($name, $value, $v);
    }

    public static function textSelectType($name, $value, $v) {
        return textSelectType::input($name, $value, $v);
    }

    public static function orderType($name, $value, $v) {
        return '<input type="number" class="wd50 form-control" name="'.$name.'" value="'.$value.'"/>';
    }

}