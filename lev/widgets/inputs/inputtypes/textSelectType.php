<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-02 15:02
 *
 * 项目：rm  -  $  - textSelectType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;
use lev\helpers\cacheFileHelpers;
use lev\widgets\inputs\inputsFormWidget;

!defined('INLEV') && exit('Access Denied LEV');

class textSelectType
{
    public static function getTextDatas($formPre = 'datax') {
        return Lev::stripTags(Lev::GPv(static::getTextInputPre($formPre)));
    }

    public static function getTextInputPre($formPre = 'datax') {
        return 'textSelects'.$formPre;
    }

    public static function input($name, $value, $v) {
        $tabid = isset($v['idk___']) ? $v['idk___'] : '';
        $opInputname = $v['inputname'] . $tabid;

        $options = inputsFormWidget::getSelectItem($v, true);
        $valuename = Lev::arrv($value, $options);
        $selected = (isset($options[$value]) || $value) ? '<option value="'.$value.'">'.$valuename.'</option>' : '';
        //$selected = '<option value="'.$value.'">'.Lev::arrv($value, $options).'</option>';
        foreach ($options as $_key => $_name) {
            $selected .= '<option value="'.$_key.'">'.$_name.'</option>';
        }

        return <<<html

<div class="textSelectBox inputname-{$opInputname}">
    <label class="inblk scale9 transl" style="max-width: 256px;width: calc(100% - 15px);">
        <input type="text" class="textSelectTxt form-control on{$opInputname}Slt" style="position: absolute;height:18px;max-height: 24px;min-width: 100%;" placeholder="可输入下拉框" title="可输入下拉框" value="{$valuename}" name="textSelects{$name}" autocomplete="off">
        <select class="textSelectSlt form-control _{$opInputname}_slt" name="{$name}" style="height: 24px;max-height: 24px;width:calc(100% + 25px);">
            {$selected}
        </select>
    </label>
</div>

<script>
    jQuery(function () {
        Levme.textSelect('{$opInputname}');
    });
</script>

html;

    }

}