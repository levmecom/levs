<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-17 13:29
 *
 * 项目：rm  -  $  - ${FILE_NAME}
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;

!defined('INLEV') && exit('Access Denied LEV');


class htmlTextareaType
{

    public static function input($name, $value, $v)
    {
        return '<textarea class="form-control resizable" name="'.$name.'">'.Lev::decodeHtml($value).'</textarea>';
    }

    public static function formatViewData($input, $value) {
        return '<div class="show-content-detail">'. Lev::decodeHtml($value) .'</div>';
    }

}