<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-10 15:11
 *
 * 项目：rm  -  $  - uploadimgType.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs\inputtypes;

use Lev;

!defined('INLEV') && exit('Access Denied LEV');


class uploadimgType
{

    public static function getSettings($settings) {
        return Lev::getSettings(Lev::getSettings($settings, 'set_uploadimg'))[0];
    }

    public static function input($name, $value, $v)
    {
        return uploadattachType::input($name, $value, $v, 1);
    }

    public static function formatViewData($input, $value) {
        $set = static::getSettings($input['settings']);
        return '<img class=lazy data-src="'.Lev::uploadRealSrc($value).'" width="'.$set['width'].'" height="'.$set['height'].'">';
    }

}