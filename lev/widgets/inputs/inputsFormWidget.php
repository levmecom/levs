<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-11-02 14:14
 *
 * 项目：rm  -  $  - inputsFormWidget.php
 *
 * 作者：liwei 
 */

namespace lev\widgets\inputs;

use Lev;
use lev\helpers\cacheFileHelpers;
use lev\widgets\inputs\inputtypes\selectRangeType;
use lev\widgets\inputs\inputtypes\uploadattachType;
use lev\widgets\inputs\inputtypes\uploadimgType;

!defined('INLEV') && exit('Access Denied LEV');

class inputsFormWidget extends inputtypes
{

    public static function inputtype()
    {
        $arr = parent::inputtype(); // TODO: Change the autogenerated stub
        unset($arr['tabletr'], $arr['tables'], $arr['selectcode'], $arr['selectscode']);
        return $arr;
    }

    public static function myInputtype() {
        return static::$inputtype;
    }

    public static function form($inputs, $values = [], $pre = '', $formInfo = [])
    {
        //return parent::form($inputs, $values, $pre, $show); // TODO: Change the autogenerated stub
        return parent::render(false, __DIR__ . '/views/form.php', [
            'inputs'   => $inputs,
            'values'   => $values,
            'pre'      => $pre,
            'formInfo' => $formInfo,
        ]);
    }

    public static function input($type = '', $name = '', $value = '', $v = []) {
        $method = $type.'Type';
        return method_exists(__CLASS__, $method) ? static::$method($name, $value, $v) : '<b class=red>未知input类型 &raquo; '.$type.'</b>';
    }

    public static function showData($inputvalue, $inputSetInfo)
    {
        return parent::showData($inputvalue, $inputSetInfo); // TODO: Change the autogenerated stub
    }

    public static function formatSaveInputsData($datas, $inputs, $tables = [], $tablesFormData = [])
    {
        //return parent::formatSaveInputsData($datas, $inputs, $tables, $tablesFormData); // TODO: Change the autogenerated stub

        return $datas;
    }

    public static function formatUploadSettings($settings, $isimg)
    {
        $upset = $isimg ? uploadimgType::getSettings($settings) : uploadattachType::getSettings($settings);
        return empty($upset['maxsize']) ? '' : $upset['maxsize'].'|'.$upset['exts'].
            (isset($upset['width']) ? '|'.$upset['width'].'x'.$upset['height'] : '');
    }

    /**
     * 同时支持自定义选项和调用自定义类
     * @param $inputSetInfo
     * @param bool $code
     * @return array|bool|mixed
     */
    public static function getSelectItem($inputSetInfo, $code = false) {
        if ($code || substr($inputSetInfo['inputtype'], -4) == 'code') {
            $item = Lev::actionObjectMethod(trim($inputSetInfo['settings']), [], 'set' . $inputSetInfo['inputname']);
            if ($item !== false) {
                return $item;
            }
        }
        $item  = [];
        if (cacheFileHelpers::isSerializeStr($inputSetInfo['settings'])) {
            $_item = Lev::getSettings(Lev::getSettings($inputSetInfo['settings'], 'set_textSelect'));
            if ($inputSetInfo['inputtype'] == 'selectRange' || $inputSetInfo['inputtype'] == 'selectsRange') {
                $item = selectRangeType::formatOptions($_item);
            }else {
                foreach ($_item as $v) {
                    (empty($v['status']) || Lev::$app['isAdmin']) && $item[$v['id']] = $v['title'];
                }
            }
        }else {
            $_item = explode("\n", $inputSetInfo['settings']);
            foreach ($_item as $k => $r) {
                if (trim($r)) {
                    $one = explode('=', $r);
                    $val = trim($one[0]);
                    $_name = trim(Lev::arrv(1, $one));
                    $item[$val] = $_name;
                }
            }
        }
        return $item;
    }

    /**
     * 是否下拉框
     * @param $inputInfo
     * @return bool
     */
    public static function isSelect($inputInfo) {
        return stripos($inputInfo['inputtype'], 'select') !== false;
    }

    /**
     * 是否仅选择性下拉框
     * @param $inputInfo
     * @return bool
     */
    public static function isSelectOnly($inputInfo)
    {
        return (static::isSelect($inputInfo) && !static::isSelectInput($inputInfo));
    }

    /**
     * 是否可输入下拉框
     * @param $inputInfo
     * @return bool
     */
    public static function isSelectInput($inputInfo) {
        $inputs = ['textSelect', 'selectSearch'];
        return stripos($inputInfo['inputtype'], 'SelectInput') !== false || in_array($inputInfo['inputtype'], $inputs);
    }

    /**
     * @param $inputInfo
     * @return bool
     */
    public static function isNumberInput($inputInfo)
    {
        return stripos($inputInfo['inputtype'], 'number') !== false;
    }

}